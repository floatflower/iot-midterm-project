describe('Test script demo.', () => {

    // clear database
    beforeEach(() => {
        cy.exec("php bin/console d:d:d --force --if-exists");
        cy.exec("php bin/console d:d:c --quiet");
        cy.exec("php bin/console d:m:m --quiet");
        cy.exec("php bin/console app:setup:test");
    });

    it('Health check.', () => {
        cy.request("/callback/health-check").then(response => {
            expect(response.status).to.equal(200);
        });
    })
})