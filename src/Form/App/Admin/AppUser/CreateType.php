<?php

namespace App\Form\App\Admin\AppUser;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, array(
                "label" => "電子郵件",
                "required" => false
            ))
            ->add("mobile", TextType::class, array(
                "label" => "手機號碼",
                "required" => false
            ))
            ->add("password", PasswordType::class, array(
                "label" => "密碼"
            ))
            ->add("avatar", TextType::class, array(
                "label" => "頭像",
                "required" => false
            ))
            ->add("is_admin", CheckboxType::class, array(
                "label" => "設為管理員",
                "required" => false
            ))
            ->add("expire_time", DateType::class, array(
                "label" => "使用者過期時間",
                "required" => false,
                "widget" => "single_text",
            ))
            ->add("forever", CheckboxType::class, array(
                "label" => "永久使用者",
                "required" => false
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
