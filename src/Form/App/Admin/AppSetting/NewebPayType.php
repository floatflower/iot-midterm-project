<?php

namespace App\Form\App\Admin\AppSetting;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewebPayType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("api_url", UrlType::class, array(
                "label" => "藍新金流 API",
                "required" => false
            ))
            ->add('merchant_id', TextType::class, array(
                "label" => "商店編號 / Merchand ID",
                "required" => false
            ))
            ->add("hash_iv", TextType::class, array(
                "label" => "Hash IV",
                "required" => false
            ))
            ->add("hash_key", TextType::class, array(
                "label" => "Hash Key",
                "required" => false
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
