<?php

namespace App\Form\App\Admin\BannerItem;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                "label" => "標題",
                "required" => false
            ))
            ->add("subtitle", TextareaType::class, array(
                "label" => "副標題",
                "required" => false
            ))
            ->add("link", UrlType::class, array(
                "label" => "連結",
                "required" => false
            ))
            ->add("image", TextType::class, array(
                "label" => "圖片"
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
