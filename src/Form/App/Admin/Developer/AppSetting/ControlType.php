<?php

namespace App\Form\App\Admin\Developer\AppSetting;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ControlType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('seo_twitter_enabled', CheckboxType::class, array(
                "label" => "Twitter Card"
            ))
            ->add("seo_facebook_enabled", CheckboxType::class, array(
                "label" => "Facebook Card"
            ))
            ->add("payment_newebpay_enabled", CheckboxType::class, array(
                "label" => "藍新金流"
            ))
            ->add("payment_ecpay_enabled", CheckboxType::class, array(
                "label" => "綠界金流"
            ))
            ->add("ga_track_id_enabled", CheckboxType::class, array(
                "label" => "GA 追蹤碼"
            ))
            ->add("google_recaptcha_enabled", CheckboxType::class, array(
                "label" => "Google Recaptcha"
            ))
            ->add("sms_get_enabled", CheckboxType::class, array(
                "label" => "SMS Get"
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
