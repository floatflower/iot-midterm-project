<?php

namespace App\Form\App\Website\Profile;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UpdatePasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('original_password', PasswordType::class, array(
                "label" => "原密碼"
            ))
            ->add("new_password", PasswordType::class, array(
                "label" => "新密碼"
            ))
            ->add("new_password_confirm", PasswordType::class, array(
                "label" => "再輸入一次新密碼"
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
