<?php

namespace App\EventSubscriber\Auth;

use App\Event\Auth\AuthTokenInvalidEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

class AuthTokenInvalidSubscriber implements EventSubscriberInterface
{
    private bool $triggered = false;

    public function onAuthTokenInvalid(AuthTokenInvalidEvent $authTokenInvalidEvent)
    {
        $this->triggered = true;
    }

    public function onKernelResponse(ResponseEvent $event)
    {
        if($this->triggered) {
            $response = $event->getResponse();
            $response->headers->setCookie(new Cookie("auth_token", null));
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            'kernel.response' => 'onKernelResponse',
            AuthTokenInvalidEvent::class => "onAuthTokenInvalid"
        ];
    }
}
