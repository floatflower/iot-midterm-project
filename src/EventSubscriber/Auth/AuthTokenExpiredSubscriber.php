<?php

namespace App\EventSubscriber\Auth;

use App\Entity\AppUserAuthToken;
use App\Event\Auth\AuthTokenExpiredEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Event\TerminateEvent;

class AuthTokenExpiredSubscriber implements EventSubscriberInterface
{
    private bool $triggered = false;

    /**
     * @var AppUserAuthToken $appUserAuthToken
     */
    private AppUserAuthToken $appUserAuthToken;

    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function onAuthTokenExpired(AuthTokenExpiredEvent $authTokenExpiredEvent)
    {
        $this->triggered = true;
        $this->appUserAuthToken = $authTokenExpiredEvent->getAppUserToken();
    }

    public function onKernelResponse(ResponseEvent $event)
    {
        if($this->triggered) {
            $response = $event->getResponse();
            $response->headers->setCookie(new Cookie("auth_token", null));
        }
    }

    public function onKernelTerminate()
    {
        if($this->triggered) {
            $this->entityManager->remove($this->appUserAuthToken);
            $this->entityManager->flush();
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            AuthTokenExpiredEvent::class => 'onAuthTokenExpired',
            'kernel.response' => 'onKernelResponse',
            'kernel.terminate' => 'onKernelTerminate'
        ];
    }
}
