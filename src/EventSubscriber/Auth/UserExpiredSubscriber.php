<?php

namespace App\EventSubscriber\Auth;

use App\Entity\AppUser;
use App\Entity\AppUserAuthToken;
use App\Event\Auth\UserExpiredEvent;
use App\Repository\AppUserAuthTokenRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Twig\Environment;

class UserExpiredSubscriber implements EventSubscriberInterface
{
    private bool $triggered = false;

    private AppUser $appUser;

    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var Environment $environment
     */
    private Environment $environment;

    public function __construct(EntityManagerInterface $entityManager, Environment $environment)
    {
        $this->entityManager = $entityManager;
        $this->environment = $environment;
    }

    public function onUserExpired(UserExpiredEvent $userExpiredEvent)
    {
        $this->triggered = true;
        $this->appUser = $userExpiredEvent->getAppUser();
    }

    public function onKernelResponse(ResponseEvent $event)
    {
        if($this->triggered) {
            $response = $event->getResponse();
            $response->headers->setCookie(new Cookie("auth_token", null));

            $content = $this->environment->render("app/error/user-expired.html.twig");
            $response->setContent($content);
            $response->setStatusCode(Response::HTTP_FORBIDDEN);

            /**
             * @var AppUserAuthTokenRepository $appUserAuthTokenRepository
             */
            $appUserAuthTokenRepository = $this->entityManager->getRepository(AppUserAuthToken::class);
            $appUserAuthTokens = $appUserAuthTokenRepository->findBy(array("appUser" => $this->appUser));

            foreach ($appUserAuthTokens as $appUserAuthToken) {
                $this->entityManager->remove($appUserAuthToken);
            }

            $this->entityManager->flush();
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            UserExpiredEvent::class => "onUserExpired",
            'kernel.response' => 'onKernelResponse'
        ];
    }
}
