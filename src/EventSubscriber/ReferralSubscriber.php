<?php

namespace App\EventSubscriber;

use App\Entity\AppUser;
use App\Repository\AppUserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

class ReferralSubscriber implements EventSubscriberInterface
{
    private EntityManagerInterface $entityManager;

    /**
     * 因為 Symfony 的 Service 在沒有特別註明的狀態下 都是單例狀態，
     * 因此透過同一個 Subscriber 的內部狀態來決定是否需要在 Response 傳回之前清空用戶的 referrer cookie
     * @var bool
     */
    private bool $referrerNeedToBeClear = false;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();
        $request->attributes->set("referrer", null);

        $referrer = $request->cookies->get("referrer", null);
        if($referrer) {

            /**
             * @var AppUserRepository $appUserRepository
             */
            $appUserRepository = $this->entityManager->getRepository(AppUser::class);

            $appUser = $appUserRepository->find($referrer);

            if($appUser) $request->attributes->set("referrer", $appUser);
            else $this->referrerNeedToBeClear = true;
        }
    }

    public function onKernelResponse(ResponseEvent $event)
    {
        if($this->referrerNeedToBeClear) {
            $response = $event->getResponse();
            $response->headers->setCookie(new Cookie("referrer", null));
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            'kernel.request' => 'onKernelRequest',
            "kernel.response" => "onKernelResponse"
        ];
    }
}
