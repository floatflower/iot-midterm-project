<?php

namespace App\Repository;

use App\Entity\AppUserMobileVerifyCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AppUserMobileVerifyCode|null find($id, $lockMode = null, $lockVersion = null)
 * @method AppUserMobileVerifyCode|null findOneBy(array $criteria, array $orderBy = null)
 * @method AppUserMobileVerifyCode[]    findAll()
 * @method AppUserMobileVerifyCode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AppUserMobileVerifyCodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AppUserMobileVerifyCode::class);
    }

    // /**
    //  * @return AppUserMobileVerifyCode[] Returns an array of AppUserMobileVerifyCode objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AppUserMobileVerifyCode
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
