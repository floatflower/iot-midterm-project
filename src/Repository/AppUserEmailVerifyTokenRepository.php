<?php

namespace App\Repository;

use App\Entity\AppUserEmailVerifyToken;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AppUserEmailVerifyToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method AppUserEmailVerifyToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method AppUserEmailVerifyToken[]    findAll()
 * @method AppUserEmailVerifyToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AppUserEmailVerifyTokenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AppUserEmailVerifyToken::class);
    }

    // /**
    //  * @return AppUserEmailVerifyToken[] Returns an array of AppUserEmailVerifyToken objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AppUserEmailVerifyToken
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
