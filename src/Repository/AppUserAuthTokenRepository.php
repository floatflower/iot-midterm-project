<?php

namespace App\Repository;

use App\Entity\AppUser;
use App\Entity\AppUserAuthToken;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AppUserAuthToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method AppUserAuthToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method AppUserAuthToken[]    findAll()
 * @method AppUserAuthToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AppUserAuthTokenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AppUserAuthToken::class);
    }

    // /**
    //  * @return AppUserAuthToken[] Returns an array of AppUserAuthToken objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AppUserAuthToken
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
