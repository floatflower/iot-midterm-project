<?php

namespace App\Repository;

use App\Entity\AppUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AppUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method AppUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method AppUser[]    findAll()
 * @method AppUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AppUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AppUser::class);
    }

    /**
     * @param string $uuid
     * @return AppUser|null
     */
    public function findOneByUuid(string $uuid) : ?AppUser
    {
        try {
            return $this->createQueryBuilder("u")
                ->where("u.uuid = :uuid")
                ->setParameter("uuid", $uuid)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $nonUniqueResultException) {}

        return null;
    }

    /**
     * @param string $loginKey
     * @return AppUser|null
     */
    public function findByLoginKey(string $loginKey) : ?AppUser
    {
        try {
            return $this->createQueryBuilder('u')
                ->where('u.email = :loginKey')
                ->orWhere('u.username = :loginKey')
                ->setParameter("loginKey", $loginKey)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            $results = $this->createQueryBuilder('u')
                ->where('u.email = :loginKey')
                ->orWhere('u.username = :loginKey')
                ->setParameter("loginKey", $loginKey)
                ->getQuery()
                ->getResult();

            return $results[0];
        }
    }

    /**
     * @param string $email
     * @return AppUser|null
     */
    public function findOneByEmail(string $email) : ?AppUser
    {
        return $this->findOneBy(array("email" => $email));
    }

    /**
     * @param string $username
     * @return AppUser|null
     */
    public function findOneByUsername(string $username) : ?AppUser
    {
        return $this->findOneBy(array("username" => $username));
    }

    /**
     * @param string $mobile
     * @return AppUser|null
     */
    public function findOneByMobile(string $mobile) : ?AppUser
    {
        return $this->findOneBy(array("mobile" => $mobile));
    }

    /**
     * @param string $referralCode
     * @return AppUser|null
     */
    public function findOneByReferralCode(string $referralCode) : ?AppUser
    {
        return $this->findOneBy(array("referralCode" => $referralCode));
    }

    // /**
    //  * @return AppUser[] Returns an array of AppUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AppUser
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
