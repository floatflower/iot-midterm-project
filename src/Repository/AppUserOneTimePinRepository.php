<?php

namespace App\Repository;

use App\Entity\AppUserOneTimePin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AppUserOneTimePin|null find($id, $lockMode = null, $lockVersion = null)
 * @method AppUserOneTimePin|null findOneBy(array $criteria, array $orderBy = null)
 * @method AppUserOneTimePin[]    findAll()
 * @method AppUserOneTimePin[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AppUserOneTimePinRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AppUserOneTimePin::class);
    }

    // /**
    //  * @return AppUserOneTimePin[] Returns an array of AppUserOneTimePin objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AppUserOneTimePin
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
