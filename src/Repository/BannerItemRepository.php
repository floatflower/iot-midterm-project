<?php

namespace App\Repository;

use App\Entity\BannerItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BannerItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method BannerItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method BannerItem[]    findAll()
 * @method BannerItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BannerItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BannerItem::class);
    }

    /**
     * @param string $uuid
     * @return BannerItem|null
     */
    public function findOneByUuid(string $uuid) : ?BannerItem
    {
        try {
            return $this->createQueryBuilder("bannerItem")
                ->where("bannerItem.uuid = :uuid")
                ->setParameter("uuid", $uuid)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $nonUniqueResultException) {
            // impossible
            return null;
        }
    }

    // /**
    //  * @return BannerItem[] Returns an array of BannerItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BannerItem
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
