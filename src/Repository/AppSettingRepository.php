<?php

namespace App\Repository;

use App\Entity\AppSetting;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AppSetting|null find($id, $lockMode = null, $lockVersion = null)
 * @method AppSetting|null findOneBy(array $criteria, array $orderBy = null)
 * @method AppSetting[]    findAll()
 * @method AppSetting[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AppSettingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AppSetting::class);
    }

    public function findOneByKey(string $key) : ?AppSetting
    {
        try {
            return $this->createQueryBuilder('a')
                ->andWhere("a.key = :key")
                ->setParameter("key", $key)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $nonUniqueResultException) {
            return null;
        }
    }

    // /**
    //  * @return AppSetting[] Returns an array of AppSetting objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AppSetting
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
