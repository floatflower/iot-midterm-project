<?php

namespace App\Repository;

use App\Entity\AppUserPunchRecord;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AppUserPunchRecord|null find($id, $lockMode = null, $lockVersion = null)
 * @method AppUserPunchRecord|null findOneBy(array $criteria, array $orderBy = null)
 * @method AppUserPunchRecord[]    findAll()
 * @method AppUserPunchRecord[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AppUserPunchRecordRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AppUserPunchRecord::class);
    }

    // /**
    //  * @return AppUserPunchRecord[] Returns an array of AppUserPunchRecord objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AppUserPunchRecord
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
