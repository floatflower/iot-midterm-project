<?php

namespace App\Security\Voter;

use App\Entity\AppUser;
use App\Entity\Service;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class ServiceVoter extends Voter
{
    const FETCH = "SERVICE_FETCH";
    const UPDATE = "SERVICE_UPDATE";
    const DELETE = "SERVICE_DELETE";

    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [self::FETCH, self::UPDATE, self::DELETE])
            && $subject instanceof Service;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /**
         * @var AppUser $user
         */
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface || $user->isAnonymous()) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::FETCH:
            case self::UPDATE:
            case self::DELETE:
                return $user->isAdmin();
        }

        return false;
    }
}
