<?php

namespace App\Security\Voter;

use App\Entity\AppUser;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class AppUserVoter extends Voter
{
    const APP_USER_FETCH = "APP_USER_FETCH";
    const APP_USER_UPDATE = "APP_USER_UPDATE";
    const APP_USER_DELETE = "APP_USER_DELETE";

    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [self::APP_USER_FETCH, self::APP_USER_UPDATE, self::APP_USER_DELETE])
            && $subject instanceof AppUser;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /**
         * @var AppUser $user
         */
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface || $user->isAnonymous()) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::APP_USER_FETCH:
            case self::APP_USER_UPDATE:
            case self::APP_USER_DELETE:
                return $user->isAdmin();
        }

        return false;
    }
}
