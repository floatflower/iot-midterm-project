<?php


namespace App\Security\AccessDeniedHandler;


use App\Service\Response\ErrorCode;
use App\Service\Response\ErrorResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;

class ServiceAccessDeniedHandler implements AccessDeniedHandlerInterface
{
    /**
     * @param Request $request
     * @param AccessDeniedException $accessDeniedException
     * @return Response
     */
    public function handle(Request $request, AccessDeniedException $accessDeniedException)
    {
        return new ErrorResponse(ErrorCode::ACCESS_DENIED, Response::HTTP_FORBIDDEN);
    }

}