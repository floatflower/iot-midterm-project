<?php


namespace App\Security\AccessDeniedHandler;


use App\Entity\AppUser;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;
use Twig\Environment;

class AppAccessDeniedHandler implements AccessDeniedHandlerInterface
{
    /**
     * @var Security $security
     */
    private Security $security;

    /**
     * @var Environment $environment
     */
    private Environment $environment;

    public function __construct(Security $security, Environment $environment)
    {
        $this->security = $security;
        $this->environment = $environment;
    }

    /**
     * @param Request $request
     * @param AccessDeniedException $accessDeniedException
     * @return RedirectResponse|Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function handle(Request $request, AccessDeniedException $accessDeniedException)
    {
        /**
         * @var AppUser $appUser
         */
        $appUser = $this->security->getUser();
        if($appUser->isAnonymous()) return new RedirectResponse("/auth/login");
        else return new Response($this->environment->render("app/error/access-denied.html.twig"), Response::HTTP_FORBIDDEN);
    }

}