<?php

namespace App\Security;

use App\Entity\AppUser;
use App\Entity\AppUserAuthToken;
use App\Event\Auth\AuthTokenExpiredEvent;
use App\Event\Auth\AuthTokenInvalidEvent;
use App\Event\Auth\UserExpiredEvent;
use App\Repository\AppUserAuthTokenRepository;
use Doctrine\ORM\EntityManagerInterface;
use Firebase\JWT\JWT;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class AppAuthenticator extends AbstractGuardAuthenticator
{
    private EntityManagerInterface $entityManager;

    /**
     * @var ContainerInterface $container
     */
    private ContainerInterface $container;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher,
                                ContainerInterface $container)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function supports(Request $request)
    {
        return true;
    }

    public function getCredentials(Request $request)
    {
        $authToken = $request->cookies->get("auth_token", null);
        if(!is_null($authToken)) return array("authToken" => $authToken);

        $authToken = $request->headers->get("Authorization", null);
        if(!is_null($authToken) && preg_match("/^Bearer \S+$/", $authToken))
            $authToken = substr($authToken, 7);

        return array("authToken" => $authToken);
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $key = $this->container->getParameter("JWT_KEY");
        $jwt = $credentials["authToken"];

        /**
         * 沒有 JWT 直接成為匿名用戶
         */
        if(is_null($jwt)) return AppUser::makeAnonymous();

        $decoded = array();

        try {
            $decoded = (array)(JWT::decode($jwt, $key, array('HS256')));
        } catch (\Exception $exception) {
            /**
             * 解析失敗代表 JWT 的格式不正確
             * 直接變匿名用戶
             */
            $authTokenInvalidEvent = new AuthTokenInvalidEvent();
            $this->eventDispatcher->dispatch($authTokenInvalidEvent);
            return AppUser::makeAnonymous();
        }

        $authToken = $decoded["auth_token"];

        /**
         * $authToken 是 false condition 或是 $authToken 是空字串
         * 直接變匿名用戶
         */
        if(!$authToken || strlen($authToken) === 0) {
            $authTokenInvalidEvent = new AuthTokenInvalidEvent();
            $this->eventDispatcher->dispatch($authTokenInvalidEvent);
            return AppUser::makeAnonymous();
        }

        $tokenHash = hash("sha512", $authToken);

        /**
         * @var AppUserAuthTokenRepository $appUserAuthTokenRepository
         */
        $appUserAuthTokenRepository
            = $this->entityManager->getRepository(AppUserAuthToken::class);

        $appUserAuthToken = $appUserAuthTokenRepository->findOneBy(array("tokenHash" => $tokenHash));

        /**
         * 找不到對應的 AuthToken 實體
         * 成為匿名用戶
         */
        if(!$appUserAuthToken) {
            $authTokenInvalidEvent = new AuthTokenInvalidEvent();
            $this->eventDispatcher->dispatch($authTokenInvalidEvent);
            return AppUser::makeAnonymous();
        }

        $currentTime = new \DateTime("now");

        /**
         * AuthToken 過期變成匿名用戶
         */
        if($currentTime > $appUserAuthToken->getExpireAt()) {
            $authTokenExpiredEvent = new AuthTokenExpiredEvent();
            $authTokenExpiredEvent->setAppUserToken($appUserAuthToken);
            $this->eventDispatcher->dispatch($authTokenExpiredEvent);
            return AppUser::makeAnonymous();
        }

        $appUser = $appUserAuthToken->getAppUser();

        /**
         * 用戶過期過期，也是變成匿名用戶
         */
        if($currentTime > $appUser->getExpireTime()) {
            $userExpiredEvent = new UserExpiredEvent();
            $userExpiredEvent->setAppUser($appUser);
            $this->eventDispatcher->dispatch($userExpiredEvent);
            return AppUser::makeAnonymous();
        }

        return $appUser;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new RedirectResponse("/auth/login");
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey)
    {
        // continue
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new RedirectResponse("/auth/login");
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
