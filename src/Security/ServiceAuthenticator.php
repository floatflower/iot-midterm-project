<?php

namespace App\Security;

use App\Entity\Service;
use App\Repository\ServiceRepository;
use App\Service\Response\ErrorCode;
use App\Service\Response\ErrorResponse;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class ServiceAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function supports(Request $request)
    {
        return true;
    }

    public function getCredentials(Request $request)
    {
        $serviceToken = $request->headers->get("Authorization", null);
        if(!is_null($serviceToken) && preg_match("/^Bearer \S+$/", $serviceToken))
            $serviceToken = substr($serviceToken, 7);

        return array("serviceToken" => $serviceToken);
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $serviceToken = $credentials["serviceToken"];
        $tokenHash = hash("sha512", $serviceToken);

        if(is_null($serviceToken))
            throw new AuthenticationException();

        /**
         * @var ServiceRepository $serviceRepository
         */
        $serviceRepository = $this->entityManager->getRepository(Service::class);
        $service = $serviceRepository->findOneBy(array("tokenHash" => $tokenHash));

        if(!$service)
            throw new AuthenticationException();

        return $service;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new ErrorResponse(ErrorCode::AUTHENTICATION_FAILED, Response::HTTP_UNAUTHORIZED);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey)
    {
        // continue
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new ErrorResponse(ErrorCode::AUTHENTICATION_FAILED, Response::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
