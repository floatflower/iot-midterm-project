<?php

namespace App\Entity;

use App\Repository\AppUserResetPasswordTokenRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=AppUserResetPasswordTokenRepository::class)
 */
class AppUserResetPasswordToken
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    private string $tokenHash;

    /**
     * @param string $tokenHash
     * @return $this
     */
    public function setTokenHash(string $tokenHash) : self
    {
        $this->tokenHash = $tokenHash;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTokenHash() : ?string
    {
        return $this->tokenHash;
    }

    /**
     * @ORM\ManyToOne(targetEntity="AppUser")
     * @ORM\JoinColumn(name="app_user_id", referencedColumnName="id", onDelete="CASCADE")
     * @var AppUser $appUser
     */
    private AppUser $appUser;

    /**
     * @param AppUser $appUser
     * @return $this
     */
    public function setAppUser(AppUser $appUser) : self
    {
        $this->appUser = $appUser;
        return $this;
    }

    /**
     * @return AppUser|null
     */
    public function getAppUser() : ?AppUser
    {
        return $this->appUser;
    }

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime $createAt
     */
    private \DateTime $createAt;

    /**
     * @return \DateTime|null
     */
    public function getCreateAt() : ?\DateTime
    {
        return $this->createAt;
    }

    /**
     * @ORM\PrePersist()
     * @return $this
     */
    public function setCreateAt() : self
    {
        $this->createAt = new \DateTime("now");
        return $this;
    }

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private \DateTime $updateAt;

    /**
     * @return \DateTime|null
     */
    public function getUpdateAt() : ?\DateTime
    {
        return $this->updateAt;
    }

    /**
     * @ORM\PreUpdate()
     * @ORM\PrePersist()
     * @return $this
     */
    public function setUpdateAt() : self
    {
        $this->updateAt = new \DateTime("now");
        return $this;
    }

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime $expireAt
     */
    private \DateTime $expireAt;

    /**
     * @ORM\PrePersist()
     * @return self
     */
    public function setExpireAt(): self
    {
        $this->expireAt = (new \DateTime("now"))->add(new \DateInterval("PT12H"));
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExpireAt(): \DateTime
    {
        return $this->expireAt;
    }
}
