<?php

namespace App\Entity;

use App\Repository\BannerItemRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass=BannerItemRepository::class)
 */
class BannerItem
{
    public function __construct()
    {
        $this->uuid = (Uuid::v4())->toRfc4122();
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="bigint")
     */
    private int $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string", unique=true)
     * @var string
     */
    private string $uuid;

    /**
     * @param string $uuid
     * @return $this
     */
    public function setUuid(string $uuid) : self
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUuid() : ?string
    {
        return $this->uuid;
    }

    /**
     * @var Banner $banner
     * @ORM\ManyToOne(targetEntity="Banner")
     * @ORM\JoinColumn(name="banner_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private Banner $banner;

    /**
     * @param Banner $banner
     * @return self
     */
    public function setBanner(Banner $banner): self
    {
        $this->banner = $banner;
        return $this;
    }

    /**
     * @return Banner
     */
    public function getBanner(): Banner
    {
        return $this->banner;
    }

    /**
     * @ORM\Column(type="text")
     * @var string $image
     */
    private string $image;

    /**
     * @param string $image
     * @return self
     */
    public function setImage(string $image): self
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @ORM\Column(type="string", nullable=false)
     * @var string|null $title
     */
    private ?string $title;

    /**
     * @param string|null $title
     * @return self
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title ?? null;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string|null $subtitle
     */
    private ?string $subtitle;

    /**
     * @return string|null
     */
    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    /**
     * @param string|null $subtitle
     * @return self
     */
    public function setSubtitle(?string $subtitle): self
    {
        $this->subtitle = $subtitle;
        return $this;
    }

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string|null $subtitle
     */
    private ?string $link;

    /**
     * @return string|null
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @param string|null $link
     */
    public function setLink(?string $link): void
    {
        $this->link = $link;
    }

    /**
     * @ORM\Column(type="integer")
     * @var int $sort
     */
    private int $sort = 0;

    /**
     * @param int $sort
     * @return self
     */
    public function setSort(int $sort): self
    {
        $this->sort = $sort;
        return $this;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime $createAt
     */
    private \DateTime $createAt;

    /**
     * @return \DateTime|null
     */
    public function getCreateAt() : ?\DateTime
    {
        return $this->createAt;
    }

    /**
     * @ORM\PrePersist()
     * @return $this
     */
    public function setCreateAt() : self
    {
        $this->createAt = new \DateTime("now");
        return $this;
    }

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private \DateTime $updateAt;

    /**
     * @return \DateTime|null
     */
    public function getUpdateAt() : ?\DateTime
    {
        return $this->updateAt;
    }

    /**
     * @ORM\PreUpdate()
     * @ORM\PrePersist()
     * @return $this
     */
    public function setUpdateAt() : self
    {
        $this->updateAt = new \DateTime("now");
        return $this;
    }
}
