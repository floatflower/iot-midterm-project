<?php

namespace App\Entity;

use App\Repository\AppUserPunchRecordRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass=AppUserPunchRecordRepository::class)
 */
class AppUserPunchRecord
{
    public function __construct()
    {
        $this->uuid = (Uuid::v4())->toRfc4122();
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="bigint")
     */
    private int $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string", unique=true)
     * @var string
     */
    private string $uuid;

    /**
     * @param string $uuid
     * @return $this
     */
    public function setUuid(string $uuid) : self
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUuid() : ?string
    {
        return $this->uuid;
    }

    /**
     * @ORM\ManyToOne(targetEntity="AppUser")
     * @ORM\JoinColumn(name="app_user_id", referencedColumnName="id", onDelete="CASCADE")
     * @var AppUser $appUser
     */
    private AppUser $appUser;

    /**
     * @param AppUser $appUser
     * @return $this
     */
    public function setAppUser(AppUser $appUser) : self
    {
        $this->appUser = $appUser;
        return $this;
    }

    /**
     * @return AppUser|null
     */
    public function getAppUser() : ?AppUser
    {
        return $this->appUser;
    }

    const TYPE_PUNCH_IN = 1;

    /**
     * @var int $type
     * @ORM\Column(type="integer")
     */
    private int $type;

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime $createAt
     */
    private \DateTime $createAt;

    /**
     * @return \DateTime|null
     */
    public function getCreateAt() : ?\DateTime
    {
        return $this->createAt;
    }

    /**
     * @ORM\PrePersist()
     * @return $this
     */
    public function setCreateAt() : self
    {
        $this->createAt = new \DateTime("now");
        return $this;
    }

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private \DateTime $updateAt;

    /**
     * @return \DateTime|null
     */
    public function getUpdateAt() : ?\DateTime
    {
        return $this->updateAt;
    }

    /**
     * @ORM\PreUpdate()
     * @ORM\PrePersist()
     * @return $this
     */
    public function setUpdateAt() : self
    {
        $this->updateAt = new \DateTime("now");
        return $this;
    }
}
