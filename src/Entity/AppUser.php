<?php

namespace App\Entity;

use App\Repository\AppUserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=AppUserRepository::class)
 */
class AppUser implements UserInterface
{
    public function __construct()
    {
        $this->username = uniqid("user_");
        $this->uuid = (Uuid::v4())->toRfc4122();
        $this->setAsNeverExpireUser();
        $this->referralCode = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSDUVWXYZabcdefghijklmnopqrstuvwxyz"), 0, 7);
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="bigint")
     */
    private int $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string", unique=true)
     * @var string
     */
    private string $uuid;

    /**
     * @param string $uuid
     * @return $this
     */
    public function setUuid(string $uuid) : self
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUuid() : ?string
    {
        return $this->uuid;
    }

    /**
     * @ORM\Column(type="string", unique=true)
     * @var string $username
     */
    private string $username;

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername(string $username) : self
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @ORM\Column(type="string", unique=true, nullable=true)
     * @var string|null $email
     */
    private ?string $email;

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail(?string $email) : self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail() : ?string
    {
        return $this->email;
    }

    /**
     * @ORM\Column(type="boolean", name="is_email_verified", options={"default": false})
     * @var bool $emailVerified
     */
    private bool $emailVerified = false;

    /**
     * @param bool $emailVerified
     * @return $this
     */
    public function setEmailVerified(bool $emailVerified) : self
    {
        $this->emailVerified = $emailVerified;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEmailVerified() : bool
    {
        return $this->emailVerified;
    }

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string $passwordHash
     */
    private string $passwordHash;

    /**
     * @param string $passwordHash
     * @return $this
     */
    public function setPasswordHash(string $passwordHash) : self
    {
        $this->passwordHash = $passwordHash;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword() : ?string
    {
        return $this->passwordHash;
    }

    /**
     * @return string|null
     */
    public function getPasswordHash() : ?string
    {
        return $this->passwordHash;
    }

    /**
     * Other unique data
     */

    /**
     * @var string|null $mobile
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    private ?string $mobile;

    /**
     * @param string $mobile
     * @return $this
     */
    public function setMobile(?string $mobile) : self
    {
        $this->mobile = $mobile ?? null;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMobile() : ?string
    {
        return $this->mobile;
    }

    /**
     * @var bool $mobileVerified
     * @ORM\Column(type="boolean", name="is_mobile_verified", options={"default": false})
     */
    private bool $mobileVerified = false;

    /**
     * @param bool $mobileVerified
     * @return $this
     */
    public function setMobileVerified(bool $mobileVerified) : self
    {
        $this->mobileVerified = $mobileVerified;
        return $this;
    }

    /**
     * @return bool
     */
    public function isMobileVerified() : bool
    {
        return $this->mobileVerified;
    }

    /**
     * @ORM\Column(type="string", unique=true, nullable=true)
     * @var string|null $facebookId
     */
    private ?string $facebookId;

    /**
     * @param string|null $facebookId
     * @return $this
     */
    public function setFacebookId(?string $facebookId) : self
    {
        $this->facebookId = $facebookId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFacebookId() : ?string
    {
        return $this->facebookId;
    }

    /**
     * @ORM\Column(type="string", nullable=true, unique=true)
     * @var string|null
     */
    private ?string $googleId;

    public function setGoogleId(?string $googleId) : self
    {
        $this->googleId = $googleId ?? "";
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGoogleId() : ?string
    {
        return $this->googleId;
    }

    /**
     * @var string $referralCode
     * @ORM\Column(type="string", unique=true)
     */
    private string $referralCode;

    /**
     * @param string $referralCode
     * @return self
     */
    public function setReferralCode(string $referralCode): self
    {
        $this->referralCode = $referralCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getReferralCode(): string
    {
        return $this->referralCode;
    }

    /**
     * @var AppUser|null
     * @ORM\ManyToOne(targetEntity="AppUser")
     * @ORM\JoinColumn(name="referrer_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private ?AppUser $referrer;

    /**
     * @param AppUser|null $referrer
     * @return self
     */
    public function setReferrer(?AppUser $referrer): self
    {
        $this->referrer = $referrer;
        return $this;
    }

    /**
     * @return AppUser|null
     */
    public function getReferrer(): ?AppUser
    {
        return $this->referrer;
    }

    /**
     * @ORM\Column(type="boolean", name="is_admin", options={"default": false})
     * @var bool $admin
     */
    private bool $admin = false;

    /**
     * @param bool $admin
     * @return $this
     */
    public function setAdmin(bool $admin) : self
    {
        if(!$this->isDeveloper()) $this->admin = $admin;
        else $this->admin = true;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAdmin() : bool
    {
        return $this->admin;
    }

    /**
     * @var bool $developer
     * @ORM\Column(type="boolean", name="is_developer", options={"default": false})
     */
    private bool $developer = false;

    /**
     * @return bool
     */
    public function isDeveloper() : bool
    {
        return $this->developer;
    }

    /**
     * @param bool $developer
     * @return self
     */
    public function setDeveloper(bool $developer): self
    {
        $this->developer = $developer;
        return $this;
    }

    /**
     * @ORM\Column(type="datetime", options={"default": "9999-12-31"})
     * @var \DateTime $expireTime
     */
    private \DateTime $expireTime;

    /**
     * @param \DateTime $expireTime
     * @return self
     */
    public function setExpireTime(\DateTime $expireTime): self
    {
        if(!$this->isDeveloper()) $this->expireTime = $expireTime;
        else $this->setAsNeverExpireUser();
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExpireTime(): \DateTime
    {
        return $this->expireTime;
    }

    /**
     * @var string|null $avatar
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $avatar = null;

    /**
     * @param string|null $avatar
     * @return self
     */
    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    /**
     * @return bool
     */
    public function isNeverExpireUser() : bool
    {
        return $this->expireTime->format("Y-m-d") === "9999-12-31";
    }

    /**
     * @return self
     */
    public function setAsNeverExpireUser() : self
    {
        $this->expireTime = new \DateTime("9999-12-31");
        return $this;
    }

    /**
     * @var bool
     */
    private bool $anonymous = false;

    /**
     * @return bool
     */
    public function isAnonymous() : bool
    {
        return $this->anonymous;
    }

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime $createAt
     */
    private \DateTime $createAt;

    /**
     * @return \DateTime|null
     */
    public function getCreateAt() : ?\DateTime
    {
        return $this->createAt;
    }

    /**
     * @ORM\PrePersist()
     * @return $this
     */
    public function setCreateAt() : self
    {
        $this->createAt = new \DateTime("now");
        return $this;
    }

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private \DateTime $updateAt;

    /**
     * @return \DateTime|null
     */
    public function getUpdateAt() : ?\DateTime
    {
        return $this->updateAt;
    }

    /**
     * @ORM\PreUpdate()
     * @ORM\PrePersist()
     * @return $this
     */
    public function setUpdateAt() : self
    {
        $this->updateAt = new \DateTime("now");
        return $this;
    }

    public function getRoles()
    {
        if($this->anonymous) return array("ROLE_ANONYMOUS");

        $roles = array();
        if($this->developer) $roles[] = "ROLE_DEVELOPER";
        if($this->admin) $roles[] = "ROLE_ADMIN";
        $roles[] = "ROLE_USER";

        return $roles;
    }

    /**
     * Actions section
     */

    /**
     * @return null
     */
    public function getSalt()
    {
        return null;
    }
    public function eraseCredentials()
    {}

    /**
     * 創造一個未登入的用戶
     * @return AppUser
     */
    static public function makeAnonymous() : AppUser
    {
        $appUser = new AppUser();

        $appUser->username = "Anonymous";
        $appUser->anonymous = true;
        $appUser->admin = false;
        $appUser->setCreateAt();
        $appUser->setUpdateAt();

        return $appUser;
    }
}
