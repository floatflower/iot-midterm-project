<?php

namespace App\Entity;

use App\Repository\AppUserAuthTokenRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=AppUserAuthTokenRepository::class)
 */
class AppUserAuthToken
{
    public function __construct()
    {
        $this->session = hash("sha512", uniqid("session_"));
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    private string $tokenHash;

    /**
     * @param string $tokenHash
     * @return $this
     */
    public function setTokenHash(string $tokenHash) : self
    {
        $this->tokenHash = $tokenHash;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTokenHash() : ?string
    {
        return $this->tokenHash;
    }

    /**
     * @var string $session
     * @ORM\Column(type="string", nullable=true)
     */
    private string $session;

    /**
     * @param string $session
     * @return self
     */
    public function setSession(string $session): self
    {
        $this->session = $session;
        return $this;
    }

    /**
     * @return string
     */
    public function getSession(): string
    {
        return $this->session;
    }

    /**
     * @ORM\ManyToOne(targetEntity="AppUser")
     * @ORM\JoinColumn(name="app_user_id", referencedColumnName="id", onDelete="CASCADE")
     * @var AppUser $appUser
     */
    private AppUser $appUser;

    /**
     * @param AppUser $appUser
     * @return $this
     */
    public function setAppUser(AppUser $appUser) : self
    {
        $this->appUser = $appUser;
        return $this;
    }

    /**
     * @return AppUser|null
     */
    public function getAppUser() : ?AppUser
    {
        return $this->appUser;
    }

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime $createAt
     */
    private \DateTime $createAt;

    /**
     * @return \DateTime|null
     */
    public function getCreateAt() : ?\DateTime
    {
        return $this->createAt;
    }

    /**
     * @ORM\PrePersist()
     * @return $this
     */
    public function setCreateAt() : self
    {
        $this->createAt = new \DateTime("now");
        return $this;
    }

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime $expireAt
     */
    private \DateTime $expireAt;

    /**
     * @ORM\PrePersist()
     * @return self
     */
    public function setExpireAt(): self
    {
        $this->expireAt = (new \DateTime("now"))->add(new \DateInterval("P7D"));
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExpireAt(): \DateTime
    {
        return $this->expireAt;
    }

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private \DateTime $updateAt;

    /**
     * @return \DateTime|null
     */
    public function getUpdateAt() : ?\DateTime
    {
        return $this->updateAt;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     * @return $this
     */
    public function setUpdateAt() : self
    {
        $this->updateAt = new \DateTime("now");
        return $this;
    }

    /**
     * @var string $userAgent
     * @ORM\Column(type="text", nullable=true)
     */
    private string $userAgent;

    /**
     * @param string $userAgent
     * @return self
     */
    public function setUserAgent(string $userAgent): self
    {
        $this->userAgent = $userAgent;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserAgent(): string
    {
        return $this->userAgent;
    }

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $clientIp;

    /**
     * @param string|null $clientIp
     * @return self
     */
    public function setClientIp(?string $clientIp): self
    {
        $this->clientIp = $clientIp;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getClientIp(): ?string
    {
        return $this->clientIp;
    }
}
