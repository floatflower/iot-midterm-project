<?php

namespace App\Entity;

use App\Repository\ServiceRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass=ServiceRepository::class)
 */
class Service implements UserInterface
{
    public function __construct()
    {
        $this->uuid = (Uuid::v4())->toRfc4122();
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="bigint")
     */
    private int $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string", unique=true)
     * @var string
     */
    private string $uuid;

    /**
     * @param string $uuid
     * @return $this
     */
    public function setUuid(string $uuid) : self
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUuid() : ?string
    {
        return $this->uuid;
    }

    /**
     * @var string $tokenHash
     * @ORM\Column(type="string", unique=true)
     */
    private string $tokenHash;

    /**
     * @param string $tokenHash
     * @return $this
     */
    public function setTokenHash(string $tokenHash) : self
    {
        $this->tokenHash = $tokenHash;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTokenHash() : ?string
    {
        return $this->tokenHash;
    }

    /**
     * @var string $name
     * @ORM\Column(type="string")
     */
    private string $name;

    /**
     * @return string|null
     */
    public function getName() : ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name) : self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string|null $description
     */
    private ?string $description = null;

    /**
     * @return string|null
     */
    public function getDescription() : ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return $this
     */
    public function setDescription(?string $description) : self
    {
        $this->description = $description ?? null;
        return $this;
    }

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime $createAt
     */
    private \DateTime $createAt;

    /**
     * @return \DateTime|null
     */
    public function getCreateAt() : ?\DateTime
    {
        return $this->createAt;
    }

    /**
     * @ORM\PrePersist()
     * @return $this
     */
    public function setCreateAt() : self
    {
        $this->createAt = new \DateTime("now");
        return $this;
    }

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private \DateTime $updateAt;

    /**
     * @return \DateTime|null
     */
    public function getUpdateAt() : ?\DateTime
    {
        return $this->updateAt;
    }

    /**
     * @ORM\PreUpdate()
     * @ORM\PrePersist()
     * @return $this
     */
    public function setUpdateAt() : self
    {
        $this->updateAt = new \DateTime("now");
        return $this;
    }

    /**
     * @return string[]
     */
    public function getRoles()
    {
        return array("ROLE_SERVICE");
    }

    /**
     * @return string|null
     */
    public function getPassword()
    {
        return $this->tokenHash;
    }

    /**
     * @return string|null
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->uuid;
    }

    public function eraseCredentials()
    {}
}
