<?php

namespace App\Entity;

use App\Repository\AppUserMobileVerifyCodeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=AppUserMobileVerifyCodeRepository::class)
 */
class AppUserMobileVerifyCode
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    private $session;

    /**
     * @param mixed $session
     * @return self
     */
    public function setSession($session): self
    {
        $this->session = $session;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @ORM\ManyToOne(targetEntity="AppUser")
     * @ORM\JoinColumn(name="app_user_id", referencedColumnName="id", onDelete="CASCADE")
     * @var AppUser $appUser
     */
    private AppUser $appUser;

    /**
     * @param AppUser $appUser
     * @return self
     */
    public function setAppUser(AppUser $appUser): self
    {
        $this->appUser = $appUser;
        return $this;
    }

    /**
     * @return AppUser
     */
    public function getAppUser(): AppUser
    {
        return $this->appUser;
    }

    /**
     * @ORM\Column(type="string")
     * @var string $mobile
     */
    private string $mobile;

    /**
     * @param string $mobile
     * @return self
     */
    public function setMobile(string $mobile): self
    {
        $this->mobile = $mobile;
        return $this;
    }

    /**
     * @return string
     */
    public function getMobile(): string
    {
        return $this->mobile;
    }

    /**
     * @var string $code
     * @ORM\Column(type="string")
     */
    private string $codeHash;

    /**
     * @param string $codeHash
     * @return self
     */
    public function setCodeHash(string $codeHash): self
    {
        $this->codeHash = $codeHash;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodeHash(): string
    {
        return $this->codeHash;
    }

    /**
     * @ORM\Column(type="integer")
     * @var int $errorCount
     */
    private int $errorCount = 0;

    /**
     * @param int $errorCount
     * @return self
     */
    public function setErrorCount(int $errorCount): self
    {
        $this->errorCount = $errorCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getErrorCount(): int
    {
        return $this->errorCount;
    }

    /**
     * @return $this
     */
    public function incrementErrorCount() : self
    {
        $this->errorCount += $this->errorCount;
        return $this;
    }

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private \DateTime $createAt;

    /**
     * @ORM\PrePersist
     * @return self
     */
    public function setCreateAt(): self
    {
        $this->createAt = new \DateTime("now");
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreateAt(): \DateTime
    {
        return $this->createAt;
    }

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime $expireAt
     */
    private \DateTime $expireAt;

    /**
     * @ORM\PrePersist()
     * @return self
     */
    public function setExpireAt(): self
    {
        $this->expireAt = (new \DateTime("now"))->add(new \DateInterval("PT5M"));
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExpireAt(): \DateTime
    {
        return $this->expireAt;
    }
}
