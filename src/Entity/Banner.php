<?php

namespace App\Entity;

use App\Repository\BannerRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass=BannerRepository::class)
 */
class Banner
{
    public function __construct()
    {
        $this->uuid = (Uuid::v4())->toRfc4122();
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="bigint")
     */
    private int $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string", unique=true)
     * @var string
     */
    private string $uuid;

    /**
     * @param string $uuid
     * @return $this
     */
    public function setUuid(string $uuid) : self
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUuid() : ?string
    {
        return $this->uuid;
    }

    /**
     * @ORM\Column(type="string", unique=true)
     * @var string $alias
     */
    private string $alias;

    /**
     * @param string $alias
     * @return self
     */
    public function setAlias(string $alias): self
    {
        $this->alias = $alias;
        return $this;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias;
    }

    /**
     * @var string $name
     * @ORM\Column(type="string")
     */
    private string $name;

    /**
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime $createAt
     */
    private \DateTime $createAt;

    /**
     * @return \DateTime|null
     */
    public function getCreateAt() : ?\DateTime
    {
        return $this->createAt;
    }

    /**
     * @ORM\PrePersist()
     * @return $this
     */
    public function setCreateAt() : self
    {
        $this->createAt = new \DateTime("now");
        return $this;
    }

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private \DateTime $updateAt;

    /**
     * @return \DateTime|null
     */
    public function getUpdateAt() : ?\DateTime
    {
        return $this->updateAt;
    }

    /**
     * @ORM\PreUpdate()
     * @ORM\PrePersist()
     * @return $this
     */
    public function setUpdateAt() : self
    {
        $this->updateAt = new \DateTime("now");
        return $this;
    }
}
