<?php


namespace App\Event\Auth;


use App\Entity\AppUserResetPasswordToken;
use Symfony\Contracts\EventDispatcher\Event;

class ResetPasswordTokenExpiredEvent extends Event
{
    /**
     * @var AppUserResetPasswordToken $appUserResetPasswordToken
     */
    private AppUserResetPasswordToken $appUserResetPasswordToken;

    /**
     * @param AppUserResetPasswordToken $appUserResetPasswordToken
     */
    public function setAppUserResetPasswordToken(AppUserResetPasswordToken $appUserResetPasswordToken): void
    {
        $this->appUserResetPasswordToken = $appUserResetPasswordToken;
    }

    /**
     * @return AppUserResetPasswordToken
     */
    public function getAppUserResetPasswordToken(): AppUserResetPasswordToken
    {
        return $this->appUserResetPasswordToken;
    }
}