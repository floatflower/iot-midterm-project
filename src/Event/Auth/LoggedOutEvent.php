<?php


namespace App\Event\Auth;


use App\Entity\AppUser;
use App\Entity\AppUserAuthToken;
use Symfony\Contracts\EventDispatcher\Event;

class LoggedOutEvent extends Event
{
    private AppUser $appUser;

    /**
     * @param AppUser $appUser
     */
    public function setAppUser(AppUser $appUser): void
    {
        $this->appUser = $appUser;
    }

    /**
     * @return AppUser
     */
    public function getAppUser(): AppUser
    {
        return $this->appUser;
    }

    private AppUserAuthToken $appUserAuthToken;

    /**
     * @param AppUserAuthToken $appUserAuthToken
     */
    public function setAppUserAuthToken(AppUserAuthToken $appUserAuthToken): void
    {
        $this->appUserAuthToken = $appUserAuthToken;
    }

    /**
     * @return AppUserAuthToken
     */
    public function getAppUserAuthToken(): AppUserAuthToken
    {
        return $this->appUserAuthToken;
    }
}