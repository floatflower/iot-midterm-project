<?php


namespace App\Event\Auth;


use App\Entity\AppUser;
use Symfony\Contracts\EventDispatcher\Event;

class UserExpiredEvent extends Event
{
    /**
     * @var AppUser $appUser
     */
    private AppUser $appUser;

    /**
     * @param AppUser $appUser
     */
    public function setAppUser(AppUser $appUser): void
    {
        $this->appUser = $appUser;
    }

    /**
     * @return AppUser
     */
    public function getAppUser(): AppUser
    {
        return $this->appUser;
    }
}