<?php


namespace App\Event\Auth;


use App\Entity\AppUser;
use App\Entity\AppUserResetPasswordToken;
use Symfony\Component\EventDispatcher\EventDispatcher;

class ResetPasswordTokenIssuedEvent extends EventDispatcher
{
    /**
     * @var AppUser $appUser
     */
    private AppUser $appUser;

    /**
     * @return AppUser
     */
    public function getAppUser(): AppUser
    {
        return $this->appUser;
    }

    /**
     * @param AppUser $appUser
     */
    public function setAppUser(AppUser $appUser): void
    {
        $this->appUser = $appUser;
    }

    /**
     * @var AppUserResetPasswordToken $appUserResetPasswordToken
     */
    private AppUserResetPasswordToken $appUserResetPasswordToken;

    /**
     * @param AppUserResetPasswordToken $appUserResetPasswordToken
     */
    public function setAppUserResetPasswordToken(AppUserResetPasswordToken $appUserResetPasswordToken): void
    {
        $this->appUserResetPasswordToken = $appUserResetPasswordToken;
    }

    /**
     * @return AppUserResetPasswordToken
     */
    public function getAppUserResetPasswordToken(): AppUserResetPasswordToken
    {
        return $this->appUserResetPasswordToken;
    }

    /**
     * @var string $plainToken
     */
    private string $plainToken;

    /**
     * @param string $plainToken
     */
    public function setPlainToken(string $plainToken): void
    {
        $this->plainToken = $plainToken;
    }

    /**
     * @return string
     */
    public function getPlainToken(): string
    {
        return $this->plainToken;
    }
}