<?php


namespace App\Event\Auth;


use App\Entity\AppUser;
use App\Entity\AppUserEmailVerifyToken;
use Symfony\Contracts\EventDispatcher\Event;

class EmailVerifyTokenIssuedEvent extends Event
{
    /**
     * @var AppUser $appUser
     */
    private AppUser $appUser;

    /**
     * @param AppUser $appUser
     */
    public function setAppUser(AppUser $appUser): void
    {
        $this->appUser = $appUser;
    }

    /**
     * @return AppUser
     */
    public function getAppUser(): AppUser
    {
        return $this->appUser;
    }

    /**
     * @var AppUserEmailVerifyToken $appUserEmailVerifyToken
     */
    private AppUserEmailVerifyToken $appUserEmailVerifyToken;

    /**
     * @param AppUserEmailVerifyToken $appUserEmailVerifyToken
     */
    public function setAppUserEmailVerifyToken(AppUserEmailVerifyToken $appUserEmailVerifyToken): void
    {
        $this->appUserEmailVerifyToken = $appUserEmailVerifyToken;
    }

    /**
     * @return AppUserEmailVerifyToken
     */
    public function getAppUserEmailVerifyToken(): AppUserEmailVerifyToken
    {
        return $this->appUserEmailVerifyToken;
    }

    /**
     * @var string $plainToken
     */
    private string $plainToken;

    /**
     * @param string $plainToken
     */
    public function setPlainToken(string $plainToken): void
    {
        $this->plainToken = $plainToken;
    }

    /**
     * @return string
     */
    public function getPlainToken(): string
    {
        return $this->plainToken;
    }
}