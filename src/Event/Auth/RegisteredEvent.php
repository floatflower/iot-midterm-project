<?php


namespace App\Event\Auth;


use Symfony\Contracts\EventDispatcher\Event;

class RegisteredEvent extends Event
{
}