<?php


namespace App\Event\Auth;


use App\Entity\AppUserEmailVerifyToken;
use Symfony\Contracts\EventDispatcher\Event;

class EmailVerifyTokenExpiredEvent extends Event
{
    /**
     * @var AppUserEmailVerifyToken $appUserEmailVerifyToken
     */
    private AppUserEmailVerifyToken $appUserEmailVerifyToken;

    /**
     * @param AppUserEmailVerifyToken $appUserEmailVerifyToken
     */
    public function setAppUserEmailVerifyToken(AppUserEmailVerifyToken $appUserEmailVerifyToken): void
    {
        $this->appUserEmailVerifyToken = $appUserEmailVerifyToken;
    }

    /**
     * @return AppUserEmailVerifyToken
     */
    public function getAppUserEmailVerifyToken(): AppUserEmailVerifyToken
    {
        return $this->appUserEmailVerifyToken;
    }
}