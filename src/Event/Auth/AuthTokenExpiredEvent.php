<?php


namespace App\Event\Auth;


use App\Entity\AppUserAuthToken;
use Symfony\Contracts\EventDispatcher\Event;

class AuthTokenExpiredEvent extends Event
{
    /**
     * @var AppUserAuthToken $appUserToken
     */
    private AppUserAuthToken $appUserToken;

    /**
     * @param AppUserAuthToken $appUserToken
     */
    public function setAppUserToken(AppUserAuthToken $appUserToken): void
    {
        $this->appUserToken = $appUserToken;
    }

    /**
     * @return AppUserAuthToken
     */
    public function getAppUserToken(): AppUserAuthToken
    {
        return $this->appUserToken;
    }
}