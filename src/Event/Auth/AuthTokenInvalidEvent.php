<?php


namespace App\Event\Auth;


use Symfony\Contracts\EventDispatcher\Event;

class AuthTokenInvalidEvent extends Event
{
}