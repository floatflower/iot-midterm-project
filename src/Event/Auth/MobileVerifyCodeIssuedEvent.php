<?php


namespace App\Event\Auth;


use App\Entity\AppUser;
use App\Entity\AppUserMobileVerifyCode;

class MobileVerifyCodeIssuedEvent
{
    private AppUser $appUser;

    /**
     * @param AppUser $appUser
     */
    public function setAppUser(AppUser $appUser): void
    {
        $this->appUser = $appUser;
    }

    /**
     * @return AppUser
     */
    public function getAppUser(): AppUser
    {
        return $this->appUser;
    }

    /**
     * @var AppUserMobileVerifyCode $appUserMobileVerifyCode
     */
    private AppUserMobileVerifyCode $appUserMobileVerifyCode;

    /**
     * @param AppUserMobileVerifyCode $appUserMobileVerifyCode
     */
    public function setAppUserMobileVerifyCode(AppUserMobileVerifyCode $appUserMobileVerifyCode): void
    {
        $this->appUserMobileVerifyCode = $appUserMobileVerifyCode;
    }

    /**
     * @return AppUserMobileVerifyCode
     */
    public function getAppUserMobileVerifyCode(): AppUserMobileVerifyCode
    {
        return $this->appUserMobileVerifyCode;
    }

    /**
     * @var string $code
     */
    private string $code;

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }
}