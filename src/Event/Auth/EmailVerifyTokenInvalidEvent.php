<?php


namespace App\Event\Auth;


use Symfony\Contracts\EventDispatcher\Event;

class EmailVerifyTokenInvalidEvent extends Event
{
}