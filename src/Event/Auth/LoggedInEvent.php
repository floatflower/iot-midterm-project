<?php


namespace App\Event\Auth;


use App\Entity\AppUser;
use App\Entity\AppUserAuthToken;
use Symfony\Contracts\EventDispatcher\Event;

class LoggedInEvent extends Event
{
    /**
     * @var AppUser $appUser
     */
    private AppUser $appUser;

    /**
     * @param AppUser $appUser
     */
    public function setAppUser(AppUser $appUser): void
    {
        $this->appUser = $appUser;
    }

    /**
     * @return AppUser
     */
    public function getAppUser(): AppUser
    {
        return $this->appUser;
    }

    /**
     * @var AppUserAuthToken $appUserAuthToken
     */
    private AppUserAuthToken $appUserAuthToken;

    /**
     * @param AppUserAuthToken $appUserAuthToken
     */
    public function setAppUserAuthToken(AppUserAuthToken $appUserAuthToken): void
    {
        $this->appUserAuthToken = $appUserAuthToken;
    }

    /**
     * @return AppUserAuthToken
     */
    public function getAppUserAuthToken(): AppUserAuthToken
    {
        return $this->appUserAuthToken;
    }

    /**
     * @var string $plainToken
     */
    private string $plainToken;

    /**
     * @param string $plainToken
     */
    public function setPlainToken(string $plainToken): void
    {
        $this->plainToken = $plainToken;
    }

    /**
     * @return string
     */
    public function getPlainToken(): string
    {
        return $this->plainToken;
    }
}