<?php


namespace App\Event\Entity\BannerItem;


use App\Entity\BannerItem;
use Symfony\Contracts\EventDispatcher\Event;

class UpdatedEvent extends Event
{
    private BannerItem $bannerItem;

    /**
     * @param BannerItem $bannerItem
     */
    public function setBannerItem(BannerItem $bannerItem): void
    {
        $this->bannerItem = $bannerItem;
    }

    /**
     * @return BannerItem
     */
    public function getBannerItem(): BannerItem
    {
        return $this->bannerItem;
    }
}