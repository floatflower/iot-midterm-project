<?php


namespace App\Event\Entity\AppSetting;


use App\Entity\AppSetting;
use Symfony\Contracts\EventDispatcher\Event;

class UpdatedEvent extends Event
{
    /**
     * @var AppSetting $appSetting
     */
    private AppSetting $appSetting;

    /**
     * @param AppSetting $appSetting
     */
    public function setAppSetting(AppSetting $appSetting): void
    {
        $this->appSetting = $appSetting;
    }

    /**
     * @return AppSetting
     */
    public function getAppSetting(): AppSetting
    {
        return $this->appSetting;
    }
}