<?php


namespace App\Event\Entity\AppUser;


use App\Entity\AppUser;
use Symfony\Contracts\EventDispatcher\Event;

class PasswordUpdatedEvent extends Event
{
    /**
     * @var AppUser
     */
    private AppUser $appUser;

    /**
     * @param AppUser $appUser
     */
    public function setAppUser(AppUser $appUser): void
    {
        $this->appUser = $appUser;
    }

    /**
     * @return AppUser
     */
    public function getAppUser(): AppUser
    {
        return $this->appUser;
    }
}