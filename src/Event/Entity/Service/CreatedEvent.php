<?php


namespace App\Event\Entity\Service;


use App\Entity\Service;
use Symfony\Contracts\EventDispatcher\Event;

class CreatedEvent extends Event
{
    private Service $service;

    /**
     * @param Service $service
     */
    public function setService(Service $service): void
    {
        $this->service = $service;
    }

    /**
     * @return Service
     */
    public function getService(): Service
    {
        return $this->service;
    }
}