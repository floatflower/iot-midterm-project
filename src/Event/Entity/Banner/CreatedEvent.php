<?php


namespace App\Event\Entity\Banner;


use App\Entity\Banner;
use Symfony\Contracts\EventDispatcher\Event;

class CreatedEvent extends Event
{
    private Banner $banner;

    /**
     * @param Banner $banner
     */
    public function setBanner(Banner $banner): void
    {
        $this->banner = $banner;
    }

    /**
     * @return Banner
     */
    public function getBanner(): Banner
    {
        return $this->banner;
    }
}