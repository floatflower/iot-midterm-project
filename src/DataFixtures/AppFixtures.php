<?php

namespace App\DataFixtures;

use App\Entity\AppUser;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        echo (
            "======================================================================================\n".
            "Notice:\n".
            "DataFixtures has been deprecated in this project\n".
            "For test environment use: php bin/console app:setup:test\n".
            "For develop environment use: php bin/console app:setup:dev\n".
            "For production environment use: php bin/console app:setup:prod\n".
            "======================================================================================"
        );
    }
}
