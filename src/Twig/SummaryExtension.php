<?php

namespace App\Twig;

use App\Entity\AppUser;
use App\Entity\Service;
use App\Repository\AppUserRepository;
use App\Repository\ServiceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class SummaryExtension extends AbstractExtension
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getFilters()
    {
        return [
            new TwigFilter("punchType", [$this, "getPunchType"])
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('count_users', [$this, 'countUsers']),
            new TwigFunction('count_services', [$this, 'countServices']),
        ];
    }

    public function countUsers() : int
    {
        /**
         * @var AppUserRepository $appUserRepository
         */
        $appUserRepository = $this->entityManager->getRepository(AppUser::class);

        return $appUserRepository->count(array());
    }

    public function countServices() : int
    {
        /**
         * @var ServiceRepository $serviceRepository
         */
        $serviceRepository = $this->entityManager->getRepository(Service::class);

        return $serviceRepository->count(array());
    }

    public function getPunchType($type) : string
    {
        if($type == 1) return "Punch In";

        return "Unknown";
    }
}
