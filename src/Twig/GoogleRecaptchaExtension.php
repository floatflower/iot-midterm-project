<?php

namespace App\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class GoogleRecaptchaExtension extends AbstractExtension
{
    /**
     * @var ContainerInterface $container
     */
    private ContainerInterface $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_google_recaptcha_site_key', [$this, 'getGoogleRecaptchaSiteKey']),
        ];
    }

    /**
     * @return string
     */
    public function getGoogleRecaptchaSiteKey(): string
    {
        return $this->container->getParameter("GOOGLE_RECAPTCHA_SITE_KEY") ?? "";
    }
}
