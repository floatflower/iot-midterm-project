<?php

namespace App\Twig;

use App\Entity\AppSetting;
use App\Repository\AppSettingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppSettingExtension extends AbstractExtension
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('app_setting', [$this, 'getAppSetting']),
        ];
    }

    /**
     * @param string $key
     * @return string|null
     */
    public function getAppSetting(string $key) : ?string
    {
        /**
         * @var AppSettingRepository $appSettingRepository
         */
        $appSettingRepository = $this->entityManager->getRepository(AppSetting::class);

        $appSetting = $appSettingRepository->findOneByKey($key);

        return $appSetting ? $appSetting->getValue() : null;
    }
}
