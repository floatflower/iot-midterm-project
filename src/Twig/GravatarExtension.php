<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class GravatarExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('gravatar', [$this, 'getAvatar']),
        ];
    }

    /**
     * @param string $email
     * @param int $size
     * @param string $default
     * @return string
     */
    public function getAvatar(string $email, int $size = 120, string $default = "mm"): string
    {
        $emailHash = md5($email);
        return "https://www.gravatar.com/avatar/$emailHash?d=$default&s=$size";
    }
}
