<?php


namespace App\Service\GoogleRecaptcha\Result;


class VerifyResult
{
    /**
     * @var bool $success
     */
    private bool $success = false;

    /**
     * @param bool $success
     * @return $this
     */
    public function setSuccess(bool $success) : self
    {
        $this->success = $success;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSuccess() : bool
    {
        return $this->success;
    }

    /**
     * @var \DateTime|null $challengeTs
     */
    private ?\DateTime $challengeTs = null;

    /**
     * @param string $challengeTs
     * @return $this
     */
    public function setChallengeTs(string $challengeTs) : self
    {
        try {
            $this->challengeTs = new \DateTime($challengeTs);
        } catch (\Exception $exception) {}
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getChallengeTs() : ?\DateTime
    {
        return $this->challengeTs;
    }

    /**
     * @var string|null $hostname
     */
    private ?string $hostname = null;

    /**
     * @param string $hostname
     * @return $this
     */
    public function setHostname(string $hostname) : self
    {
        $this->hostname = $hostname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getHostname() : ?string
    {
        return $this->hostname;
    }

    /**
     * @var string|null $apkPackageName
     */
    private ?string $apkPackageName = null;

    /**
     * @param string $apkPackageName
     * @return $this
     */
    public function setApkPackageName(string $apkPackageName) : self
    {
        $this->apkPackageName = $apkPackageName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getApkPackageName() : ?string
    {
        return $this->apkPackageName;
    }

    /**
     * @var string[] $errorCodes
     */
    private array $errorCodes = array();

    /**
     * @param array $errorCodes
     * @return $this
     */
    public function setErrorCodes(array $errorCodes) : self
    {
        $this->errorCodes = $errorCodes;
        return $this;
    }

    /**
     * @return array|string[]
     */
    public function getErrorCodes() : array
    {
        return $this->errorCodes;
    }
}