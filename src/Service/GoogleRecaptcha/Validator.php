<?php


namespace App\Service\GoogleRecaptcha;


use App\Entity\AppSetting;
use App\Repository\AppSettingRepository;
use App\Service\Entity\AppSetting\AppSettingKey;
use App\Service\Entity\AppSetting\UpdateService;
use App\Service\GoogleRecaptcha\Result\VerifyResult;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Validator
{
    /**
     * @var HttpClientInterface $httpClient
     */
    private HttpClientInterface $httpClient;

    /**
     * @var ContainerInterface $container
     */
    private ContainerInterface $container;

    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    public function __construct(HttpClientInterface $httpClient,
                                EntityManagerInterface $entityManager,
                                ContainerInterface $container)
    {
        $this->httpClient = $httpClient;
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    /**
     * @param string $googleRecaptchaSecret
     * @return $this
     */
    public function setGoogleRecaptchaSecret(string $googleRecaptchaSecret) : self
    {
        $this->googleRecaptchaSiteSecret = $googleRecaptchaSecret;
        return $this;
    }

    /**
     * @param string $response
     * @param string|null $remoteIP
     * @param string|null $secret
     * @return VerifyResult|null;
     */
    public function verify(string $response, string $remoteIP = null, string $secret = null) : ?VerifyResult
    {
        $_secret = $secret;
        if(is_null($_secret)) {
            /**
             * @var AppSettingRepository $appSettingRepository
             */
            $appSettingRepository = $this->entityManager->getRepository(AppSetting::class);
            $googleRecaptchaSiteSecret = $appSettingRepository->findOneByKey(AppSettingKey::GOOGLE_RECAPTCHA_SITE_SECRET);

            $_secret = $googleRecaptchaSiteSecret ? $googleRecaptchaSiteSecret->getValue() : "";
        }

        $requestUri = "https://www.google.com/recaptcha/api/siteverify?secret=$_secret&response=$response";

        if(!is_null($remoteIP))
            $requestUri .= "&remoteip=$remoteIP";

        $verifyResult = new VerifyResult();

        try {
            $response = $this->httpClient->request(
                "POST",
                $requestUri
            );

            $responseData = json_decode($response->getContent(), true);

            $verifyResult->setSuccess($responseData["success"]);
            $verifyResult->setChallengeTs($responseData["challenge_ts"]);

            if(array_key_exists("error-codes", $responseData))
                $verifyResult->setErrorCodes($responseData["error-codes"]);

            if(array_key_exists("hostname", $responseData))
                $verifyResult->setHostname($responseData["hostname"]);

            if(array_key_exists("apk_package_name", $responseData))
                $verifyResult->setApkPackageName($responseData["apk_package_name"]);

            return $verifyResult;
        } catch (TransportExceptionInterface $e) {
        } catch (ClientExceptionInterface $e) {
        } catch (RedirectionExceptionInterface $e) {
        } catch (ServerExceptionInterface $e) {
        }

        return $verifyResult;
    }
}