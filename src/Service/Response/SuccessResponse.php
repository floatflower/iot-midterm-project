<?php


namespace App\Service\Response;

use Symfony\Component\HttpFoundation\Response;

class SuccessResponse extends Response
{
    public function __construct(array $data = array(), int $status = 200, array $headers = [])
    {
        $response = array(
            "error" => false,
            "error_code" => 0,
            "data" => $data,
            "time" => time()
        );
        parent::__construct(json_encode($response), $status, $headers);
    }
}