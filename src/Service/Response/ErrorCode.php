<?php


namespace App\Service\Response;


class ErrorCode
{
    const UNKNOWN_ERROR = 100000;
    const AUTHENTICATION_FAILED = 100001;
    const ACCESS_DENIED = 100002;
    const AUTH_TOKEN_INVALID = 100003;
    const SERVICE_TOKEN_INVALID = 100004;
    const JSON_FORMAT_INVALID = 100005;
    const FORM_FORMAT_INVALID = 100006;

    const SERVICE_ERROR = 101001;
    const SERVICE_NOT_FOUND = 101002;
    const SERVICE_NAME_IS_REQUIRED = 101003;

    const APP_USER_ERROR = 102001;
    const APP_USER_NOT_FOUND = 102002;

    const APP_SETTING_ERROR = 103001;

    // 預留一些空間給可能比較系統級的錯誤碼
    const BANNER_ERROR = 110001;

    const BANNER_ITEM_ERROR = 111001;
    const BANNER_ITEM_NOT_FOUND = 111002;
}