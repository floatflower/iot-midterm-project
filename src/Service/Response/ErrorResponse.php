<?php


namespace App\Service\Response;

use Symfony\Component\HttpFoundation\Response;

class ErrorResponse extends Response
{
    public function __construct(int $errorCode = ErrorCode::UNKNOWN_ERROR, int $status = 200, array $data = array(), array $headers = [])
    {
        $response = array(
            "error" => true,
            "error_code" => $errorCode,
            "data" => $data,
            "time" => time()
        );
        parent::__construct(json_encode($response), $status, $headers);
    }

}