<?php


namespace App\Service\Email;


use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Environment;

class EmailVerifyEmailSender
{
    /**
     * @var \Swift_Mailer $swiftMailer
     */
    private \Swift_Mailer $swiftMailer;

    /**
     * @var Environment $environment
     */
    private Environment $environment;

    /**
     * @var ContainerInterface $container
     */
    private ContainerInterface $container;

    public function __construct(\Swift_Mailer $swiftMailer,
                                Environment $environment,
                                ContainerInterface $container)
    {
        $this->swiftMailer = $swiftMailer;
        $this->environment = $environment;
        $this->container = $container;
    }

    public function send(array $data, string $to, string $from = null) {

        $fromAddress = $from ?: $this->container->getParameter("MAILER_DEFAULT_FROM");

        $body = $this->environment->render("email/email-verify.html.twig", $data);

        $message = (new \Swift_Message("驗證您的電子郵件"))
            ->setFrom($fromAddress)
            ->setTo($to)
            ->setBody($body, "text/html");

        return $this->swiftMailer->send($message);

    }
}