<?php


namespace App\Service\SMS\Result;


class SMSGetSendResult
{
    /**
     * @var string $messageId
     */
    private string $messageId;

    /**
     * @param string $messageId
     * @return $this
     */
    public function setMessageId(string $messageId) : self
    {
        $this->messageId = $messageId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMessageId() : ?string
    {
        return $this->messageId;
    }

    /**
     * @var int $pointsUsed
     */
    private int $pointsUsed;

    /**
     * @param int $pointsUsed
     * @return $this
     */
    public function setPointsUsed(int $pointsUsed) : self
    {
        $this->pointsUsed = $pointsUsed;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPointsUsed() : ?int
    {
        return $this->pointsUsed;
    }

    /**
     * @var int $pointsLeft
     */
    private int $pointsLeft;

    /**
     * @param int $pointsLeft
     * @return $this
     */
    public function setPointsLeft(int $pointsLeft) : self
    {
        $this->pointsLeft = $pointsLeft;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPointsLeft() : ?int
    {
        return $this->pointsLeft;
    }
}