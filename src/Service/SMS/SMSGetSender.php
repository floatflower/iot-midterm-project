<?php


namespace App\Service\SMS;


use App\Entity\AppSetting;
use App\Repository\AppSettingRepository;
use App\Service\Entity\AppSetting\AppSettingKey;
use App\Service\SMS\Exception\SMSGet\IPInaccessibleException;
use App\Service\SMS\Exception\SMSGet\MessageTooLongException;
use App\Service\SMS\Exception\SMSGet\NoReceiverException;
use App\Service\SMS\Exception\SMSGet\OutOfPointsException;
use App\Service\SMS\Exception\SMSGet\ParameterErrorException;
use App\Service\SMS\Exception\SMSGet\ScheduleTimeExpiredException;
use App\Service\SMS\Exception\SMSGet\ScheduleTimeParameterErrorException;
use App\Service\SMS\Exception\SMSGet\TooManyReceiversException;
use App\Service\SMS\Exception\SMSGet\UsernameOrPasswordInvalidException;
use App\Service\SMS\Result\SMSGetSendResult;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SMSGetSender
{
    /**
     * @var HttpClientInterface $httpClient
     */
    private HttpClientInterface $httpClient;

    /**
     * @var ContainerInterface $container
     */
    private ContainerInterface $container;

    public function __construct(ContainerInterface $container,
                                HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
        $this->container = $container;
    }

    /**
     * @param SMSGetMessage $smsgetMessage
     * @return SMSGetSendResult
     * @throws IPInaccessibleException
     * @throws MessageTooLongException
     * @throws NoReceiverException
     * @throws OutOfPointsException
     * @throws ParameterErrorException
     * @throws ScheduleTimeExpiredException
     * @throws ScheduleTimeParameterErrorException
     * @throws TooManyReceiversException
     * @throws UsernameOrPasswordInvalidException
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function send(SMSGetMessage $smsgetMessage)
    {
        $smsgetUsername = $this->container->getParameter("SMSGET_USERNAME");
        $smsgetPassword = $this->container->getParameter("SMSGET_PASSWORD");

        $username = $smsgetUsername;
        $password = $smsgetPassword;

        $message = $smsgetMessage->getMessage();
        $number = $smsgetMessage->getNumber();

        $requestUri = "http://sms-get.com/api_send.php?username=$username&password=$password&method=1&sms_msg=$message&phone=$number";

        $response = $this->httpClient->request(
            "GET",
            $requestUri
        );

        $responseData = json_decode($response->getContent(), true);

        if($responseData["stats"] === "False") {
            switch ($responseData["error_code"]) {
                case "001":
                    throw new ParameterErrorException();
                case "002":
                    throw new ScheduleTimeParameterErrorException();
                case "003":
                    throw new ScheduleTimeExpiredException();
                case "004":
                    throw new MessageTooLongException();
                case "005":
                    throw new UsernameOrPasswordInvalidException();
                case "006":
                    throw new IPInaccessibleException();
                case "007":
                    throw new NoReceiverException();
                case "008":
                    throw new TooManyReceiversException();
                case "009":
                    throw new OutOfPointsException();
            }
        }

        $errorMessage = $responseData["error_msg"];
        $messages = explode("|", $errorMessage);

        $sendResult = new SMSGetSendResult();
        $sendResult->setMessageId($messages[0]);
        $sendResult->setPointsUsed(intval($messages[1]));
        $sendResult->setPointsLeft(intval($messages[2]));

        return $sendResult;
    }
}