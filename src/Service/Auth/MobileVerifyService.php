<?php


namespace App\Service\Auth;


use App\Entity\AppUser;
use App\Entity\AppUserMobileVerifyCode;
use App\Event\Auth\MobileVerifyCodeExpiredEvent;
use App\Event\Auth\MobileVerifyCodeInvalidEvent;
use App\Event\Auth\MobileVerifyCodeIssuedEvent;
use App\Event\Auth\MobileVerifyCodeNotMatchEvent;
use App\Event\Auth\MobileVerifyCodeTryTooManyTimesEvent;
use App\Event\Entity\AppUser\MobileVerifiedEvent;
use App\Repository\AppUserMobileVerifyCodeRepository;
use App\Service\Auth\Exception\MobileVerifyCodeExpiredException;
use App\Service\Auth\Exception\MobileVerifyCodeInvalidException;
use App\Service\Auth\Exception\MobileVerifyCodeNotMatchException;
use App\Service\Auth\Exception\MobileVerifyCodeTryTooManyTimesException;
use App\Service\Auth\Result\IssueMobileVerifyCodeResult;
use App\Service\Auth\Result\MobileVerifyResult;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class MobileVerifyService
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param AppUser $appUser
     * @param string $mobile
     * @return IssueMobileVerifyCodeResult|null
     */
    public function issueVerifyCode(AppUser $appUser, string $mobile) : ?IssueMobileVerifyCodeResult
    {
        $session = hash("sha512", uniqid("mobile_code_"));
        $code = rand(100000, 999999)."";
        $codeHash = password_hash($code, PASSWORD_BCRYPT);

        $appUserMobileVerifyCode = new AppUserMobileVerifyCode();
        $appUserMobileVerifyCode->setAppUser($appUser);
        $appUserMobileVerifyCode->setSession($session);
        $appUserMobileVerifyCode->setCodeHash($codeHash);
        $appUserMobileVerifyCode->setAppUser($appUser);
        $appUserMobileVerifyCode->setErrorCount(0);
        $appUserMobileVerifyCode->setMobile($mobile);

        $this->entityManager->persist($appUserMobileVerifyCode);
        $this->entityManager->flush();

        $issueMobileVerifyCodeResult = new IssueMobileVerifyCodeResult();
        $issueMobileVerifyCodeResult->setAppUser($appUser);
        $issueMobileVerifyCodeResult->setAppUserMobileVerifyCode($appUserMobileVerifyCode);
        $issueMobileVerifyCodeResult->setCode($code);

        $mobileVerifyCodeIssuedEvent = new MobileVerifyCodeIssuedEvent();
        $mobileVerifyCodeIssuedEvent->setCode($code);
        $mobileVerifyCodeIssuedEvent->setAppUser($appUser);
        $mobileVerifyCodeIssuedEvent->setAppUserMobileVerifyCode($appUserMobileVerifyCode);
        $this->eventDispatcher->dispatch($mobileVerifyCodeIssuedEvent);

        return $issueMobileVerifyCodeResult;

    }

    /**
     * @param string $session
     * @param string $code
     * @return MobileVerifyResult|null
     * @throws MobileVerifyCodeExpiredException
     * @throws MobileVerifyCodeInvalidException
     * @throws MobileVerifyCodeNotMatchException
     * @throws MobileVerifyCodeTryTooManyTimesException
     */
    public function verifyByCode(string $session, string $code) : ?MobileVerifyResult
    {
        /**
         * @var AppUserMobileVerifyCodeRepository $appUserMobileVerifyCodeRepository
         */
        $appUserMobileVerifyCodeRepository = $this->entityManager->getRepository(AppUserMobileVerifyCode::class);
        $appUserMobileVerifyCode = $appUserMobileVerifyCodeRepository->findOneBy(array("session" => $session));

        if(!$appUserMobileVerifyCode) {

            $mobileVerifyCodeInvalidEvent = new MobileVerifyCodeInvalidEvent();
            $this->eventDispatcher->dispatch($mobileVerifyCodeInvalidEvent);

            throw new MobileVerifyCodeInvalidException();
        }

        $appUser = $appUserMobileVerifyCode->getAppUser();

        $currentTime = new \DateTime("now");

        if($currentTime > $appUserMobileVerifyCode->getExpireAt()) {

            $mobileVerifyCodeExpiredEvent = new MobileVerifyCodeExpiredEvent();
            $mobileVerifyCodeExpiredEvent->setAppUser($appUser);
            $mobileVerifyCodeExpiredEvent->setAppUserMobileVerifyCode($appUserMobileVerifyCode);

            throw new MobileVerifyCodeExpiredException();
        }

        if($appUserMobileVerifyCode->getErrorCount() > 3) {

            $mobileVerifyCodeTryTooMuchTimeEvent = new MobileVerifyCodeTryTooManyTimesEvent();
            $mobileVerifyCodeTryTooMuchTimeEvent->setAppUser($appUser);
            $mobileVerifyCodeTryTooMuchTimeEvent->setAppUserMobileVerifyCode($appUserMobileVerifyCode);
            $this->eventDispatcher->dispatch($mobileVerifyCodeTryTooMuchTimeEvent);

            throw new MobileVerifyCodeTryTooManyTimesException();
        }

        if(!password_verify($code, $appUserMobileVerifyCode->getCodeHash())) {

            $appUserMobileVerifyCode->incrementErrorCount();
            $this->entityManager->persist($appUserMobileVerifyCode);
            $this->entityManager->flush();

            $mobileVerifyCodeNotMatchEvent = new MobileVerifyCodeNotMatchEvent();
            $mobileVerifyCodeNotMatchEvent->setAppUser($appUser);
            $mobileVerifyCodeNotMatchEvent->setAppUserMobileVerifyCode($appUserMobileVerifyCode);

            throw new MobileVerifyCodeNotMatchException();
        }

        $appUser = $appUserMobileVerifyCode->getAppUser();

        $appUser->setMobile($appUserMobileVerifyCode->getMobile());
        $this->entityManager->persist($appUser);
        $this->entityManager->remove($appUserMobileVerifyCode);
        $this->entityManager->flush();

        return $this->verify($appUser);
    }

    /**
     * @param AppUser $appUser
     * @return MobileVerifyResult|null
     */
    public function verify(AppUser $appUser) : ?MobileVerifyResult
    {
        $appUser->setMobileVerified(true);

        $this->entityManager->persist($appUser);
        $this->entityManager->flush();

        $mobileVerifyResult = new MobileVerifyResult();
        $mobileVerifyResult->setAppUser($appUser);

        $mobileVerifiedEvent = new MobileVerifiedEvent();
        $mobileVerifiedEvent->setAppUser($appUser);
        $this->eventDispatcher->dispatch($mobileVerifiedEvent);

        return $mobileVerifyResult;

    }
}