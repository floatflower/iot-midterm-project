<?php


namespace App\Service\Auth\Result;


use App\Entity\AppUser;
use App\Entity\AppUserOneTimePin;

class IssueOneTimePinResult
{
    /**
     * @var AppUser $appUser
     */
    private AppUser $appUser;

    /**
     * @return AppUser
     */
    public function getAppUser(): AppUser
    {
        return $this->appUser;
    }

    /**
     * @param AppUser $appUser
     */
    public function setAppUser(AppUser $appUser): void
    {
        $this->appUser = $appUser;
    }

    /**
     * @var AppUserOneTimePin $appUserOneTimePin
     */
    private AppUserOneTimePin $appUserOneTimePin;

    /**
     * @return AppUserOneTimePin
     */
    public function getAppUserOneTimePin(): AppUserOneTimePin
    {
        return $this->appUserOneTimePin;
    }

    /**
     * @param AppUserOneTimePin $appUserOneTimePin
     */
    public function setAppUserOneTimePin(AppUserOneTimePin $appUserOneTimePin): void
    {
        $this->appUserOneTimePin = $appUserOneTimePin;
    }

    /**
     * @var string $code
     */
    private string $code;

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }
}