<?php


namespace App\Service\Auth\Result;


use App\Entity\AppUser;
use App\Entity\AppUserMobileVerifyCode;

class IssueMobileVerifyCodeResult
{
    private AppUserMobileVerifyCode $appUserMobileVerifyCode;

    /**
     * @param AppUserMobileVerifyCode $appUserMobileVerifyCode
     */
    public function setAppUserMobileVerifyCode(AppUserMobileVerifyCode $appUserMobileVerifyCode): void
    {
        $this->appUserMobileVerifyCode = $appUserMobileVerifyCode;
    }

    /**
     * @return AppUserMobileVerifyCode
     */
    public function getAppUserMobileVerifyCode(): AppUserMobileVerifyCode
    {
        return $this->appUserMobileVerifyCode;
    }

    /**
     * @var AppUser $appUser
     */
    private AppUser $appUser;

    /**
     * @param AppUser $appUser
     * @return self
     */
    public function setAppUser(AppUser $appUser): self
    {
        $this->appUser = $appUser;
        return $this;
    }

    /**
     * @return AppUser
     */
    public function getAppUser(): AppUser
    {
        return $this->appUser;
    }

    /**
     * @var string $code
     */
    private string $code;

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }
}