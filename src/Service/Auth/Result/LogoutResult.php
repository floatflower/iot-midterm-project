<?php

namespace App\Service\Auth\Result;


use App\Entity\AppUserAuthToken;

class LogoutResult
{
    private ?AppUserAuthToken $appUserAuthToken = null;

    public function setAppUserAuthToken(AppUserAuthToken $appUserAuthToken) : self
    {
        $this->appUserAuthToken = $appUserAuthToken;
        return $this;
    }

    public function getAppUserAuthToken() : ?AppUserAuthToken
    {
        return $this->appUserAuthToken;
    }
}