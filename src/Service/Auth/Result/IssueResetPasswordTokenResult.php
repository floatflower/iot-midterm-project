<?php


namespace App\Service\Auth\Result;


use App\Entity\AppUserResetPasswordToken;

class IssueResetPasswordTokenResult
{
    /**
     * @var AppUserResetPasswordToken $appUserResetPasswordToken
     */
    private AppUserResetPasswordToken $appUserResetPasswordToken;

    /**
     * @param AppUserResetPasswordToken $appUserResetPasswordToken
     * @return $this
     */
    public function setAppUserResetPasswordToken(AppUserResetPasswordToken $appUserResetPasswordToken) : self
    {
        $this->appUserResetPasswordToken = $appUserResetPasswordToken;
        return $this;
    }

    /**
     * @return AppUserResetPasswordToken|null
     */
    public function getAppUserResetPasswordToken() : ?AppUserResetPasswordToken
    {
        return $this->appUserResetPasswordToken;
    }

    /**
     * @var string $plainToken
     */
    private string $plainToken;

    /**
     * @param string $plainToken
     * @return $this
     */
    public function setPlainToken(string $plainToken) : self
    {
        $this->plainToken = $plainToken;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPlainToken() : ?string
    {
        return $this->plainToken;
    }
}