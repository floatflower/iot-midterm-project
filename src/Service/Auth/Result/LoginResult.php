<?php


namespace App\Service\Auth\Result;


use App\Entity\AppUserAuthToken;

class LoginResult
{
    public function __construct()
    {
    }

    /**
     * @var bool $success
     */
    private bool $success = false;

    /**
     * @param bool $success
     * @return $this
     */
    public function setSuccess(bool $success) : self
    {
        $this->success = $success;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSuccess() : bool
    {
        return $this->success;
    }

    /**
     * @var string|null $plainToken
     */
    private ?string $plainToken = null;

    /**
     * @param string $plainToken
     * @return $this
     */
    public function setPlainToken(string $plainToken) : self
    {
        $this->plainToken = $plainToken;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPlainToken() : ?string
    {
        return $this->plainToken;
    }

    private string $jwt;

    /**
     * @param string $jwt
     * @return self
     */
    public function setJwt(string $jwt): self
    {
        $this->jwt = $jwt;
        return $this;
    }

    /**
     * @return string
     */
    public function getJwt(): string
    {
        return $this->jwt;
    }

    /**
     * @var AppUserAuthToken|null $appUserAuthToken
     */
    private ?AppUserAuthToken $appUserAuthToken = null;

    /**
     * @param AppUserAuthToken $appUserAuthToken
     * @return $this
     */
    public function setAppUserAuthToken(AppUserAuthToken $appUserAuthToken) : self
    {
        $this->appUserAuthToken = $appUserAuthToken;
        return $this;
    }

    /**
     * @return AppUserAuthToken|null
     */
    public function getAppUserAuthToken() : ?AppUserAuthToken
    {
        return $this->appUserAuthToken;
    }
}