<?php


namespace App\Service\Auth\Result;


use App\Entity\AppUserEmailVerifyToken;

class IssueEmailVerifyTokenResult
{
    /**
     * @var AppUserEmailVerifyToken $appUserEmailVerifyToken
     */
    private AppUserEmailVerifyToken $appUserEmailVerifyToken;

    /**
     * @return AppUserEmailVerifyToken|null
     */
    public function getAppUserEmailVerifyToken() : ?AppUserEmailVerifyToken
    {
        return $this->appUserEmailVerifyToken;
    }

    /**
     * @param AppUserEmailVerifyToken $appUserEmailVerifyToken
     * @return $this
     */
    public function setAppUserEmailVerifyToken(AppUserEmailVerifyToken $appUserEmailVerifyToken) : self
    {
        $this->appUserEmailVerifyToken = $appUserEmailVerifyToken;
        return $this;
    }

    /**
     * @var string|null
     */
    private ?string $plainToken = null;

    /**
     * @return string|null
     */
    public function getPlainToken() : ?string
    {
        return $this->plainToken;
    }

    /**
     * @param string $plainToken
     * @return $this
     */
    public function setPlainToken(string $plainToken) : self
    {
        $this->plainToken = $plainToken;
        return $this;
    }
}