<?php


namespace App\Service\Auth\Result;


use App\Entity\AppUser;
use App\Entity\AppUserEmailVerifyToken;

class RegisterResult
{
    /**
     * @var AppUser
     */
    private AppUser $appUser;

    /**
     * @param AppUser $appUser
     * @return $this
     */
    public function setAppUser(AppUser $appUser) : self
    {
        $this->appUser = $appUser;
        return $this;
    }

    /**
     * @return AppUser|null
     */
    public function getAppUser() : ?AppUser
    {
        return $this->appUser;
    }
}