<?php


namespace App\Service\Auth\Result;


use App\Entity\AppUser;

class EmailVerifyResult
{
    private AppUser $appUser;

    /**
     * @param AppUser $appUser
     * @return self
     */
    public function setAppUser(AppUser $appUser): self
    {
        $this->appUser = $appUser;
        return $this;
    }

    /**
     * @return AppUser
     */
    public function getAppUser(): AppUser
    {
        return $this->appUser;
    }
}