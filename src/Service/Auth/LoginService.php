<?php


namespace App\Service\Auth;


use App\Entity\AppUser;
use App\Entity\AppUserAuthToken;
use App\Entity\AppUserMobileLoginCode;
use App\Entity\AppUserOneTimePin;
use App\Event\Auth\MobileLoginCodeIssuedEvent;
use App\Event\Auth\OneTimePinExpiredEvent;
use App\Event\Auth\OneTimePinIssuedEvent;
use App\Event\Auth\OneTimePinNotMatchEvent;
use App\Event\Auth\OneTimePinTryTooManyTimesEvent;
use App\Event\Auth\PasswordInvalidEvent;
use App\Repository\AppUserAuthTokenRepository;
use App\Repository\AppUserOneTimePinRepository;
use App\Repository\AppUserRepository;
use App\Event\Auth\LoggedInEvent;
use App\Event\Auth\LoggedOutEvent;
use App\Service\Auth\Exception\OneTimePinExpiredException;
use App\Service\Auth\Exception\OneTimePinInvalidException;
use App\Service\Auth\Exception\OneTimePinNotMatchException;
use App\Service\Auth\Exception\OneTimePinTryTooManyTimesException;
use App\Service\Auth\Exception\UserExpiredException;
use App\Service\Auth\Exception\UsernameOrPasswordInvalidException;
use App\Service\Auth\Result\IssueMobileLoginCodeResult;
use App\Service\Auth\Result\IssueOneTimePinResult;
use App\Service\Auth\Result\LoginResult;
use App\Service\Auth\Result\LogoutResult;
use Doctrine\ORM\EntityManagerInterface;
use Firebase\JWT\JWT;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class LoginService
{
    /**
     * @var EventDispatcherInterface
     */
    private EventDispatcherInterface $eventDispatcher;

    /**
     * @var ContainerInterface $container
     */
    private ContainerInterface $container;

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var RequestStack $requestStack
     */
    private RequestStack $requestStack;

    public function __construct(EntityManagerInterface $entityManager,
                                ContainerInterface $container,
                                EventDispatcherInterface $eventDispatcher,
                                RequestStack $requestStack)
    {
        $this->container = $container;
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
        $this->requestStack = $requestStack;
    }

    /**
     * Section: 登入
     */

    /**
     * @param AppUser $appUser
     * @return LoginResult
     * @throws UserExpiredException
     */
    public function login(AppUser $appUser) : LoginResult
    {
        $request = $this->requestStack->getCurrentRequest();

        $currentTime = new \DateTime("now");

        if($currentTime > $appUser->getExpireTime())
            throw new UserExpiredException();

        $plainToken = hash("sha512", uniqid("auth_token_", true));
        $tokenHash = hash("sha512", $plainToken);

        $key = $this->container->getParameter("JWT_KEY");

        $payload = array(
            "iat" => time(),
            "auth_token" => $plainToken,
            "user_uuid" => $appUser->getUuid()
        );

        $jwt = JWT::encode($payload, $key);

        $appUserAuthToken = new AppUserAuthToken();
        $appUserAuthToken->setAppUser($appUser);
        $appUserAuthToken->setTokenHash($tokenHash);
        $appUserAuthToken->setClientIp($request->getClientIp());
        $appUserAuthToken->setUserAgent($request->headers->get("User-Agent"));

        $this->entityManager->persist($appUserAuthToken);
        $this->entityManager->flush();

        $loginResult = new LoginResult();
        $loginResult->setSuccess(true);
        $loginResult->setPlainToken($plainToken);
        $loginResult->setJwt($jwt);
        $loginResult->setAppUserAuthToken($appUserAuthToken);

        $loggedInEvent = new LoggedInEvent();
        $loggedInEvent->setAppUser($appUser);
        $loggedInEvent->setAppUserAuthToken($appUserAuthToken);
        $loggedInEvent->setPlainToken($plainToken);
        $this->eventDispatcher->dispatch($loggedInEvent);

        return $loginResult;
    }

    /**
     * Section: 透過唯一鍵值，如：Email, Username 及密碼登入
     */

    /**
     * @param string $username
     * @param string $password
     * @return LoginResult
     * @throws UserExpiredException
     * @throws UsernameOrPasswordInvalidException
     */
    public function loginWithUsername(string $username, string $password) : LoginResult
    {
        /**
         * @var AppUserRepository $appUserRepository
         */
        $appUserRepository = $this->entityManager->getRepository(AppUser::class);

        $appUser = $appUserRepository->findByLoginKey($username);
        if($appUser) {
            if(password_verify($password, $appUser->getPasswordHash())) return $this->login($appUser);

            $passwordInvalidEvent = new PasswordInvalidEvent();
            $passwordInvalidEvent->setAppUser($appUser);
            $this->eventDispatcher->dispatch($passwordInvalidEvent);
        }

        throw new UsernameOrPasswordInvalidException();
    }

    /**
     * Section: One Time Pin
     */

    /**
     * @param AppUser $appUser
     * @return IssueOneTimePinResult|null
     */
    public function issueOneTimePin(AppUser $appUser) : ?IssueOneTimePinResult
    {
        $session = hash("sha512", uniqid("otp_"));
        $code = rand(100000, 999999)."";
        $codeHash = password_hash($code, PASSWORD_BCRYPT);

        $appUserOneTimePin = new AppUserOneTimePin();
        $appUserOneTimePin->setAppUser($appUser);
        $appUserOneTimePin->setErrorCount(0);
        $appUserOneTimePin->setSession($session);
        $appUserOneTimePin->setCodeHash($codeHash);
        $appUserOneTimePin->setType(AppUserOneTimePin::TYPE_LOGIN);

        $this->entityManager->persist($appUserOneTimePin);
        $this->entityManager->flush();

        $issueOneTimePinResult = new IssueOneTimePinResult();
        $issueOneTimePinResult->setAppUser($appUser);
        $issueOneTimePinResult->setAppUserOneTimePin($appUserOneTimePin);
        $issueOneTimePinResult->setCode($code);

        $oneTimePinIssuedEvent = new OneTimePinIssuedEvent();
        $oneTimePinIssuedEvent->setAppUser($appUser);
        $oneTimePinIssuedEvent->setAppUserOneTimePin($appUserOneTimePin);
        $oneTimePinIssuedEvent->setCode($code);
        $this->eventDispatcher->dispatch($oneTimePinIssuedEvent);

        return $issueOneTimePinResult;
    }

    /**
     * Section: One Time Pin 登入
     */

    /**
     * @param string $session
     * @param string $code
     * @return LoginResult|null
     * @throws OneTimePinExpiredException
     * @throws OneTimePinInvalidException
     * @throws OneTimePinNotMatchException
     * @throws OneTimePinTryTooManyTimesException
     * @throws UserExpiredException
     */
    public function loginWithOneTimePin(string $session, string $code) : ?LoginResult
    {
        /**
         * @var AppUserOneTimePinRepository $appUserOneTimePinRepository
         */
        $appUserOneTimePinRepository = $this->entityManager->getRepository(AppUserOneTimePin::class);
        $appUserOneTimePin = $appUserOneTimePinRepository->findOneBy(array("session" => $session, "type" => AppUserOneTimePin::TYPE_LOGIN));
        $this->entityManager->refresh($appUserOneTimePin);

        if(!$appUserOneTimePin) {

            $oneTimePinInvalidEvent = new OneTimePinInvalidException();
            $this->eventDispatcher->dispatch($oneTimePinInvalidEvent);

            throw new OneTimePinInvalidException();
        }

        $appUser = $appUserOneTimePin->getAppUser();

        if($appUserOneTimePin->getErrorCount() >= 3) {

            $oneTimePinTryTooManyTimesEvent = new OneTimePinTryTooManyTimesEvent();
            $oneTimePinTryTooManyTimesEvent->setAppUserOneTimePin($appUserOneTimePin);
            $oneTimePinTryTooManyTimesEvent->setAppUser($appUser);

            throw new OneTimePinTryTooManyTimesException();
        }

        $currentTime = new \DateTime("now");

        if($currentTime > $appUserOneTimePin->getExpireAt()) {

            $oneTimePinExpiredEvent = new OneTimePinExpiredEvent();
            $oneTimePinExpiredEvent->setAppUser($appUser);;
            $oneTimePinExpiredEvent->setAppUserOneTimePin($appUserOneTimePin);

            throw new OneTimePinExpiredException();
        }

        if(!password_verify($code, $appUserOneTimePin->getCodeHash())) {

            $appUserOneTimePin->incrementErrorCount();
            $this->entityManager->persist($appUserOneTimePin);
            $this->entityManager->flush();

            $oneTimePinNotMatchEvent = new OneTimePinNotMatchEvent();
            $oneTimePinNotMatchEvent->setAppUser($appUser);
            $oneTimePinNotMatchEvent->setAppUserOneTimePin($appUserOneTimePin);
            $this->eventDispatcher->dispatch($oneTimePinNotMatchEvent);

            throw new OneTimePinNotMatchException();
        }

        $appUser = $appUserOneTimePin->getAppUser();

        $this->entityManager->remove($appUserOneTimePin);
        $this->entityManager->flush();

        return $this->login($appUser);
    }

    /**
     * Section: 登出
     */

    /**
     * 透過 auth_token 登出
     * @param string $plainToken
     * @return LogoutResult
     */
    public function logout(string $plainToken) : LogoutResult
    {
        $tokenHash = hash("sha512", $plainToken);

        /**
         * @var AppUserAuthTokenRepository $appUserAuthTokenRepository
         */
        $appUserAuthTokenRepository
            = $this->entityManager->getRepository(AppUserAuthToken::class);

        $appUserAuthToken = $appUserAuthTokenRepository->findOneBy(array("tokenHash" => $tokenHash));

        $logoutResult = new LogoutResult();

        if($appUserAuthToken) {

            $this->entityManager->remove($appUserAuthToken);
            $this->entityManager->flush();

            $appUser = $appUserAuthToken->getAppUser();

            $loggedOutEvent = new LoggedOutEvent();
            $loggedOutEvent->setAppUser($appUser);
            $loggedOutEvent->setAppUserAuthToken($appUserAuthToken);
            $this->eventDispatcher->dispatch($loggedOutEvent);

        }

        return $logoutResult;
    }

    /**
     * 登出 Session Key
     *
     * @param string $session
     * @return LogoutResult
     */
    public function logoutWithSession(string $session) : LogoutResult
    {
        /**
         * @var AppUserAuthTokenRepository $appUserAuthTokenRepository
         */
        $appUserAuthTokenRepository
            = $this->entityManager->getRepository(AppUserAuthToken::class);

        $appUserAuthToken = $appUserAuthTokenRepository->findOneBy(array("session" => $session));

        $logoutResult = new LogoutResult();

        if($appUserAuthToken) {

            $this->entityManager->remove($appUserAuthToken);
            $this->entityManager->flush();

            $appUser = $appUserAuthToken->getAppUser();

            $loggedOutEvent = new LoggedOutEvent();
            $loggedOutEvent->setAppUser($appUser);
            $loggedOutEvent->setAppUserAuthToken($appUserAuthToken);
            $this->eventDispatcher->dispatch($loggedOutEvent);

        }

        return $logoutResult;
    }

    /**
     * 登出指定用戶的所有裝置
     *
     * @param AppUser $appUser
     * @return LogoutResult[]
     */
    public function logoutAppUser(AppUser $appUser) : array
    {

    }
}