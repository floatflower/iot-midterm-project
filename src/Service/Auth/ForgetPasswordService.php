<?php


namespace App\Service\Auth;


use App\Entity\AppUser;
use App\Entity\AppUserOneTimePin;
use App\Entity\AppUserResetPasswordToken;
use App\Event\Auth\OneTimePinExpiredEvent;
use App\Event\Auth\OneTimePinIssuedEvent;
use App\Event\Auth\OneTimePinNotMatchEvent;
use App\Event\Auth\OneTimePinTryTooManyTimesEvent;
use App\Event\Auth\ResetPasswordTokenExpiredEvent;
use App\Event\Auth\ResetPasswordTokenInvalidEvent;
use App\Event\Auth\ResetPasswordTokenIssuedEvent;
use App\Event\Entity\AppUser\PasswordUpdatedEvent;
use App\Repository\AppUserOneTimePinRepository;
use App\Repository\AppUserRepository;
use App\Repository\AppUserResetPasswordTokenRepository;
use App\Service\Auth\Exception\OneTimePinExpiredException;
use App\Service\Auth\Exception\OneTimePinInvalidException;
use App\Service\Auth\Exception\OneTimePinNotMatchException;
use App\Service\Auth\Exception\OneTimePinTryTooManyTimesException;
use App\Service\Auth\Exception\ResetPasswordTokenExpiredException;
use App\Service\Auth\Exception\ResetPasswordTokenInvalidException;
use App\Service\Auth\Exception\UserNotFoundException;
use App\Service\Auth\Result\IssueOneTimePinResult;
use App\Service\Auth\Result\ResetPasswordResult;
use App\Service\Auth\Result\IssueResetPasswordTokenResult;
use App\Service\Email\ResetPasswordEmailSender;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ForgetPasswordService
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param string $username
     * @return IssueResetPasswordTokenResult|null
     * @throws UserNotFoundException
     */
    public function issueResetPasswordToken(string $username) : ?IssueResetPasswordTokenResult
    {
        /**
         * @var AppUserRepository $appUserRepository
         */
        $appUserRepository = $this->entityManager->getRepository(AppUser::class);

        $appUser = $appUserRepository->findByLoginKey($username);

        if(!$appUser) throw new UserNotFoundException();

        $plainToken = hash("sha512", uniqid("reset_password", true));
        $tokenHash = hash("sha512", $plainToken);

        $appUserResetPasswordToken = new AppUserResetPasswordToken();
        $appUserResetPasswordToken->setTokenHash($tokenHash);
        $appUserResetPasswordToken->setAppUser($appUser);

        $this->entityManager->persist($appUserResetPasswordToken);
        $this->entityManager->flush();

        $resetPasswordTokenResult = new IssueResetPasswordTokenResult();
        $resetPasswordTokenResult->setPlainToken($plainToken);
        $resetPasswordTokenResult->setAppUserResetPasswordToken($appUserResetPasswordToken);

        $resetPasswordTokenIssuedEvent = new ResetPasswordTokenIssuedEvent();
        $resetPasswordTokenIssuedEvent->setAppUser($appUser);
        $resetPasswordTokenIssuedEvent->setPlainToken($plainToken);
        $resetPasswordTokenIssuedEvent->setAppUserResetPasswordToken($appUserResetPasswordToken);
        $this->eventDispatcher->dispatch($resetPasswordTokenIssuedEvent);

        return $resetPasswordTokenResult;
    }

    /**
     * @param AppUser $appUser
     * @return IssueOneTimePinResult
     */
    public function issueOneTimePin(AppUser $appUser) : IssueOneTimePinResult
    {
        $session = hash("sha512", uniqid("otp_"));
        $code = rand(100000, 999999)."";
        $codeHash = password_hash($code, PASSWORD_BCRYPT);

        $appUserOneTimePin = new AppUserOneTimePin();
        $appUserOneTimePin->setAppUser($appUser);
        $appUserOneTimePin->setErrorCount(0);
        $appUserOneTimePin->setSession($session);
        $appUserOneTimePin->setCodeHash($codeHash);
        $appUserOneTimePin->setType(AppUserOneTimePin::TYPE_RESET_PASSWORD);

        $this->entityManager->persist($appUserOneTimePin);
        $this->entityManager->flush();

        $issueOneTimePinResult = new IssueOneTimePinResult();
        $issueOneTimePinResult->setAppUser($appUser);
        $issueOneTimePinResult->setAppUserOneTimePin($appUserOneTimePin);
        $issueOneTimePinResult->setCode($code);

        $oneTimePinIssuedEvent = new OneTimePinIssuedEvent();
        $oneTimePinIssuedEvent->setAppUser($appUser);
        $oneTimePinIssuedEvent->setAppUserOneTimePin($appUserOneTimePin);
        $oneTimePinIssuedEvent->setCode($code);
        $this->eventDispatcher->dispatch($oneTimePinIssuedEvent);

        return $issueOneTimePinResult;
    }

    public function issueResetPasswordTokenWithOneTimePin(string $session, string $code) : IssueResetPasswordTokenResult
    {
        /**
         * @var AppUserOneTimePinRepository $appUserOneTimePinRepository
         */
        $appUserOneTimePinRepository = $this->entityManager->getRepository(AppUserOneTimePin::class);

        $appUserOneTimePin = $appUserOneTimePinRepository->findOneBy(array("session" => $session));
        if(!$appUserOneTimePin) {

            $oneTimePinInvalidEvent = new OneTimePinInvalidException();
            $this->eventDispatcher->dispatch($oneTimePinInvalidEvent);

            throw new OneTimePinInvalidException();
        }

        $appUser = $appUserOneTimePin->getAppUser();

        if($appUserOneTimePin->getErrorCount() > 3) {

            $oneTimePinTryTooManyTimesEvent = new OneTimePinTryTooManyTimesEvent();
            $oneTimePinTryTooManyTimesEvent->setAppUserOneTimePin($appUserOneTimePin);
            $oneTimePinTryTooManyTimesEvent->setAppUser($appUser);

            throw new OneTimePinTryTooManyTimesException();
        }

        $currentTime = new \DateTime("now");

        if($currentTime > $appUserOneTimePin->getExpireAt()) {

            $oneTimePinExpiredEvent = new OneTimePinExpiredEvent();
            $oneTimePinExpiredEvent->setAppUser($appUser);;
            $oneTimePinExpiredEvent->setAppUserOneTimePin($appUserOneTimePin);

            throw new OneTimePinExpiredException();
        }

        if(!password_verify($code, $appUserOneTimePin->getCodeHash())) {

            $appUserOneTimePin->incrementErrorCount();
            $this->entityManager->persist($appUserOneTimePin);
            $this->entityManager->flush();

            $oneTimePinNotMatchEvent = new OneTimePinNotMatchEvent();
            $oneTimePinNotMatchEvent->setAppUser($appUser);
            $oneTimePinNotMatchEvent->setAppUserOneTimePin($appUserOneTimePin);
            $this->eventDispatcher->dispatch($oneTimePinNotMatchEvent);

            throw new OneTimePinNotMatchException();
        }

        $appUser = $appUserOneTimePin->getAppUser();

        $this->entityManager->remove($appUserOneTimePin);
        $this->entityManager->flush();

        return $this->issueResetPasswordToken($appUser->getUsername());
    }

    /**
     * @param string $token
     * @param string $password
     * @return ResetPasswordResult|null
     * @throws ResetPasswordTokenInvalidException
     * @throws ResetPasswordTokenExpiredException
     */
    public function resetPassword(string $token, string $password) : ?ResetPasswordResult
    {
        $tokenHash = hash("sha512", $token);

        /**
         * @var AppUserResetPasswordTokenRepository $appUserResetPasswordTokenRepository
         */
        $appUserResetPasswordTokenRepository
            = $this->entityManager->getRepository(AppUserResetPasswordToken::class);

        /**
         * @var AppUserResetPasswordToken $appUserResetPasswordToken
         */
        $appUserResetPasswordToken
            = $appUserResetPasswordTokenRepository->findOneBy(array("tokenHash" => $tokenHash));

        if(!$appUserResetPasswordToken) {

            $resetPasswordTokenInvalidEvent = new ResetPasswordTokenInvalidEvent();
            $this->eventDispatcher->dispatch($resetPasswordTokenInvalidEvent);

            throw new ResetPasswordTokenInvalidException();
        }

        $currentTime = new \DateTime("now");

        if($currentTime > $appUserResetPasswordToken->getExpireAt()) {

            $resetPasswordTokenExpiredEvent = new ResetPasswordTokenExpiredEvent();
            $resetPasswordTokenExpiredEvent->setAppUserResetPasswordToken($appUserResetPasswordToken);
            $this->eventDispatcher->dispatch($resetPasswordTokenExpiredEvent);

            throw new ResetPasswordTokenExpiredException();
        }

        $appUser = $appUserResetPasswordToken->getAppUser();
        $appUser->setPasswordHash(password_hash($password, PASSWORD_BCRYPT));

        $this->entityManager->persist($appUser);
        $this->entityManager->remove($appUserResetPasswordToken);

        $this->entityManager->flush();

        $resetPasswordResult = new ResetPasswordResult();

        $passwordUpdatedEvent = new PasswordUpdatedEvent();
        $passwordUpdatedEvent->setAppUser($appUser);
        $this->eventDispatcher->dispatch($passwordUpdatedEvent);

        return $resetPasswordResult;
    }
}