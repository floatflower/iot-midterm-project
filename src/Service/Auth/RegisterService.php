<?php


namespace App\Service\Auth;


use App\Entity\AppUser;
use App\Service\Auth\Result\EmailVerifyResult;
use App\Service\Auth\Result\IssueEmailVerifyTokenResult;
use App\Service\Auth\Result\IssueMobileVerifyCodeResult;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class RegisterService
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }
}