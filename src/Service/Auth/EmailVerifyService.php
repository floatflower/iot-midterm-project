<?php


namespace App\Service\Auth;


use App\Entity\AppUser;
use App\Entity\AppUserEmailVerifyToken;
use App\Event\Auth\EmailVerifyTokenExpiredEvent;
use App\Event\Auth\EmailVerifyTokenIssuedEvent;
use App\Event\Entity\AppUser\EmailVerifiedEvent;
use App\Repository\AppUserEmailVerifyTokenRepository;
use App\Service\Auth\Exception\EmailVerifyTokenExpiredException;
use App\Service\Auth\Exception\EmailVerifyTokenInvalidException;
use App\Service\Auth\Result\EmailVerifyResult;
use App\Service\Auth\Result\IssueEmailVerifyTokenResult;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class EmailVerifyService
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param AppUser $appUser
     * @param string $email
     * @return IssueEmailVerifyTokenResult|null
     */
    public function issueVerifyToken(AppUser $appUser, string $email) : ?IssueEmailVerifyTokenResult
    {
        $plainToken = hash("sha512", uniqid("email_verify_"));
        $tokenHash = hash("sha512", $plainToken);

        $appUserEmailVerifyToken = new AppUserEmailVerifyToken();
        $appUserEmailVerifyToken->setAppUser($appUser);
        $appUserEmailVerifyToken->setEmail($email);
        $appUserEmailVerifyToken->setTokenHash($tokenHash);

        $this->entityManager->persist($appUserEmailVerifyToken);
        $this->entityManager->flush();

        $issueEmailVerifyTokenResult = new IssueEmailVerifyTokenResult();
        $issueEmailVerifyTokenResult->setAppUserEmailVerifyToken($appUserEmailVerifyToken);
        $issueEmailVerifyTokenResult->setPlainToken($plainToken);

        $emailVerifyTokenIssuedEvent = new EmailVerifyTokenIssuedEvent();
        $emailVerifyTokenIssuedEvent->setPlainToken($plainToken);
        $emailVerifyTokenIssuedEvent->setAppUserEmailVerifyToken($appUserEmailVerifyToken);
        $emailVerifyTokenIssuedEvent->setAppUser($appUser);
        $this->eventDispatcher->dispatch($emailVerifyTokenIssuedEvent);

        return $issueEmailVerifyTokenResult;
    }

    /**
     * @param string $token
     * @return EmailVerifyResult|null
     * @throws EmailVerifyTokenExpiredException
     * @throws EmailVerifyTokenInvalidException
     */
    public function verifyByToken(string $token) : ?EmailVerifyResult
    {
        $tokenHash = hash("sha512", $token);

        /**
         * @var AppUserEmailVerifyTokenRepository $appUserEmailVerifyTokenRepository
         */
        $appUserEmailVerifyTokenRepository = $this->entityManager->getRepository(AppUserEmailVerifyToken::class);
        $appUserEmailVerifyToken = $appUserEmailVerifyTokenRepository->findOneBy(array("tokenHash" => $tokenHash));

        if(!$appUserEmailVerifyToken) {

            $emailVerifyTokenExpiredEvent = new EmailVerifyTokenExpiredEvent();
            $emailVerifyTokenExpiredEvent->setAppUserEmailVerifyToken($appUserEmailVerifyToken);
            $this->eventDispatcher->dispatch($emailVerifyTokenExpiredEvent);

            throw new EmailVerifyTokenInvalidException();
        }

        $currentTime = new \DateTime("now");
        if($currentTime > $appUserEmailVerifyToken->getExpireAt()) {

            $emailVerifyTokenExpiredEvent = new EmailVerifyTokenExpiredEvent();
            $emailVerifyTokenExpiredEvent->setAppUserEmailVerifyToken($appUserEmailVerifyToken);
            $this->eventDispatcher->dispatch($emailVerifyTokenExpiredEvent);

            throw new EmailVerifyTokenExpiredException();
        }

        $appUser = $appUserEmailVerifyToken->getAppUser();
        
        $appUser->setEmail($appUserEmailVerifyToken->getEmail());
        $this->entityManager->persist($appUser);
        $this->entityManager->remove($appUserEmailVerifyToken);
        $this->entityManager->flush();

        return $this->verify($appUser);
    }

    /**
     * @param AppUser $appUser
     * @return EmailVerifyResult|null
     */
    public function verify(AppUser $appUser) : ?EmailVerifyResult
    {
        $appUser->setEmailVerified(true);

        $this->entityManager->persist($appUser);
        $this->entityManager->flush();

        $emailVerifyResult = new EmailVerifyResult();
        $emailVerifyResult->setAppUser($appUser);

        $emailVerifiedEvent = new EmailVerifiedEvent();
        $emailVerifiedEvent->setAppUser($appUser);
        $this->eventDispatcher->dispatch($emailVerifiedEvent);

        return $emailVerifyResult;
    }
}