<?php


namespace App\Service\Entity\Banner;


use App\Entity\Banner;
use App\Event\Entity\Banner\CreatedEvent;
use App\Service\Entity\Banner\Exception\NameRequiredException;
use App\Service\Entity\Banner\Result\CreateResult;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CreateService
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param array $data
     * @return CreateResult|null
     * @throws NameRequiredException
     */
    public function create(array $data) : ?CreateResult
    {
        $name = null;
        $alias = null;

        if(array_key_exists("name", $data) && is_string($data["name"]) && strlen($data["name"]) > 0)
            $name = $data["name"];

        if(array_key_exists("alias", $data) && is_string($data["alias"]) && strlen($data["alias"]) > 0)
            $alias = $data["alias"];

        if(is_null($name))
            throw new NameRequiredException();

        if(is_null($alias))
            $alias = uniqid("banner_");

        $banner = new Banner();
        $banner->setName($name);
        $banner->setAlias($alias);

        $this->entityManager->persist($banner);
        $this->entityManager->flush();

        $createResult = new CreateResult();
        $createResult->setBanner($banner);

        $createdEvent = new CreatedEvent();
        $createdEvent->setBanner($banner);
        $this->eventDispatcher->dispatch($createdEvent);

        return $createResult;
    }
}