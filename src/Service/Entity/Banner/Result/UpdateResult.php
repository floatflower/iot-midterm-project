<?php


namespace App\Service\Entity\Banner\Result;


use App\Entity\Banner;

class UpdateResult
{
    /**
     * @var Banner $banner
     */
    private Banner $banner;

    /**
     * @param Banner $banner
     * @return self
     */
    public function setBanner(Banner $banner): self
    {
        $this->banner = $banner;
        return $this;
    }

    /**
     * @return Banner
     */
    public function getBanner(): Banner
    {
        return $this->banner;
    }
}