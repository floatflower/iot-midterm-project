<?php


namespace App\Service\Entity\Banner;


use App\Entity\Banner;
use App\Event\Entity\Banner\UpdatedEvent;
use App\Service\Entity\Banner\Result\UpdateResult;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UpdateService
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function update(Banner $banner, array $data) : ?UpdateResult
    {
        $updated = false;

        if(array_key_exists("name", $data)
            && is_string($data["name"])
            && strlen($data["name"]) > 0) {
            $banner->setName($data["name"]);
            $updated = true;
        }

        if(array_key_exists("alias", $data)
            && is_string($data["alias"])
            && strlen($data["alias"]) > 0) {
            $banner->setAlias($data["alias"]);
            $updated = true;
        }

        $updateResult = new UpdateResult();
        $updateResult->setBanner($banner);

        if($updated) {
            $this->entityManager->persist($banner);
            $this->entityManager->flush();

            $updatedEvent = new UpdatedEvent();
            $updatedEvent->setBanner($banner);
            $this->eventDispatcher->dispatch($updatedEvent);
        }

        return $updateResult;
    }
}