<?php


namespace App\Service\Entity\Banner;


use App\Entity\Banner;
use App\Event\Entity\Banner\DeletedEvent;
use App\Service\Entity\Banner\Result\DeleteResult;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class DeleteService
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Banner $banner
     * @return DeleteResult|null
     */
    public function delete(Banner $banner) : ?DeleteResult
    {
        $this->entityManager->remove($banner);
        $this->entityManager->flush();

        $deleteResult = new DeleteResult();
        $deleteResult->setBanner($banner);

        $deletedEvent = new DeletedEvent();
        $deletedEvent->setBanner($banner);
        $this->eventDispatcher->dispatch($deletedEvent);

        return $deleteResult;
    }
}