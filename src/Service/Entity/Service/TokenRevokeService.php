<?php


namespace App\Service\Entity\Service;


use App\Entity\Service;
use App\Event\Entity\Service\TokenRevokedEvent;
use App\Repository\ServiceRepository;
use App\Service\Entity\Service\Exception\ServiceNotFoundException;
use App\Service\Entity\Service\Result\TokenRevokeResult;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class TokenRevokeService
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Service $service
     * @return TokenRevokeResult|null
     */
    public function revoke(Service $service) : ?TokenRevokeResult
    {
        $plainToken = hash("sha512", uniqid("service_", true));
        $tokenHash = hash("sha512", $plainToken);

        $service->setTokenHash($tokenHash);
        $this->entityManager->persist($service);
        $this->entityManager->flush();

        $tokenRevokeResult = new TokenRevokeResult();
        $tokenRevokeResult->setService($service);
        $tokenRevokeResult->setPlainToken($plainToken);

        $tokenRevokedEvent = new TokenRevokedEvent();
        $tokenRevokedEvent->setService($service);
        $this->eventDispatcher->dispatch($tokenRevokedEvent);

        return $tokenRevokeResult;
    }

    /**
     * @param string $uuid
     * @return TokenRevokeResult|null
     * @throws ServiceNotFoundException
     */
    public function revokeByUuid(string $uuid) : ?TokenRevokeResult
    {
        /**
         * @var ServiceRepository $serviceRepository
         */
        $serviceRepository = $this->entityManager->getRepository(Service::class);

        $service = $serviceRepository->findOneBy(array("uuid" => $uuid));

        if(!$service) {
            throw new ServiceNotFoundException();
        }

        return $this->revoke($service);
    }

    /**
     * @param int $id
     * @return TokenRevokeResult|null
     * @throws ServiceNotFoundException
     */
    public function revokeById(int $id) : ?TokenRevokeResult
    {
        /**
         * @var ServiceRepository $serviceRepository
         */
        $serviceRepository = $this->entityManager->getRepository(Service::class);

        $service = $serviceRepository->find($id);

        if(!$service) {
            throw new ServiceNotFoundException();
        }

        return $this->revoke($service);
    }
}