<?php


namespace App\Service\Entity\Service;

use App\Entity\Service;
use App\Event\Entity\Service\DeletedEvent;
use App\Repository\ServiceRepository;
use App\Service\Entity\Service\Exception\ServiceNotFoundException;
use App\Service\Entity\Service\Result\DeleteResult;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class DeleteService
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Service $service
     * @return DeleteResult|null
     */
    public function delete(Service $service) : ?DeleteResult
    {
        $this->entityManager->remove($service);
        $this->entityManager->flush();

        $deleteResult = new DeleteResult();
        $deleteResult->setService($service);

        $deletedEvent = new DeletedEvent();
        $deletedEvent->setService($service);
        $this->eventDispatcher->dispatch($deletedEvent);

        return $deleteResult;
    }

    /**
     * @param string $uuid
     * @return DeleteResult|null
     * @throws ServiceNotFoundException
     */
    public function deleteByUuid(string $uuid) : ?DeleteResult
    {
        /**
         * @var ServiceRepository $serviceRepository
         */
        $serviceRepository = $this->entityManager->getRepository(Service::class);

        $service = $serviceRepository->findOneBy(array("uuid" => $uuid));

        if(!$service) {
            throw new ServiceNotFoundException();
        }

        return $this->delete($service);
    }

    /**
     * @param int $id
     * @return DeleteResult|null
     * @throws ServiceNotFoundException
     */
    public function deleteById(int $id) : ?DeleteResult
    {
        /**
         * @var ServiceRepository $serviceRepository
         */
        $serviceRepository = $this->entityManager->getRepository(Service::class);

        $service = $serviceRepository->find($id);

        if(!$service) {
            throw new ServiceNotFoundException();
        }

        return $this->delete($service);
    }
}