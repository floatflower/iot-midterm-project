<?php


namespace App\Service\Entity\Service;


use App\Entity\Service;
use App\Event\Entity\Service\CreatedEvent;
use App\Service\Entity\Service\Exception\NameRequiredException;
use App\Service\Entity\Service\Result\CreateResult;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CreateService
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param array $data
     * @return CreateResult
     * @throws NameRequiredException
     */
    public function create(array $data) : CreateResult
    {
        $name = null;
        $description = null;

        if(array_key_exists("name", $data) && is_string($data["name"]) && strlen($data["name"]) > 0)
            $name = $data["name"];

        if(array_key_exists("description", $data) && is_string($data["description"]) && strlen($data["description"]) > 0)
            $description = $data["description"];

        if(is_null($name))
            throw new NameRequiredException();

        $plainToken = hash("sha512", uniqid("service_", true));
        $tokenHash = hash("sha512", $plainToken);
        $service = new Service();
        $service->setName($name);
        $service->setDescription($description);
        $service->setTokenHash($tokenHash);

        $this->entityManager->persist($service);
        $this->entityManager->flush();

        $createResult = new CreateResult();
        $createResult->setService($service);
        $createResult->setPlainToken($plainToken);

        $createdEvent = new CreatedEvent();
        $createdEvent->setService($service);
        $this->eventDispatcher->dispatch($createdEvent);

        return $createResult;
    }
}