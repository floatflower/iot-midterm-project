<?php


namespace App\Service\Entity\Service;


use App\Entity\Service;
use App\Event\Entity\Service\UpdatedEvent;
use App\Repository\ServiceRepository;
use App\Service\Entity\Service\Exception\ServiceNotFoundException;
use App\Service\Entity\Service\Result\UpdateResult;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UpdateService
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Service $service
     * @param array $data
     * @return UpdateResult|null
     */
    public function update(Service $service, array $data) : ?UpdateResult
    {
        $updated = false;

        if(array_key_exists("name", $data)
            && is_string($data["name"])
            && strlen($data["name"]) > 0) {
            $service->setName($data["name"]);
            $updated = true;
        }

        if(array_key_exists("description", $data)
            && (is_string($data["description"]) || is_null($data["description"]))) {
            $description = $data["description"] ?? null;
            $service->setDescription($description);
            $updated = true;
        }

        $updateResult = new UpdateResult();
        $updateResult->setService($service);

        if($updated) {
            $this->entityManager->persist($service);
            $this->entityManager->flush();

            $updatedEvent = new UpdatedEvent();
            $updatedEvent->setService($service);
            $this->eventDispatcher->dispatch($updatedEvent);
        }

        return $updateResult;
    }

    /**
     * @param string $uuid
     * @param array $data
     * @return UpdateResult|null
     * @throws ServiceNotFoundException
     */
    public function updateByUuid(string $uuid, array $data) : ?UpdateResult
    {
        /**
         * @var ServiceRepository $serviceRepository
         */
        $serviceRepository = $this->entityManager->getRepository(Service::class);

        $service = $serviceRepository->findOneBy(array("uuid" => $uuid));

        if(!$service) {
            throw new ServiceNotFoundException();
        }

        return $this->update($service, $data);
    }

    /**
     * @param int $id
     * @param array $data
     * @return UpdateResult|null
     * @throws ServiceNotFoundException
     */
    public function updateById(int $id, array $data) : ?UpdateResult
    {
        /**
         * @var ServiceRepository $serviceRepository
         */
        $serviceRepository = $this->entityManager->getRepository(Service::class);

        $service = $serviceRepository->find($id);

        if(!$service) {
            throw new ServiceNotFoundException();
        }

        return $this->update($service, $data);
    }
}