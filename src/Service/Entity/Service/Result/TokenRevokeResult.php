<?php


namespace App\Service\Entity\Service\Result;


use App\Entity\Service;

class TokenRevokeResult
{
    /**
     * @var Service $service
     */
    private Service $service;

    /**
     * @param Service $service
     * @return $this
     */
    public function setService(Service $service) : self
    {
        $this->service = $service;
        return $this;
    }

    /**
     * @return Service|null
     */
    public function getService() : ?Service
    {
        return $this->service;
    }

    /**
     * @var string $plainToken
     */
    private string $plainToken;

    /**
     * @param string $plainToken
     * @return $this
     */
    public function setPlainToken(string $plainToken) : self
    {
        $this->plainToken = $plainToken;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPlainToken() : ?string
    {
        return $this->plainToken;
    }
}