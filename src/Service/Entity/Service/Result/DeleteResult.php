<?php


namespace App\Service\Entity\Service\Result;


use App\Entity\Service;

class DeleteResult
{
    /**
     * @var Service $service
     */
    private Service $service;

    /**
     * @param Service $service
     * @return $this
     */
    public function setService(Service $service) : self
    {
        $this->service = $service;
        return $this;
    }

    /**
     * @return Service|null
     */
    public function getService() : ?Service
    {
        return $this->service;
    }
}