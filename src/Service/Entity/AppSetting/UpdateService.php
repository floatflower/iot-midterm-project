<?php


namespace App\Service\Entity\AppSetting;


use App\Entity\AppSetting;
use App\Event\Entity\AppSetting\UpdatedEvent;
use App\Repository\AppSettingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UpdateService
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param string $key
     * @param string|null $value
     * @return AppSetting
     */
    public function set(string $key, ?string $value) : AppSetting
    {
        /**
         * @var AppSettingRepository $appSettingRepository
         */
        $appSettingRepository = $this->entityManager->getRepository(AppSetting::class);

        $appSetting = $appSettingRepository->findOneByKey($key);

        if(!$appSetting)
            $appSetting = new AppSetting();

        $appSetting->setKey($key);
        $appSetting->setValue($value);

        $this->entityManager->persist($appSetting);
        $this->entityManager->flush();

        $updatedEvent = new UpdatedEvent();
        $updatedEvent->setAppSetting($appSetting);
        $this->eventDispatcher->dispatch($updatedEvent);

        return $appSetting;
    }
}