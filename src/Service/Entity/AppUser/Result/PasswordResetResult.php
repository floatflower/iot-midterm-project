<?php


namespace App\Service\Entity\AppUser\Result;


use App\Entity\AppUser;

class PasswordResetResult
{
    /**
     * @var AppUser $appUser
     */
    private AppUser $appUser;

    /**
     * @return AppUser
     */
    public function getAppUser(): AppUser
    {
        return $this->appUser;
    }

    /**
     * @param AppUser $appUser
     * @return self
     */
    public function setAppUser(AppUser $appUser): self
    {
        $this->appUser = $appUser;
        return $this;
    }

    /**
     * @var string $newPassword
     */
    private string $newPassword;

    /**
     * @param string $newPassword
     * @return self
     */
    public function setNewPassword(string $newPassword): self
    {
        $this->newPassword = $newPassword;
        return $this;
    }

    /**
     * @return string
     */
    public function getNewPassword(): string
    {
        return $this->newPassword;
    }

    /**
     * @var string $newPasswordHash
     */
    private string $newPasswordHash;

    /**
     * @return string
     */
    public function getNewPasswordHash(): string
    {
        return $this->newPasswordHash;
    }

    /**
     * @param string $newPasswordHash
     * @return self
     */
    public function setNewPasswordHash(string $newPasswordHash): self
    {
        $this->newPasswordHash = $newPasswordHash;
        return $this;
    }
}