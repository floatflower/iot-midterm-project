<?php


namespace App\Service\Entity\AppUser\Result;


use App\Entity\AppUser;

class EmailVerifyResult
{
    /**
     * @var AppUser $appUser
     */
    private AppUser $appUser;

    /**
     * @param AppUser $appUser
     * @return $this
     */
    public function setAppUser(AppUser $appUser) : self
    {
        $this->appUser = $appUser;
        return $this;
    }

    /**
     * @return AppUser|null
     */
    public function getAppUser() : ?AppUser
    {
        return $this->appUser;
    }
}