<?php


namespace App\Service\Entity\AppUser;


use App\Entity\AppUser;
use App\Repository\AppUserRepository;
use App\Service\Entity\AppUser\Exception\EmailInUsedException;
use App\Service\Entity\AppUser\Exception\MobileInUsedException;
use App\Service\Entity\AppUser\Exception\UsernameInUsedException;
use App\Service\Entity\AppUser\Result\UpdateResult;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UpdateService
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param AppUser $appUser
     * @param array $data
     * @return UpdateResult
     * @throws EmailInUsedException
     * @throws MobileInUsedException
     * @throws UsernameInUsedException
     */
    public function update(AppUser $appUser, array $data) : UpdateResult
    {
        /**
         * @var AppUserRepository $appUserRepository
         */
        $appUserRepository = $this->entityManager->getRepository(AppUser::class);

        if(array_key_exists("email", $data)
            && (is_string($data["email"]) || is_null($data["email"]))) {

            $email = $data["email"];

            if(!is_null($email)) {
                $appUserWithEmail = $appUserRepository->findOneByEmail($email);
                if ($appUserWithEmail && $appUserWithEmail->getUuid() !== $appUser->getUuid())
                    throw new EmailInUsedException();
            }

            $appUser->setEmail($email);
        }

        if(array_key_exists("mobile", $data)
            && (is_string($data["mobile"]) || is_null($data["mobile"]))) {
            $mobile = $data["mobile"];

            if(!is_null($mobile)) {
                $appUserWithMobile = $appUserRepository->findOneByMobile($mobile);
                if ($appUserWithMobile && $appUserWithMobile->getUuid() !== $appUser->getUuid())
                    throw new MobileInUsedException();
            }

            $appUser->setMobile($mobile);
        }

        if(array_key_exists("username", $data)
            && is_string($data["username"])
            && strlen($data["username"]) > 0) {

            $username = $data["username"];

            if(!is_null($username)) {
                $appUserWithUsername = $appUserRepository->findOneByUsername($username);
                if ($appUserWithUsername && $appUserWithUsername->getUuid() !== $appUser->getUuid())
                    throw new UsernameInUsedException();
            }

            $appUser->setUsername($username);
        }

        if(array_key_exists("is_admin", $data) && is_bool($data["is_admin"])) {

            $isAdmin = $data["is_admin"];

            $appUser->setAdmin($isAdmin);
        }

        if(array_key_exists("forever", $data) && is_bool($data["forever"]) && $data["forever"])
        {
            $appUser->setAsNeverExpireUser();
        } else if(array_key_exists("expire_time", $data) && $data["expire_time"] instanceof \DateTime)
        {
            $appUser->setExpireTime($data["expire_time"]);
        }

        if(array_key_exists("avatar", $data)
            && (is_string($data["avatar"]) || is_null($data["avatar"])))
        {
            $avatar = strlen($data["avatar"]) > 0 ? $data["avatar"] : null;
            $appUser->setAvatar($avatar);
        }


        $updateResult = new UpdateResult();
        $updateResult->setAppUser($appUser);

        $this->entityManager->persist($appUser);
        $this->entityManager->flush();

        return $updateResult;
    }
}