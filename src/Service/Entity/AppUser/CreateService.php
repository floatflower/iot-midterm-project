<?php


namespace App\Service\Entity\AppUser;


use App\Entity\AppUser;
use App\Repository\AppUserRepository;
use App\Service\Entity\AppUser\Exception\EmailInUsedException;
use App\Service\Entity\AppUser\Exception\EmailRequiredException;
use App\Service\Entity\AppUser\Exception\MobileInUsedException;
use App\Service\Entity\AppUser\Exception\PasswordRequiredException;
use App\Service\Entity\AppUser\Exception\UsernameInUsedException;
use App\Service\Entity\AppUser\Result\CreateResult;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CreateService
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param array $data
     * @return CreateResult
     * @throws EmailInUsedException
     * @throws MobileInUsedException
     * @throws PasswordRequiredException
     * @throws UsernameInUsedException
     */
    public function create(array $data) : CreateResult
    {
        $username = uniqid("user_");

        $email = null;
        $mobile = null;
        $password = null;
        $isAdmin = false;
        $forever = true;
        $expireTime = null;

        // get field
        // only those which match the requirement will be store into variable,
        // otherwise, the variable will always be null.

        if(array_key_exists("email", $data) && is_string($data["email"]) && strlen($data["email"]) > 0)
            $email = $data["email"];

        if(array_key_exists("mobile", $data) && is_string($data["mobile"]) && strlen($data["mobile"]) > 0)
            $mobile = $data["mobile"];

        if(array_key_exists("password", $data) && is_string($data["password"]) && strlen($data["password"]) > 0)
            $password = $data["password"];

        if(array_key_exists("is_admin", $data) && is_bool($data["is_admin"]))
            $isAdmin = $data["is_admin"];

        if(array_key_exists("forever", $data) && is_bool($data["forever"]))
            $forever = $data["forever"];

        if(array_key_exists("expire_time", $data)
            && is_string($data["expire_time"])
            && strlen($data["expire_time"]) > 0)
        {
            $expireTime = $data["expire_time"];
        }

        if(is_null($password))
            throw new PasswordRequiredException();

        if(!$forever && !is_null($expireTime)) {
            try {
                $expireTime = new \DateTime($expireTime);
                $forever = false;
            } catch (\Exception $exception) {
                $forever = true;
            }
        }

        /**
         * @var AppUserRepository $appUserRepository
         */
        $appUserRepository = $this->entityManager->getRepository(AppUser::class);

        if(!is_null($email)) {
            /**
             * Check whether email is in used.
             */
            $appUserWithEmail = $appUserRepository->findOneByEmail($email);

            if ($appUserWithEmail)
                throw new EmailInUsedException();
        }

        if(!is_null($mobile)) {
            /**
             * Check whether mobile is in used.
             */
            $appUserWithMobile = $appUserRepository->findOneByMobile($mobile);

            if($appUserWithMobile)
                throw new MobileInUsedException();
        }

        $appUserWithUsername = $appUserRepository->findOneByUsername($username);

        if($appUserWithUsername)
            throw new UsernameInUsedException();

        $appUser = new AppUser();
        $appUser->setUsername($username);
        $appUser->setAdmin(false);
        $appUser->setPasswordHash(password_hash($password, PASSWORD_BCRYPT));
        $appUser->setMobile($mobile);
        $appUser->setEmail($email);
        $appUser->setEmailVerified(false);
        $appUser->setAdmin($isAdmin);
        if($forever) $appUser->setAsNeverExpireUser();
        else $appUser->setExpireTime($expireTime);

        $this->entityManager->persist($appUser);
        $this->entityManager->flush();

        $createResult = new CreateResult();
        $createResult->setAppUser($appUser);

        return $createResult;
    }
}