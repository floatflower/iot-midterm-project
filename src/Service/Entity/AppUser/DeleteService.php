<?php


namespace App\Service\Entity\AppUser;


use App\Entity\AppUser;
use App\Service\Entity\AppUser\Result\DeleteResult;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class DeleteService
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param AppUser $appUser
     * @return DeleteResult
     */
    public function delete(AppUser $appUser)
    {
        $this->entityManager->remove($appUser);
        $this->entityManager->flush();

        $deleteResult = new DeleteResult();
        $deleteResult->setAppUser($appUser);

        return $deleteResult;
    }
}