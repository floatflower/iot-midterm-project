<?php


namespace App\Service\Entity\AppUser;


use App\Entity\AppUser;
use App\Service\Entity\AppUser\Result\PasswordResetResult;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ResetPasswordService
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param AppUser $appUser
     * @param string $newPassword
     * @return PasswordResetResult
     */
    public function resetPassword(AppUser $appUser, string $newPassword) : PasswordResetResult
    {
        $newPasswordHash = password_hash($newPassword, PASSWORD_BCRYPT);

        $appUser->setPasswordHash($newPasswordHash);

        $this->entityManager->persist($appUser);
        $this->entityManager->flush();

        $passwordResetResult = new PasswordResetResult();
        $passwordResetResult->setAppUser($appUser);
        $passwordResetResult->setNewPassword($newPassword);
        $passwordResetResult->setNewPasswordHash($newPasswordHash);

        return $passwordResetResult;
    }
}