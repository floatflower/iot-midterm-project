<?php


namespace App\Service\Entity\BannerItem;


use App\Entity\BannerItem;
use App\Event\Entity\BannerItem\UpdatedEvent;
use App\Service\Entity\BannerItem\Result\UpdateResult;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UpdateService
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function update(BannerItem $bannerItem, array $data) : UpdateResult
    {
        $updated = false;

        if(array_key_exists("title", $data)
            && (is_string($data["title"]) || is_null($data["title"]))) {
            $bannerItem->setTitle($data["title"]);
            $updated = true;
        }

        if(array_key_exists("subtitle", $data)
            && (is_string($data["subtitle"]) || is_null($data["subtitle"]))) {
            $bannerItem->setSubtitle($data["subtitle"]);
            $updated = true;
        }

        if(array_key_exists("link", $data)
            && (is_string($data["link"]) || is_null($data["link"]))) {
            $bannerItem->setLink($data["link"]);
            $updated = true;
        }

        if(array_key_exists("image", $data)
            && (is_string($data["image"]) || is_null($data["image"])))
        {
            $bannerItem->setImage($data["image"]);
            $updated = true;
        }

        $updateResult = new UpdateResult();
        $updateResult->setBannerItem($bannerItem);

        if($updated) {
            $this->entityManager->persist($bannerItem);
            $this->entityManager->flush();

            $updatedEvent = new UpdatedEvent();
            $updatedEvent->setBannerItem($bannerItem);
            $this->eventDispatcher->dispatch($updatedEvent);
        }

        return $updateResult;
    }
}