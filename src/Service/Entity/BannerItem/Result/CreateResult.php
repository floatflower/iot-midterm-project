<?php


namespace App\Service\Entity\BannerItem\Result;


use App\Entity\BannerItem;

class CreateResult
{
    /**
     * @var BannerItem $bannerItem
     */
    private BannerItem $bannerItem;

    /**
     * @param BannerItem $bannerItem
     * @return self
     */
    public function setBannerItem(BannerItem $bannerItem): self
    {
        $this->bannerItem = $bannerItem;
        return $this;
    }

    /**
     * @return BannerItem
     */
    public function getBannerItem(): BannerItem
    {
        return $this->bannerItem;
    }
}