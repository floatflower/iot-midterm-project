<?php


namespace App\Service\Entity\BannerItem\Result;


use App\Entity\BannerItem;

class ReorderResult
{
    private BannerItem $bannerItem;

    /**
     * @param BannerItem $bannerItem
     * @return self
     */
    public function setBannerItem(BannerItem $bannerItem): self
    {
        $this->bannerItem = $bannerItem;
        return $this;
    }

    /**
     * @return BannerItem
     */
    public function getBannerItem(): BannerItem
    {
        return $this->bannerItem;
    }

    private int $from;

    /**
     * @return int
     */
    public function getFrom(): int
    {
        return $this->from;
    }

    /**
     * @param int $from
     * @return self
     */
    public function setFrom(int $from): self
    {
        $this->from = $from;
        return $this;
    }

    private int $to;

    /**
     * @return int
     */
    public function getTo(): int
    {
        return $this->to;
    }

    /**
     * @param int $to
     * @return self
     */
    public function setTo(int $to): self
    {
        $this->to = $to;
        return $this;
    }
}