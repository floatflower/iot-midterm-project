<?php


namespace App\Service\Entity\BannerItem;


use App\Entity\BannerItem;
use App\Event\Entity\BannerItem\DeletedEvent;
use App\Repository\BannerItemRepository;
use App\Service\Entity\BannerItem\Result\DeleteResult;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class DeleteService
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param BannerItem $bannerItem
     * @return DeleteResult
     */
    public function delete(BannerItem $bannerItem) : DeleteResult
    {
        $this->entityManager->remove($bannerItem);
        $this->entityManager->flush();

        $banner = $bannerItem->getBanner();
        /**
         * @var BannerItemRepository $bannerItemRepository
         */
        $bannerItemRepository = $this->entityManager->getRepository(BannerItem::class);
        $bannerItems = $bannerItemRepository->findBy(array("banner" => $banner));

        foreach ($bannerItems as $index => $bannerItem) {
            $bannerItem->setSort($index);
            $this->entityManager->persist($bannerItem);
        }

        $this->entityManager->flush();

        $deleteResult = new DeleteResult();
        $deleteResult->setBannerItem($bannerItem);

        $deletedEvent = new DeletedEvent();
        $deletedEvent->setBannerItem($bannerItem);
        $this->eventDispatcher->dispatch($deletedEvent);

        return $deleteResult;
    }
}