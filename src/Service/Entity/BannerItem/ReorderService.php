<?php


namespace App\Service\Entity\BannerItem;


use App\Entity\BannerItem;
use App\Event\Entity\BannerItem\ReorderedEvent;
use App\Repository\BannerItemRepository;
use App\Service\Entity\BannerItem\Result\ReorderResult;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ReorderService
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function reorder(BannerItem $bannerItem, int $target)
    {
        $banner = $bannerItem->getBanner();

        /**
         * @var BannerItemRepository $bannerItemRepository
         */
        $bannerItemRepository = $this->entityManager->getRepository(BannerItem::class);
        $bannerItems = $bannerItemRepository->findBy(array("banner" => $banner), array("sort" => "ASC"));

        $originalPosition = $bannerItem->getSort();
        $targetPosition = min($target, count($bannerItems));
        $targetPosition = max(0, $targetPosition);

        $reorderResult = new ReorderResult();
        $reorderResult->setBannerItem($bannerItem);
        $reorderResult->setTo($targetPosition);

        if($originalPosition !== $targetPosition)
        {
            $a1 = array_splice($bannerItems, $originalPosition, 1);
            $a2 = array_splice($bannerItems, 0, $targetPosition);
            $bannerItems = array_merge($a2, $a1, $bannerItems);

            foreach ($bannerItems as $index => $bannerItem) {
                $bannerItem->setSort($index);
                $this->entityManager->persist($bannerItem);
            }

            $this->entityManager->flush();
        }

        $reorderedEvent = new ReorderedEvent();
        $reorderedEvent->setBannerItem($bannerItem);
        $this->eventDispatcher->dispatch($reorderedEvent);

        return $reorderResult;
    }
}