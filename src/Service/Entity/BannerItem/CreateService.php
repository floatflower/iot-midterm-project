<?php


namespace App\Service\Entity\BannerItem;


use App\Entity\Banner;
use App\Entity\BannerItem;
use App\Event\Entity\BannerItem\CreatedEvent;
use App\Repository\BannerItemRepository;
use App\Service\Entity\BannerItem\Exception\ImageRequiredException;
use App\Service\Entity\BannerItem\Result\CreateResult;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CreateService
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Banner $banner
     * @param array $data
     * @return CreateResult
     * @throws ImageRequiredException
     */
    public function create(Banner $banner, array $data)
    {
        $title = null;
        $subtitle = null;
        $link = null;
        $image = null;

        if(array_key_exists("title", $data)
            && is_string($data["title"])
            && strlen($data["title"]) > 0) {
            $title = $data["title"];
        }

        if(array_key_exists("subtitle", $data)
            && is_string($data["subtitle"])
            && strlen($data["subtitle"]) > 0) {
            $subtitle = $data["subtitle"];
        }

        if(array_key_exists("link", $data)
            && is_string($data["link"])
            && strlen($data["link"])) {
            $link = $data["link"];
        }

        if(array_key_exists("image", $data)
            && is_string($data["image"])
            && strlen($data["image"])) {
            $image = $data["image"];
        }

        if(is_null($image))
            throw new ImageRequiredException();

        /**
         * @var BannerItemRepository $bannerItemRepository
         */
        $bannerItemRepository = $this->entityManager->getRepository(BannerItem::class);
        $bannerItems = $bannerItemRepository->findBy(array("banner" => $banner));

        $bannerItem = new BannerItem();
        $bannerItem->setTitle($title);
        $bannerItem->setSubtitle($subtitle);
        $bannerItem->setLink($link);
        $bannerItem->setImage($image);
        $bannerItem->setSort(0);
        $bannerItem->setBanner($banner);

        $this->entityManager->persist($bannerItem);

        foreach ($bannerItems as $index => $bannerItem) {
            $bannerItem->setSort($index + 1);
            $this->entityManager->persist($bannerItem);
        }

        $this->entityManager->flush();

        $createResult = new CreateResult();
        $createResult->setBannerItem($bannerItem);

        $createdEvent = new CreatedEvent();
        $createdEvent->setBannerItem($bannerItem);
        $this->eventDispatcher->dispatch($createdEvent);

        return $createResult;
    }
}