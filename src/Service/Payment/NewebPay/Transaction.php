<?php


namespace App\Service\Payment\NewebPay;


class Transaction
{
    public function __construct(string $title, string $amount, string $serialNumber)
    {
        $this->orderTitle = $title;
        $this->amount = $amount;
        $this->serialNumber = $serialNumber;
        $this->expireDate = (new \DateTime("now"))->add(new \DateInterval("P3D"));
    }

    /**
     * @var string $serialNumber
     */
    private string $serialNumber;

    /**
     * @return string
     */
    public function getSerialNumber(): string
    {
        return $this->serialNumber;
    }

    /**
     * @param string $serialNumber
     * @return self
     */
    public function setSerialNumber(string $serialNumber): self
    {
        $this->serialNumber = $serialNumber;
        return $this;
    }

    /**
     * @var int $amount
     */
    private int $amount = 0;

    /**
     * @param int $amount
     * @return self
     */
    public function setAmount(int $amount): self
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @var string $orderTitle
     */
    private string $orderTitle;

    /**
     * @param string $orderTitle
     * @return self
     */
    public function setOrderTitle(string $orderTitle): self
    {
        $this->orderTitle = $orderTitle;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderTitle(): string
    {
        return $this->orderTitle;
    }

    /**
     * @var string|null $returnURL
     */
    private ?string $returnURL = null;

    /**
     * @param string|null $returnURL
     * @return self
     */
    public function setReturnURL(?string $returnURL): self
    {
        $this->returnURL = $returnURL;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReturnURL(): ?string
    {
        return $this->returnURL;
    }

    /**
     * @var string|null
     */
    private ?string $notifyURL = null;

    /**
     * @param string|null $notifyURL
     * @return self
     */
    public function setNotifyURL(?string $notifyURL): self
    {
        $this->notifyURL = $notifyURL;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNotifyURL(): ?string
    {
        return $this->notifyURL;
    }

    /**
     * @var string|null $customerURL
     */
    private ?string $customerURL = null;

    /**
     * @param string|null $customerURL
     * @return self
     */
    public function setCustomerURL(?string $customerURL): self
    {
        $this->customerURL = $customerURL;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomerURL(): ?string
    {
        return $this->customerURL;
    }

    private ?string $clientBackURL = null;

    /**
     * @param string|null $clientBackURL
     * @return self
     */
    public function setClientBackURL(?string $clientBackURL): self
    {
        $this->clientBackURL = $clientBackURL;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getClientBackURL(): ?string
    {
        return $this->clientBackURL;
    }

    private ?\DateTime $expireDate = null;

    /**
     * @param \DateTime|null $expireDate
     * @return self
     */
    public function setExpireDate(?\DateTime $expireDate): self
    {
        $this->expireDate = $expireDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getExpireDate(): ?\DateTime
    {
        return $this->expireDate;
    }
}