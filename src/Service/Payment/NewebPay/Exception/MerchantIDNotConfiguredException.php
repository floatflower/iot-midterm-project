<?php


namespace App\Service\Payment\NewebPay\Exception;


class MerchantIDNotConfiguredException extends NewebPayException
{

}