<?php


namespace App\Service\Payment\NewebPay;


use App\Entity\AppSetting;
use App\Repository\AppSettingRepository;
use App\Service\Entity\AppSetting\AppSettingKey;
use App\Service\Payment\NewebPay\Exception\ApiUrlNotConfiguredException;
use App\Service\Payment\NewebPay\Exception\HashIVNotConfiguredException;
use App\Service\Payment\NewebPay\Exception\HashKeyNotConfiguredException;
use App\Service\Payment\NewebPay\Exception\MerchantIDNotConfiguredException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class NewebPay
{
    /**
     * @var HttpClientInterface $httpClient
     */
    private HttpClientInterface $httpClient;

    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    public function __construct(HttpClientInterface $httpClient,
                                EntityManagerInterface $entityManager)
    {
        $this->httpClient = $httpClient;
        $this->entityManager = $entityManager;
    }

    /*HashKey AES 加解密 */
    static function createMPGAESEncrypt ($parameter = "" , $key = "", $iv = "") {
        $return_str = '';
        if (!empty($parameter)) {
            //將參數經過 URL ENCODED QUERY STRING
            $return_str = http_build_query($parameter);
        }
        return trim(bin2hex(openssl_encrypt(self::addpadding($return_str), 'aes-256-cbc', $key, OPENSSL_RAW_DATA|OPENSSL_ZERO_PADDING, $iv)));
    }

    static function addPadding($string, $blocksize = 32) {
        $len = strlen($string);
        $pad = $blocksize - ($len % $blocksize);
        $string .= str_repeat(chr($pad), $pad);

        return $string;
    }

    /*HashKey AES 解密 */
    static function createAESDecrypt($parameter = "", $key = "", $iv = "") {
        return self::strippadding(openssl_decrypt(hex2bin($parameter),'AES-256-CBC', $key, OPENSSL_RAW_DATA|OPENSSL_ZERO_PADDING, $iv));
    }

    static function strippadding($string) {
        $slast = ord(substr($string, -1));
        $slastc = chr($slast);
        $pcheck = substr($string, -$slast);
        if (preg_match("/$slastc{" . $slast . "}/", $string)) {
            $string = substr($string, 0, strlen($string) - $slast);
            return $string;
        } else {
            return false;
        }
    }

    /*HashIV SHA256 加密*/
    static function SHA256($key="", $tradeinfo="", $iv=""){
        return "HashKey=".$key."&".$tradeinfo."&HashIV=".$iv;
    }

    /**
     * @param Transaction $transaction
     * @return string
     * @throws ApiUrlNotConfiguredException
     * @throws HashIVNotConfiguredException
     * @throws HashKeyNotConfiguredException
     * @throws MerchantIDNotConfiguredException
     */
    public function checkOut(Transaction $transaction): string
    {
        /**
         * @var AppSettingRepository $appSettingRepository
         */
        $appSettingRepository = $this->entityManager->getRepository(AppSetting::class);

        $merchantIdSetting = $appSettingRepository->findOneByKey(AppSettingKey::NEWEBPAY_MERCHANT_ID);
        $hashKeySetting = $appSettingRepository->findOneByKey(AppSettingKey::NEWEBPAY_HASH_KEY);
        $hashIVSetting = $appSettingRepository->findOneByKey(AppSettingKey::NEWEBPAY_HASH_IV);
        $apiUrlSetting = $appSettingRepository->findOneByKey(AppSettingKey::NEWEBPAY_API_URL);

        if(!$merchantIdSetting || strlen($merchantIdSetting->getValue()) === 0)
            throw new MerchantIDNotConfiguredException();

        if(!$hashKeySetting || strlen($hashKeySetting->getValue()) === 0)
            throw new HashKeyNotConfiguredException();

        if(!$hashIVSetting || strlen($hashIVSetting->getValue()) === 0)
            throw new HashIVNotConfiguredException();

        if(!$apiUrlSetting || strlen($apiUrlSetting->getValue()) == 0)
            throw new ApiUrlNotConfiguredException();

        $merchantId = $merchantIdSetting->getValue();
        $hashIV = $hashIVSetting->getValue();
        $hashKey = $hashKeySetting->getValue();
        $url = $apiUrlSetting->getValue();
        // here is test data
        // $url = "https://ccore.newebpay.com/MPG/mpg_gateway"
        // $merchatId = "MS116347410";
        // $HashKey = "jhRs98A15eciqtFKgrWDTykoWY9uObRW";
        // $HashIV = "CwQr6DixjDoq2S3P";

        $tradeInfoData = array(
            'MerchantID' => $merchantIdSetting->getValue(),
            'RespondType' => 'JSON',
            'TimeStamp' => time(),
            'Version' => 1.5,
            'MerchantOrderNo' => $transaction->getSerialNumber(),
            'Amt' => $transaction->getAmount(),
            'ItemDesc' => $transaction->getOrderTitle(),
            'ReturnURL' => $transaction->getReturnURL() ?? "", //支付完成 返回商店網址
            'NotifyURL' => $transaction->getNotifyURL() ?? "", //支付通知網址
            'CustomerURL' => $transaction->getCustomerURL() ?? "", //商店取號網址
            'ClientBackURL' => $transaction->getClientBackURL() ?? "", //支付取消 返回商店網址
            'ExpireDate' => $transaction->getExpireDate()->format("Y-m-d")
        );

        $tradeInfo = NewebPay::createMPGAESEncrypt($tradeInfoData, $hashKey, $hashIV);
        $tradeSha = strtoupper(hash("sha256", NewebPay::SHA256($hashKey, $tradeInfo, $hashIV)));

        $szHtml = '<!doctype html>';
        $szHtml .='<html>';
        $szHtml .='<head>';
        $szHtml .='<meta charset="utf-8">';
        $szHtml .='</head>';
        $szHtml .='<body>';
        $szHtml .='<form name="newebpay" id="newebpay" method="post" action="'.$url.'" style="display:none;">';
        $szHtml .='<input type="text" name="MerchantID" value="'.$merchantId.'" type="hidden">';
        $szHtml .='<input type="text" name="TradeInfo" value="'.$tradeInfo.'"   type="hidden">';
        $szHtml .='<input type="text" name="TradeSha" value="'.$tradeSha.'" type="hidden">';
        $szHtml .='<input type="text" name="Version"  value="1.5" type="hidden">';
        $szHtml .='</form>';
        $szHtml .='<script type="text/javascript">';
        $szHtml .='document.getElementById("newebpay").submit();';
        $szHtml .='</script>';
        $szHtml .='</body>';
        $szHtml .='</html>';

        return $szHtml;
    }
}