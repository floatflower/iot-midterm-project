<?php


namespace App\Service\PunchQrCode;


use App\Entity\AppUser;
use Firebase\JWT\JWT;
use Symfony\Component\DependencyInjection\ContainerInterface;

class createService
{
    private ContainerInterface $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function generate(AppUser $appUser, int $type) : string
    {
        $key = $this->container->getParameter("JWT_KEY");

        $payload = array(
            "iat" => time(),
            "nonce" => uniqid("nonce_"),
            "user_uuid" => $appUser->getUuid(),
            "type" => $type
        );

        return JWT::encode($payload, $key);
    }
}