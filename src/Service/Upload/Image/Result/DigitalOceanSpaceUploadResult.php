<?php


namespace App\Service\Upload\Image\Result;


class DigitalOceanSpaceUploadResult
{
    private string $publicPath;

    /**
     * @param string $publicPath
     * @return self
     */
    public function setPublicPath(string $publicPath): self
    {
        $this->publicPath = $publicPath;
        return $this;
    }

    /**
     * @return string
     */
    public function getPublicPath(): string
    {
        return $this->publicPath;
    }

    /**
     * 上傳後的檔案大小
     * 因為可能涉及圖片壓縮或轉檔
     * 所以大小會與上傳後的大小不同
     * @var int
     */
    private int $size = 0;

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @param int $size
     * @return self
     */
    public function setSize(int $size): self
    {
        $this->size = $size;
        return $this;
    }

    /**
     * 上傳後的檔案名稱
     * @var string $filename
     */
    private string $filename;

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     * @return self
     */
    public function setFilename(string $filename): self
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * 檔案類別
     * @var string $mimeType
     */
    private string $mimeType;

    /**
     * @return string
     */
    public function getMimeType(): string
    {
        return $this->mimeType;
    }

    /**
     * @param string $mimeType
     * @return self
     */
    public function setMimeType(string $mimeType): self
    {
        $this->mimeType = $mimeType;
        return $this;
    }

    /**
     * 網路公開位置
     * @var string
     */
    private string $publicUri;

    /**
     * @return string
     */
    public function getPublicUri(): string
    {
        return $this->publicUri;
    }

    /**
     * @param string $publicUri
     * @return self
     */
    public function setPublicUri(string $publicUri): self
    {
        $this->publicUri = $publicUri;
        return $this;
    }
}