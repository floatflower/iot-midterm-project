<?php


namespace App\Service\Upload\Image;


use App\Entity\AppSetting;
use App\Repository\AppSettingRepository;
use App\Service\Entity\AppSetting\AppSettingKey;
use App\Service\Upload\Image\Result\DigitalOceanSpaceUploadResult;
use Aws\S3\MultipartUploader;
use Aws\S3\S3Client;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Uid\Uuid;

class DigitalOceanSpaceUploader
{
    /**
     * @var ContainerInterface $container
     */
    private ContainerInterface $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function upload(UploadedFile $uploadedFile)
    {
        $region = $this->container->getParameter("S3_REGION");
        $endpoint = $this->container->getParameter("S3_API_ENDPOINT");
        $bucketName = $this->container->getParameter("S3_BUCKET_NAME");
        $key = $this->container->getParameter("S3_KEY");
        $secret = $this->container->getParameter("S3_SECRET");
        $publicEndpoint = $this->container->getParameter("S3_PUBLIC_ENDPOINT");

        $filesize = $uploadedFile->getSize();

        $s3Client = new S3Client(array(
            'version' => 'latest',
            'region'  => $region,
            'endpoint' => $endpoint,
            'credentials' => [
                'key'    => $key,
                'secret' => $secret
            ],
        ));

        $uniqueName = (Uuid::v4())->toBase58();
        $generatedName = $uniqueName.".".$uploadedFile->guessClientExtension();

        $appEnv = $this->container->getParameter("APP_ENV");

        $uploadPublicPath = "$appEnv/images/".$generatedName;

        $digitalOceanSpaceUploadResult = new DigitalOceanSpaceUploadResult();
        $digitalOceanSpaceUploadResult->setPublicPath($uploadPublicPath);
        $digitalOceanSpaceUploadResult->setFilename($generatedName);
        $digitalOceanSpaceUploadResult->setSize($uploadedFile->getSize());
        $digitalOceanSpaceUploadResult->setMimeType($uploadedFile->getMimeType());

        $digitalOceanSpaceUploadResult->setPublicUri("$publicEndpoint/$uploadPublicPath");

        if($filesize > 5 * 1024) {
            $uploader = new MultipartUploader($s3Client, $uploadedFile->getRealPath(), [
                'Bucket' => $bucketName->getValue(),
                'Key'    => $uploadPublicPath,
                'ACL'    => 'public-read'
            ]);
            $uploader->upload();
        } else {
            $s3Client->putObject([
                'Bucket' => $bucketName->getValue(),
                'Key'    => $uploadPublicPath,
                'Body'   => file_get_contents($uploadedFile->getRealPath()),
                'ACL'    => 'public-read'
            ]);
        }

        return $digitalOceanSpaceUploadResult;
    }
}