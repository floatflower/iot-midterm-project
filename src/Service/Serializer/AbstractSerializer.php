<?php


namespace App\Service\Serializer;


use App\Entity\Service;

abstract class AbstractSerializer
{
    public function serialize($data) : array
    {
        if(is_array($data)) {
            $output = array();
            foreach ($data as $d) {
                $output[] = $this->handleData($d);
            }

            return $output;
        }

        return $this->handleData($data);
    }

    protected function handleData($data) : array {
        return array();
    }
}