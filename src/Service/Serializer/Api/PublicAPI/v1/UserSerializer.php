<?php


namespace App\Service\Serializer\Api\PublicAPI\v1;


use App\Entity\AppUser;
use App\Service\Serializer\AbstractSerializer;

class UserSerializer extends AbstractSerializer
{
    /**
     * @param AppUser $data
     * @return array
     */
    public function handleData($data): array
    {
        return array(
            "uuid" => $data->getUuid(),
            "username" => $data->getUsername(),
            "email" => $data->getEmail(),
            "create_at" => $data->getCreateAt()->format(\DateTimeInterface::W3C),
            "update_at" => $data->getUpdateAt()->format(\DateTimeInterface::W3C)
        );
    }
}