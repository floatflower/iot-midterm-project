<?php

namespace App\Controller\Api\Admin\v1\Upload;

use App\Form\Api\Admin\v1\Upload\ImageType;
use App\Service\Upload\Image\DigitalOceanSpaceUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ImageController extends AbstractController
{
    /**
     * @param Request $request
     * @param DigitalOceanSpaceUploader $digitalOceanSpaceUploader
     * @return Response
     * @Route("/api/admin/v1/upload/images", name="api.admin.upload.images")
     */
    public function __invoke(Request $request, DigitalOceanSpaceUploader $digitalOceanSpaceUploader): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $data = array();
        $uploadForm = $this->createForm(ImageType::class, $data);
        $uploadForm->handleRequest($request);

        if($uploadForm->isSubmitted() && $uploadForm->isValid()) {

            $data = $uploadForm->getData();

            /**
             * @var UploadedFile $file
             */
            $file = $data["file"];

            $digitalOceanSpaceUploadResult = $digitalOceanSpaceUploader->upload($file);
            return new JsonResponse(array(
                "uri" => $digitalOceanSpaceUploadResult->getPublicUri(),
                "size" => $digitalOceanSpaceUploadResult->getSize(),
                "mime_type" => $digitalOceanSpaceUploadResult->getMimeType()
            ));

        }

        return new Response("", Response::HTTP_BAD_REQUEST);
    }
}
