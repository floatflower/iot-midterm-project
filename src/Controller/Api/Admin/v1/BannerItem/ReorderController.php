<?php

namespace App\Controller\Api\Admin\v1\BannerItem;

use App\Entity\BannerItem;
use App\Repository\BannerItemRepository;
use App\Service\Entity\BannerItem\ReorderService;
use App\Service\Response\ErrorCode;
use App\Service\Response\ErrorResponse;
use App\Service\Response\SuccessResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReorderController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $uuid
     * @param ReorderService $reorderService
     * @return Response
     * @Route("/api/admin/v1/banner-items/{uuid}/reorder", name="api.admin.v1.banner_items.reorder")
     */
    public function __invoke(Request $request, string $uuid, ReorderService $reorderService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var BannerItemRepository $bannerItemRepository
         */
        $bannerItemRepository = $entityManager->getRepository(BannerItem::class);

        $bannerItem = $bannerItemRepository->findOneByUuid($uuid);

        if(!$bannerItem)
            return new ErrorResponse(ErrorCode::BANNER_ITEM_NOT_FOUND, Response::HTTP_NOT_FOUND);

        $requestData = json_decode($request->getContent(), true);

        if(array_key_exists("target", $requestData) && is_int($requestData["target"]))
            $reorderService->reorder($bannerItem, $requestData["target"]);

        return new SuccessResponse(array(), Response::HTTP_ACCEPTED);
    }
}
