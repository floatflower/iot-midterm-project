<?php

namespace App\Controller\Api\PublicAPI\v1;

use App\Entity\AppUser;
use App\Service\Response\SuccessResponse;
use App\Service\Serializer\Api\PublicAPI\v1\UserSerializer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class LoginCheckController extends AbstractController
{
    /**
     * @param Security $security
     * @param UserSerializer $userSerializer
     * @return Response
     * @Route("/api/public/v1/login-check", name="api.public.v1.login_check")
     */
    public function __invoke(Security $security, UserSerializer $userSerializer): Response
    {
        /**
         * @var AppUser $appUser
         */
        $appUser = $security->getUser();

        $responseData = array(
            "loggedIn" => !$appUser->isAnonymous()
        );

        if(!$appUser->isAnonymous()) $responseData["user"] = $userSerializer->serialize($appUser);

        return new SuccessResponse($responseData);
    }
}
