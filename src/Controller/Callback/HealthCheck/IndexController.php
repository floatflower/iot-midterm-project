<?php

namespace App\Controller\Callback\HealthCheck;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @return Response
     * @Route("/callback/health-check", name="callback.health_check")
     */
    public function index(): Response
    {
        // Add system check code
        return new Response("", Response::HTTP_OK);
    }
}
