<?php

namespace App\Controller\App\Website;

use App\Service\GoogleRecaptcha\Validator;
use App\Service\Payment\NewebPay\Exception\NewebPayException;
use App\Service\Payment\NewebPay\NewebPay;
use App\Service\Payment\NewebPay\Transaction;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="app.website.index")
     * @IsGranted("ROLE_ANONYMOUS")
     */
    public function __invoke(): Response
    {
        return $this->redirectToRoute("app.website.profile");
    }
}
