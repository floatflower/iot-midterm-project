<?php

namespace App\Controller\App\Website;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SitemapController extends AbstractController
{
    /**
     * @Route("/sitemap.xml", name="app.website.sitemap.index")
     */
    public function index(): Response
    {
        $response = new Response();
        $response->setContent($this->renderView("app/website/sitemap/index.html.twig"));
        $response->headers->set("Content-Type", "application/xml");
        return $response;
    }

    /**
     * @return Response
     * @Route("/sitemap/page.xml", name="app.website.sitemap.page")
     */
    public function page() : Response
    {
        $response = new Response();
        $response->setContent($this->renderView("app/website/sitemap/page.html.twig"));
        $response->headers->set("Content-Type", "application/xml");
        return $response;
    }
}
