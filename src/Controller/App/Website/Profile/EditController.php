<?php

namespace App\Controller\App\Website\Profile;

use App\Entity\AppUser;
use App\Form\App\Website\Profile\EditType;
use App\Service\Entity\AppUser\Exception\EmailInUsedException;
use App\Service\Entity\AppUser\Exception\MobileInUsedException;
use App\Service\Entity\AppUser\Exception\UsernameInUsedException;
use App\Service\Entity\AppUser\UpdateService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class EditController extends AbstractController
{
    /**
     * @param Request $request
     * @param Security $security
     * @param UpdateService $updateService
     * @return Response
     * @Route("/profile/edit", name="app.website.profile.edit")
     */
    public function index(Request $request,
                          Security $security,
                          UpdateService $updateService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_USER");

        /**
         * @var AppUser $appUser
         */
        $appUser = $security->getUser();

        $data = array(
            "username" => $appUser->getUsername()
        );

        $profileEditForm = $this->createForm(EditType::class, $data);
        $profileEditForm->handleRequest($request);

        if($profileEditForm->isSubmitted() && $profileEditForm->isValid()) {

            $data = $profileEditForm->getData();

            try {
                $updateResult = $updateService->update($appUser, $data);

                return $this->redirectToRoute("app.website.profile");
            } catch (EmailInUsedException $e) {
                $profileEditForm->addError(new FormError("電子郵件已經被使用。"));
            } catch (MobileInUsedException $e) {
                $profileEditForm->addError(new FormError("手機號碼已經被使用。"));
            } catch (UsernameInUsedException $e) {
                $profileEditForm->addError(new FormError("用戶名已經被使用。"));
            }

        }

        return $this->render('app/website/profile/edit/index.html.twig', array(
            "profileEditForm" => $profileEditForm->createView()
        ));
    }
}
