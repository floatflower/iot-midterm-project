<?php

namespace App\Controller\App\Website\Profile\PunchRecords;

use App\Entity\AppUser;
use App\Entity\AppUserPunchRecord;
use App\Repository\AppUserPunchRecordRepository;
use App\Repository\AppUserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class ListController extends AbstractController
{
    /**
     * @Route("/profile/punch-records/list", name="app.website.profile.punch_records.list")
     */
    public function index(Security $security): Response
    {
        $this->denyAccessUnlessGranted("ROLE_USER");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var AppUser $appUser
         */
        $appUser = $security->getUser();
        if(!$appUser){
            return $this->redirectToRoute("app.admin.users.list");
        }

        /**
         * @var AppUserPunchRecordRepository $appUserPunchRecordRepository
         */
        $appUserPunchRecordRepository = $entityManager->getRepository(AppUserPunchRecord::class);

        return $this->render('app/website/profile/punch_records/list/index.html.twig', [
            "appUserPunchRecords" => $appUserPunchRecordRepository->findBy(array("appUser" => $appUser))
        ]);
    }
}
