<?php

namespace App\Controller\App\Website\Profile\LoggedInDevice;

use App\Service\Auth\LoginService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class ForceLogoutController extends AbstractController
{
    /**
     * @param Request $request
     * @param Security $security
     * @param LoginService $loginService
     * @return Response
     * @Route("/profile/logged-in-devices/{session}/force-logout",
     *     name="app.website.profile.logged_in_devices.force_logout")
     */
    public function index(Request $request, Security $security, LoginService $loginService): Response
    {
        return $this->redirectToRoute("app.admin.user.logged_in_devices.list");
    }
}
