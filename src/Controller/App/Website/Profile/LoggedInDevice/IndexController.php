<?php

namespace App\Controller\App\Website\Profile\LoggedInDevice;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/profile/logged-in-devices", name="app.website.profile.logged_in_devices.list")
     */
    public function index(): Response
    {
        return $this->render('app/website/profile/logged-in-device/index.html.twig');
    }
}
