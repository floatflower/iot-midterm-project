<?php

namespace App\Controller\App\Website\Profile;

use App\Entity\AppUser;
use App\Form\App\Website\Profile\UpdateEmailType;
use App\Service\Auth\EmailVerifyService;
use App\Service\Email\EmailVerifyEmailSender;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Security\Core\Security;

class UpdateEmailController extends AbstractController
{
    /**
     * @param Request $request
     * @param Security $security
     * @param EventDispatcherInterface $eventDispatcher
     * @param EmailVerifyService $emailVerifyService
     * @param EmailVerifyEmailSender $emailVerifyEmailSender
     * @return Response
     * @Route("/profile/update/email", name="app.website.profile.update.email")
     */
    public function index(Request $request,
                          Security $security,
                          EventDispatcherInterface $eventDispatcher,
                          EmailVerifyService $emailVerifyService,
                          EmailVerifyEmailSender $emailVerifyEmailSender): Response
    {
        $this->denyAccessUnlessGranted("ROLE_USER");

        /**
         * @var AppUser $appUser
         */
        $appUser = $security->getUser();

        $originalEmail = $appUser->getEmail();

        $data = array(
            "email" => ""
        );

        $updateEmailForm = $this->createForm(UpdateEmailType::class, $data);
        $updateEmailForm->handleRequest($request);

        if($updateEmailForm->isSubmitted() && $updateEmailForm->isValid())
        {
            $data = $updateEmailForm->getData();

            $newEmail = $data["email"];

            if($newEmail !== $originalEmail) {

                $issueVerifyTokenResult = $emailVerifyService->issueVerifyToken($appUser, $newEmail);

                $eventDispatcher->addListener(
                    KernelEvents::TERMINATE,
                    function(TerminateEvent $terminateEvent)
                    use ($emailVerifyEmailSender, $issueVerifyTokenResult, $appUser, $newEmail) {

                        $plainToken = $issueVerifyTokenResult->getPlainToken();

                        $data = array(
                            "verifyLink" => $this->generateUrl("app.auth.email_verify", array("token" => $plainToken), UrlGenerator::ABSOLUTE_URL)
                        );

                        $emailVerifyEmailSender->send($data, $newEmail);
                });
            }

            return $this->render("app/website/profile/update-email/finished.html.twig");
        }

        return $this->render('app/website/profile/update-email/index.html.twig', array(
            "appUser" => $appUser,
            "updateEmailForm" => $updateEmailForm->createView()
        ));
    }
}
