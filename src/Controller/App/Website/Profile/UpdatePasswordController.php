<?php

namespace App\Controller\App\Website\Profile;

use App\Entity\AppUser;
use App\Form\App\Website\Auth\ResetPasswordType;
use App\Form\App\Website\Profile\UpdatePasswordType;
use App\Service\Entity\AppUser\ResetPasswordService;
use App\Service\Entity\AppUser\UpdateService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class UpdatePasswordController extends AbstractController
{
    /**
     * @param Request $request
     * @param Security $security
     * @param ResetPasswordService $resetPasswordService
     * @return Response
     * @Route("/profile/update/password", name="app.website.profile.update_password")
     */
    public function index(Request $request,
                          Security $security,
                          ResetPasswordService $resetPasswordService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_USER");

        $data = array();

        $updatePasswordForm = $this->createForm(UpdatePasswordType::class, $data);
        $updatePasswordForm->handleRequest($request);

        if($updatePasswordForm->isSubmitted() && $updatePasswordForm->isValid()) {

            /**
             * @var AppUser $appUser
             */
            $appUser = $security->getUser();

            $data = $updatePasswordForm->getData();

            $originalPassword = $data["original_password"];
            $newPassword = $data["new_password"];
            $newPasswordConfirm = $data["new_password_confirm"];

            if(!password_verify($originalPassword, $appUser->getPasswordHash()))
            {
                $updatePasswordForm->addError(new FormError("原密碼不正確"));
            }
            else if($newPassword !== $newPasswordConfirm)
            {
                $updatePasswordForm->addError(new FormError("新密碼兩次輸入不相同"));
            }
            else {
                $resetPasswordResult = $resetPasswordService->resetPassword($appUser, $newPassword);
                return $this->render("app/website/profile/update-password/finished.html.twig");
            }
        }

        return $this->render('app/website/profile/update-password/index.html.twig', array(
            "updatePasswordForm" => $updatePasswordForm->createView()
        ));
    }
}
