<?php

namespace App\Controller\App\Website\Profile;

use App\Entity\AppUser;
use App\Form\App\Website\Profile\MobileVerifyType;
use App\Form\App\Website\Profile\UpdateMobileType;
use App\Service\Auth\Exception\MobileVerifyCodeExpiredException;
use App\Service\Auth\Exception\MobileVerifyCodeInvalidException;
use App\Service\Auth\Exception\MobileVerifyCodeNotMatchException;
use App\Service\Auth\Exception\MobileVerifyCodeTryTooManyTimesException;
use App\Service\Auth\MobileVerifyService;
use App\Service\Entity\AppUser\UpdateService;
use App\Service\SMS\SMSGetMessage;
use App\Service\SMS\SMSGetSender;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class UpdateMobileController extends AbstractController
{
    /**
     * @param Request $request
     * @param Security $security
     * @param MobileVerifyService $mobileVerifyService
     * @param SMSGetSender $smsGetSender
     * @param UpdateService $updateService
     * @return Response
     * @Route("/profile/update/mobile", name="app.website.profile.update.mobile")
     */
    public function index(Request $request,
                          Security $security,
                          SMSGetSender $smsGetSender,
                          MobileVerifyService $mobileVerifyService,
                          UpdateService $updateService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_USER");

        /**
         * @var AppUser $appUser
         */
        $appUser = $security->getUser();

        $originalMobile = $appUser->getMobile();

        $data = array(
            "mobile" => $appUser->getMobile()
        );

        $updateMobileForm = $this->createForm(UpdateMobileType::class, $data);
        $updateMobileForm->handleRequest($request);

        if($updateMobileForm->isSubmitted() && $updateMobileForm->isValid())
        {
            $data = $updateMobileForm->getData();

            $newMobile = $data["mobile"];

            if(strlen($newMobile) === 0) {
                $updateService->update($appUser, array(
                    "mobile" => $newMobile
                ));

                return $this->redirectToRoute("app.website.profile");
            }

            else if($newMobile !== $originalMobile) {

                $issueVerifyCodeResult = $mobileVerifyService->issueVerifyCode($appUser, $newMobile);

                $session = $issueVerifyCodeResult->getAppUserMobileVerifyCode()->getSession();
                $code = $issueVerifyCodeResult->getCode();

                $smsgetMessage = new SMSGetMessage();
                $smsgetMessage->setMessage("您的手機號碼更改驗證碼為：${code}，請在五分鐘內使用此驗證碼");
                $smsgetMessage->setNumber($newMobile);

                $smsGetSender->send($smsgetMessage);

                return $this->redirectToRoute(
                    "app.website.profile.update.mobile.check",
                    array("session" => $session));

            }
        }

        return $this->render('app/website/profile/update-mobile/index.html.twig', array(
            "appUser" => $appUser,
            "updateMobileForm" => $updateMobileForm->createView()
        ));
    }

    /**
     * @param Request $request
     * @param string $session
     * @param Security $security
     * @param MobileVerifyService $mobileVerifyService
     * @return Response
     * @Route("/profile/update/mobile/{session}", name="app.website.profile.update.mobile.check")
     */
    public function verify(Request $request, string $session, Security $security, MobileVerifyService $mobileVerifyService) : Response
    {
        $this->denyAccessUnlessGranted("ROLE_USER");

        $data = array();

        $mobileVerifyForm = $this->createForm(MobileVerifyType::class, $data);
        $mobileVerifyForm->handleRequest($request);

        if($mobileVerifyForm->isSubmitted() && $mobileVerifyForm->isValid())
        {
            $data = $mobileVerifyForm->getData();

            $code = $data["code"];

            try {
                $verifyResult = $mobileVerifyService->verifyByCode($session, $code);

                return $this->redirectToRoute("app.website.profile");
            } catch (MobileVerifyCodeExpiredException $e) {
                return $this->render("app/website/profile/update-mobile/session-expired.html.twig");
            } catch (MobileVerifyCodeInvalidException $e) {
                return $this->render("app/website/profile/update-mobile/session-invalid.html.twig");
            } catch (MobileVerifyCodeNotMatchException $e) {
                $mobileVerifyForm->addError(new FormError("驗證碼不正確"));
            } catch (MobileVerifyCodeTryTooManyTimesException $e) {
                return $this->render("app/website/profile/update-mobile/try-too-many-times.html.twig");
            }
        }

        return $this->render("app/website/profile/update-mobile/check.html.twig", array(
            "mobileVerifyForm" => $mobileVerifyForm->createView()
        ));
    }
}
