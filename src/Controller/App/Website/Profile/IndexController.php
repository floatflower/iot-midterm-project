<?php

namespace App\Controller\App\Website\Profile;

use App\Entity\AppUser;
use App\Entity\AppUserPunchRecord;
use App\Service\PunchQrCode\createService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class IndexController extends AbstractController
{
    /**
     * @param Security $security
     * @param createService $createService
     * @return Response
     * @Route("/profile", name="app.website.profile")
     */
    public function __invoke(Security $security, createService $createService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_USER");

        /**
         * @var AppUser $appUser
         */
        $appUser = $security->getUser();

        $punchJWT = $createService->generate($appUser, AppUserPunchRecord::TYPE_PUNCH_IN);

        return $this->render('app/website/profile/index.html.twig', array(
            "appUser" => $appUser,
            "punchJWT" => $punchJWT
        ));
    }
}
