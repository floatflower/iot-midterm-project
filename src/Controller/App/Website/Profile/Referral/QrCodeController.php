<?php

namespace App\Controller\App\Website\Profile\Referral;

use App\Entity\AppUser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class QrCodeController extends AbstractController
{
    /**
     * @param Request $request
     * @param Security $security
     * @return Response
     * @Route("/profile/referrer/qrcode", name="app.website.profile.referrer.qr_code")
     */
    public function index(Request $request, Security $security): Response
    {
        $this->denyAccessUnlessGranted("ROLE_USER");

        /**
         * @var AppUser $appUser
         */
        $appUser = $security->getUser();

        return $this->render('app/website/profile/referral/qrcode/index.html.twig', array(
            "appUser" => $appUser
        ));
    }
}
