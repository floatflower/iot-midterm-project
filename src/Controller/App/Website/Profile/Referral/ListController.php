<?php

namespace App\Controller\App\Website\Profile\Referral;

use App\Entity\AppUser;
use App\Repository\AppUserRepository;
use App\Security\Voter\AppUserVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class ListController extends AbstractController
{
    private int $defaultLimit = 50;
    private int $defaultMaxLimit = 100;

    public function __construct()
    {

    }

    /**
     * @param Request $request
     * @param Security $security
     * @return Response
     * @Route("/profile/referrals", name="app.website.profile.referral.list")
     */
    public function index(Request $request, Security $security): Response
    {
        $this->denyAccessUnlessGranted("ROLE_USER");

        $page = $request->query->get("page", 1);
        $limit = $request->query->get("limit", $this->defaultLimit);

        $page = max(1, $page);
        $limit = min($this->defaultMaxLimit, $limit);
        $limit = max(1, $limit);

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var AppUser $appUser
         */
        $appUser = $security->getUser();

        /**
         * @var AppUserRepository $appUserRepository
         */
        $appUserRepository = $entityManager->getRepository(AppUser::class);

        if(!$appUser) {
            return $this->redirectToRoute("app.admin.users.list");
        }

        $this->denyAccessUnlessGranted(AppUserVoter::APP_USER_FETCH, $appUser);

        $criteria = array("referrer" => $appUser);
        $referrals = $appUserRepository->findBy($criteria, array("createAt" => "DESC"), $limit, ($page - 1) * $limit);
        $count = $appUserRepository->count($criteria);

        return $this->render('app/website/profile/referral/list/index.html.twig', array(
            "referrals" => $referrals,
            "appUser" => $appUser,
            "page" => $page,
            "limit" => $limit,
            "count" => $count,
            "maxPage" => ceil($count / $limit),
            "searchMode" => false
        ));
    }
}
