<?php

namespace App\Controller\App\Website\Referral;

use App\Entity\AppUser;
use App\Repository\AppUserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $referralCode
     * @return Response
     * @Route("/referral/{referralCode}", name="app.website.referral_code")
     */
    public function index(Request $request, string $referralCode): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var AppUserRepository $appUserRepository
         */
        $appUserRepository = $entityManager->getRepository(AppUser::class);
        $appUser = $appUserRepository->findOneByReferralCode($referralCode);

        $response = new RedirectResponse("/");

        if($appUser) {
            $response->headers->setCookie(new Cookie("referrer", $appUser->getUuid()));
        }

        return $response;
    }
}
