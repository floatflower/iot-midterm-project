<?php

namespace App\Controller\App\Website\Auth\Login;

use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DiscordLoginController extends AbstractController
{
    /**
     * @param Request $request
     * @param ClientRegistry $clientRegistry
     * @return Response
     * @Route("/auth/login/oauth2/discord",
     *     name="app.website.auth.login.oauth2.discord.connect")
     */
    public function connect(Request $request, ClientRegistry $clientRegistry): Response
    {

    }

    /**
     * @param Request $request
     * @param ClientRegistry $clientRegistry
     * @return Response
     * @Route("/auth/login/oauth2/discord/check",
     *     name="app.website.auth.login.oauth2.discord.check")
     */
    public function check(Request $request, ClientRegistry $clientRegistry) : Response
    {

    }
}
