<?php

namespace App\Controller\App\Website\Auth\Login;

use App\Entity\AppUser;
use App\Form\App\Auth\LoginType;
use App\Form\App\Website\Auth\Login\MobileLoginType;
use App\Form\App\Website\Auth\Login\UsernameLoginType;
use App\Repository\AppUserRepository;
use App\Service\Auth\Exception\UserExpiredException;
use App\Service\Auth\Exception\UsernameOrPasswordInvalidException;
use App\Service\Auth\LoginService;
use App\Service\Auth\Result\LoginResult;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @param Request $request
     * @param LoginService $loginService
     * @return Response
     * @Route("/auth/login", name="app.auth.login")
     */
    public function index(Request $request, LoginService $loginService): Response
    {
        $data = array();
        $usernameLoginForm = $this->createForm(UsernameLoginType::class, $data, array(
            "method" => "POST"
        ));

        $usernameLoginForm->handleRequest($request);

        if($usernameLoginForm->isSubmitted() && $usernameLoginForm->isValid()) {
            $data = $usernameLoginForm->getData();

            $username = $data["username"];
            $password = $data["password"];

            if(is_null($username) || strlen($username) === 0)
                $usernameLoginForm->addError(new FormError("用戶名不得為空"));
            else if(is_null($password) || strlen($password) === 0)
                $usernameLoginForm->addError(new FormError("密碼不得為空"));
            else {
                try {

                    $entityManager = $this->getDoctrine()->getManager();

                    /**
                     * @var AppUserRepository $appUserRepository
                     */
                    $appUserRepository = $entityManager->getRepository(AppUser::class);

                    $appUser = $appUserRepository->findByLoginKey($username);
                    if ($appUser && password_verify($password, $appUser->getPasswordHash())) {
                        $loginResult = $loginService->login($appUser);

                        $jwt = $loginResult->getJwt();

                        $response = new RedirectResponse($this->generateUrl("app.website.profile"));
                        $response->headers->setCookie(new Cookie("auth_token", $jwt));

                        return $response;
                    }

                    $usernameLoginForm->addError(new FormError("用戶名或密碼不正確"));

                } catch (UserExpiredException $userExpiredException) {
                    $usernameLoginForm->addError(new FormError("該帳號已經過期，無法再登入"));
                }
            }

        }

        return $this->render("app/website/auth/login/index.html.twig", array(
            "usernameLoginForm" => $usernameLoginForm->createView()
        ));
    }
}
