<?php

namespace App\Controller\App\Website\Auth\Login;

use App\Entity\AppUser;
use App\Form\App\Website\Auth\Login\MobileLoginCheckType;
use App\Form\App\Website\Auth\Login\MobileLoginType;
use App\Repository\AppUserRepository;
use App\Service\Auth\Exception\OneTimePinExpiredException;
use App\Service\Auth\Exception\OneTimePinInvalidException;
use App\Service\Auth\Exception\OneTimePinNotMatchException;
use App\Service\Auth\Exception\OneTimePinTryTooManyTimesException;
use App\Service\Auth\Exception\UserExpiredException;
use App\Service\Auth\LoginService;
use App\Service\SMS\SMSGetMessage;
use App\Service\SMS\SMSGetSender;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MobileLoginController extends AbstractController
{
    /**
     * @param Request $request
     * @param LoginService $loginService
     * @param SMSGetSender $smsGetSender
     * @return Response
     * @throws \App\Service\SMS\Exception\SMSGet\IPInaccessibleException
     * @throws \App\Service\SMS\Exception\SMSGet\MessageTooLongException
     * @throws \App\Service\SMS\Exception\SMSGet\NoReceiverException
     * @throws \App\Service\SMS\Exception\SMSGet\OutOfPointsException
     * @throws \App\Service\SMS\Exception\SMSGet\ParameterErrorException
     * @throws \App\Service\SMS\Exception\SMSGet\ScheduleTimeExpiredException
     * @throws \App\Service\SMS\Exception\SMSGet\ScheduleTimeParameterErrorException
     * @throws \App\Service\SMS\Exception\SMSGet\TooManyReceiversException
     * @throws \App\Service\SMS\Exception\SMSGet\UsernameOrPasswordInvalidException
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @Route("/auth/login/method/mobile",
     *     name="app.website.auth.login.method.mobile")
     */
    public function index(Request $request,
                          LoginService $loginService,
                          SMSGetSender $smsGetSender): Response
    {
        $data = array();
        $mobileLoginForm = $this->createForm(MobileLoginType::class, $data);

        $mobileLoginForm->handleRequest($request);

        if($mobileLoginForm->isSubmitted() && $mobileLoginForm->isValid())
        {
            $data = $mobileLoginForm->getData();

            $mobile = $data["mobile"];

            $entityManager = $this->getDoctrine()->getManager();

            /**
             * @var AppUserRepository $appUserRepository
             */
            $appUserRepository = $entityManager->getRepository(AppUser::class);

            $appUser = $appUserRepository->findOneByMobile($mobile);

            if($appUser) {
                $issueOneTimePinResult = $loginService->issueOneTimePin($appUser);

                $code = $issueOneTimePinResult->getCode();

                $smsgetMessage = new SMSGetMessage();
                $smsgetMessage->setMessage("您的登入驗證碼為：${code}，請在五分鐘內使用此驗證碼");
                $smsgetMessage->setNumber($appUser->getMobile());

                $smsGetSender->send($smsgetMessage);

                $session = $issueOneTimePinResult->getAppUserOneTimePin()->getSession();

                return $this->redirectToRoute(
                    "app.website.auth.login.method.mobile.session.check",
                    array("session" => $session));
            }

            $mobileLoginForm->addError(new FormError("手機號碼錯誤或不存在。"));
        }

        return $this->render('app/website/auth/login/mobile-login/index.html.twig', array(
            "mobileLoginForm" => $mobileLoginForm->createView()
        ));
    }

    /**
     * @param Request $request
     * @param string $session
     * @param LoginService $loginService
     * @return Response
     * @Route("/auth/login/method/mobile/{session}/check",
     *     name="app.website.auth.login.method.mobile.session.check")
     */
    public function check(Request $request,
                          string $session,
                          LoginService $loginService) : Response
    {

        $data = array();

        $mobileLoginCheckForm = $this->createForm(MobileLoginCheckType::class, $data);
        $mobileLoginCheckForm->handleRequest($request);

        if($mobileLoginCheckForm->isSubmitted() && $mobileLoginCheckForm->isValid()) {

            $data = $mobileLoginCheckForm->getData();
            $code = $data["code"];


            try {
                $loginResult = $loginService->loginWithOneTimePin($session, $code);

                $jwt = $loginResult->getJwt();

                $response = new RedirectResponse($this->generateUrl("app.website.profile"));
                $response->headers->setCookie(new Cookie("auth_token", $jwt));

                return $response;
            } catch (OneTimePinExpiredException $e) {
                return $this->render("app/website/auth/login/mobile-login/one-time-pin-expired.html.twig");
            } catch (OneTimePinInvalidException $e) {
                return $this->render("app/website/auth/login/mobile-login/one-time-pin-invalid.html.twig");
            } catch (OneTimePinNotMatchException $e) {
                $mobileLoginCheckForm->addError(new FormError("驗證碼不正確"));
            } catch (OneTimePinTryTooManyTimesException $e) {
                return $this->render("app/website/auth/login/mobile-login/one-time-pin-try-too-many-times.html.twig");
            } catch (UserExpiredException $e) {
                return $this->render("app/website/auth/login/mobile-login/user-expired.html.twig");
            }
        }

        return $this->render('app/website/auth/login/mobile-login/check.html.twig', array(
            "mobileLoginCheckForm" => $mobileLoginCheckForm->createView()
        ));
    }
}
