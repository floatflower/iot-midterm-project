<?php

namespace App\Controller\App\Website\Auth\Login;

use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GoogleLoginController extends AbstractController
{
    /**
     * @param Request $request
     * @param ClientRegistry $clientRegistry
     * @return Response
     * @Route("/auth/login/oauth2/google",
     *     name="app.website.auth.login.oauth2.google.connect")
     */
    public function connect(Request $request, ClientRegistry $clientRegistry): Response
    {

    }

    /**
     * @param Request $request
     * @param ClientRegistry $clientRegistry
     * @return Response
     * @Route("/auth/login/oauth2/google/check",
     *     name="app.website.auth.login.oauth2.google.check")
     */
    public function check(Request $request, ClientRegistry $clientRegistry) : Response
    {

    }
}
