<?php

namespace App\Controller\App\Website\Auth;

use App\Service\Auth\EmailVerifyService;
use App\Service\Auth\Exception\EmailVerifyTokenExpiredException;
use App\Service\Auth\Exception\EmailVerifyTokenInvalidException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EmailVerifyController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $token
     * @param EmailVerifyService $emailVerifyService
     * @return Response
     * @Route("/auth/email-verify/{token}", name="app.auth.email_verify")
     */
    public function __invoke(Request $request,
                          string $token,
                          EmailVerifyService $emailVerifyService): Response
    {
        try {
            $emailVerifyResult = $emailVerifyService->verifyByToken($token);

            return $this->render("app/website/auth/email-verify/finished.html.twig");
        } catch (EmailVerifyTokenExpiredException $e) {
            return $this->render("app/website/auth/email-verify/token-expired.html.twig");
        } catch (EmailVerifyTokenInvalidException $e) {
            return $this->render("app/website/auth/email-verify/token-invalid.html.twig");
        }
    }
}
