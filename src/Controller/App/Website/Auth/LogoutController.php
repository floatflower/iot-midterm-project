<?php

namespace App\Controller\App\Website\Auth;

use App\Service\Auth\LoginService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LogoutController extends AbstractController
{
    /**
     * @param Request $request
     * @param LoginService $loginService
     * @return Response
     * @Route("/auth/logout", name="app.auth.logout")
     */
    public function __invoke(Request $request, LoginService $loginService): Response
    {
        $authToken = $request->cookies->get("auth_token", null);

        // auth_token 存於 cookie
        if(!is_null($authToken)) $loginService->logout($authToken);
        else {
            $authToken = $request->headers->get("Authorization", null);

            // auth_token 存於 Authorization Header 中
            if(!is_null($authToken) && preg_match("/^Bearer \S+$/", $authToken)) {
                $authToken = substr($authToken, 7);
                $loginService->logout($authToken);
            }
        }

        $response = new RedirectResponse($this->generateUrl("app.website.index"));
        $response->headers->setCookie(new Cookie("auth_token", null));
        return $response;
    }
}
