<?php

namespace App\Controller\App\Website\Auth;

use App\Entity\AppUser;
use App\Form\App\Website\Auth\MobileForgetPasswordCheckType;
use App\Form\App\Website\Auth\UsernameForgetPasswordType;
use App\Form\Website\Auth\MobileForgetPasswordType;
use App\Service\Auth\Exception\OneTimePinExpiredException;
use App\Service\Auth\Exception\OneTimePinInvalidException;
use App\Service\Auth\Exception\OneTimePinNotMatchException;
use App\Service\Auth\Exception\OneTimePinTryTooManyTimesException;
use App\Service\Auth\Exception\UserNotFoundException;
use App\Service\Auth\ForgetPasswordService;
use App\Service\SMS\SMSGetMessage;
use App\Service\SMS\SMSGetSender;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Annotation\Route;

class ForgetPasswordController extends AbstractController
{
    /**
     * @param Request $request
     * @param ForgetPasswordService $forgetPasswordService
     * @return Response
     * @Route("/auth/forget-password/method/username", name="app.auth.forget_password.method.username")
     */
    public function usernameMethod(Request $request,
                                   ForgetPasswordService $forgetPasswordService): Response
    {
        $data = array();
        $forgetPasswordForm = $this->createForm(UsernameForgetPasswordType::class, $data);
        $forgetPasswordForm->handleRequest($request);

        if($forgetPasswordForm->isSubmitted() && $forgetPasswordForm->isValid()) {

            $data = $forgetPasswordForm->getData();

            $username = $data["username"];

            try {
                $issueResetPasswordTokenResult = $forgetPasswordService->issueResetPasswordToken($username);

                // TODO: 寄送電子郵件
            } catch (UserNotFoundException $userNotFoundException) {}

            return $this->render("app/website/auth/forget-password/finished.html.twig");
        }

        return $this->render("app/website/auth/forget-password/index.html.twig", array(
            "forgetPasswordForm" => $forgetPasswordForm->createView()
        ));
    }

    /**
     * @param Request $request
     * @param ForgetPasswordService $forgetPasswordService
     * @param SMSGetSender $smsGetSender
     * @return Response
     * @Route("/auth/forget-password/method/mobile", name="app.auth.forget_password.method.mobile")
     */
    public function mobileMethod(Request $request,
                                 ForgetPasswordService $forgetPasswordService,
                                 SMSGetSender $smsGetSender) : Response
    {
        $data = array();

        $mobileForgetPasswordForm = $this->createForm(MobileForgetPasswordType::class, $data);
        $mobileForgetPasswordForm->handleRequest($request);

        if($mobileForgetPasswordForm->isSubmitted() && $mobileForgetPasswordForm->isValid())
        {
            $data = $mobileForgetPasswordForm->getData();

            $mobile = $data["mobile"];

            $entityManager = $this->getDoctrine()->getManager();
            $appUserRepository = $entityManager->getRepository(AppUser::class);
            $appUser = $appUserRepository->findOneBy(array("mobile" => $mobile));

            if($appUser) {

                $issueOneTimePinResult = $forgetPasswordService->issueOneTimePin($appUser);

                $code = $issueOneTimePinResult->getCode();

                $smsgetMessage = new SMSGetMessage();
                $smsgetMessage->setMessage("您的重設密碼的驗證碼為：${code}，請在五分鐘內使用此驗證碼");
                $smsgetMessage->setNumber($appUser->getMobile());

                $smsGetSender->send($smsgetMessage);

                $session = $issueOneTimePinResult->getAppUserOneTimePin()->getSession();

                return $this->redirectToRoute(
                    "app.auth.forget_password.method.mobile.check",
                    array("session" => $session));
            }

            $mobileForgetPasswordForm->addError(new FormError("手機號碼不存在。"));
        }

        return $this->render("app/website/auth/forget-password/mobile-method.html.twig", array(
            "mobileForgetPasswordForm" => $mobileForgetPasswordForm->createView()
        ));
    }

    /**
     * @param Request $request
     * @param string $session
     * @param ForgetPasswordService $forgetPasswordService
     * @return Response
     * @Route("/auth/forget-password/method/mobile/{session}", name="app.auth.forget_password.method.mobile.check")
     */
    public function mobileMethodCheck(Request $request,
                                      string $session,
                                      ForgetPasswordService $forgetPasswordService) : Response
    {
        $data = array();

        $mobileForgetPasswordCheckForm = $this->createForm(MobileForgetPasswordCheckType::class, $data);
        $mobileForgetPasswordCheckForm->handleRequest($request);

        if($mobileForgetPasswordCheckForm->isSubmitted() && $mobileForgetPasswordCheckForm->isValid())
        {
            $data = $mobileForgetPasswordCheckForm->getData();

            $code = $data["code"];

            try {
                $issueResetPasswordTokenResult = $forgetPasswordService->issueResetPasswordTokenWithOneTimePin($session, $code);

                return $this->redirectToRoute("app.auth.reset_password", array(
                    "token" => $issueResetPasswordTokenResult->getPlainToken()
                ));
            } catch (OneTimePinExpiredException $e) {
                return $this->render("app/website/auth/forget-password/one-time-pin-expired.html.twig");
            } catch (OneTimePinInvalidException $e) {
                return $this->render("app/website/auth/forget-password/one-time-pin-invalid.html.twig");
            } catch (OneTimePinNotMatchException $e) {
                $mobileForgetPasswordCheckForm->addError(new FormError("驗證碼不正確"));
            } catch (OneTimePinTryTooManyTimesException $e) {
                return $this->render("app/website/auth/forget-password/one-time-pin-try-too-many-times.html.twig");
            }
        }

        return $this->render("app/website/auth/forget-password/mobile-method-check.html.twig", array(
            "mobileForgetPasswordCheckForm" => $mobileForgetPasswordCheckForm->createView()
        ));
    }
}
