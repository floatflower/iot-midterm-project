<?php

namespace App\Controller\App\Website\Auth;

use App\Form\App\Website\Auth\ResetPasswordType;
use App\Service\Auth\Exception\ResetPasswordTokenExpiredException;
use App\Service\Auth\Exception\ResetPasswordTokenInvalidException;
use App\Service\Auth\ForgetPasswordService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ResetPasswordController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $token
     * @param ForgetPasswordService $forgetPasswordService
     * @return Response
     * @Route("/auth/reset-password/{token}", name="app.auth.reset_password")
     */
    public function __invoke(Request $request,
                          string $token,
                          ForgetPasswordService $forgetPasswordService): Response
    {
        $data = array();
        $resetPasswordForm = $this->createForm(ResetPasswordType::class, $data);
        $resetPasswordForm->handleRequest($request);

        if($resetPasswordForm->isSubmitted() && $resetPasswordForm->isValid()) {

            $data = $resetPasswordForm->getData();

            $newPassword = $data['new_password'];
            $newPasswordConfirm = $data['new_password_confirm'];

            if($newPassword !== $newPasswordConfirm) {
                $resetPasswordForm->addError(new FormError("兩次密碼不相同"));
            }
            else if(is_null($newPassword) || strlen($newPassword) === 0) {
                $resetPasswordForm->addError(new FormError("請輸入新密碼"));
            }
            else if(is_null($newPasswordConfirm) || strlen($newPasswordConfirm) === 0) {
                $resetPasswordForm->addError(new FormError("請確認新密碼"));
            }
            else {
                try {
                    $forgetPasswordService->resetPassword($token, $newPassword);
                    return $this->render("app/website/auth/reset-password/finished.html.twig");
                } catch (ResetPasswordTokenInvalidException $resetPasswordTokenInvalidException) {
                    return $this->render("app/website/auth/reset-password/token-invalid.html.twig");
                } catch (ResetPasswordTokenExpiredException $e) {
                    return $this->render("app/website/auth/reset-password/token-expired.html.twig");
                }
            }

        }

        return $this->render("app/website/auth/reset-password/index.html.twig", array(
            "resetPasswordForm" => $resetPasswordForm->createView()
        ));
    }
}
