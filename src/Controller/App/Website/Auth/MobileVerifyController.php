<?php

namespace App\Controller\App\Website\Auth;

use App\Form\App\Website\Auth\MobileVerifyType;
use App\Service\Auth\Exception\MobileVerifyCodeExpiredException;
use App\Service\Auth\Exception\MobileVerifyCodeInvalidException;
use App\Service\Auth\Exception\MobileVerifyCodeNotMatchException;
use App\Service\Auth\Exception\MobileVerifyCodeTryTooManyTimesException;
use App\Service\Auth\MobileVerifyService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MobileVerifyController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $session
     * @return Response
     * @Route("/auth/mobile-verify/{session}", name="app.website.auth.mobile_verify")
     */
    public function index(Request $request, string $session, MobileVerifyService $mobileVerifyService): Response
    {
        $data = array();

        $mobileVerifyForm = $this->createForm(MobileVerifyType::class, $data);
        $mobileVerifyForm->handleRequest($request);

        if($mobileVerifyForm->isSubmitted() && $mobileVerifyForm->isValid())
        {
            $data = $mobileVerifyForm->getData();

            $code = $data["code"];

            try {
                $mobileVerifyResult = $mobileVerifyService->verifyByCode($session, $code);

                return $this->redirectToRoute("app.website.profile");
            } catch (MobileVerifyCodeExpiredException $e) {
                return $this->render("app/website/auth/mobile-verify/session-expired.html.twig");
            } catch (MobileVerifyCodeInvalidException $e) {
                return $this->render("app/website/auth/mobile-verify/session-invalid.html.twig");
            } catch (MobileVerifyCodeNotMatchException $e) {
                $mobileVerifyForm->addError(new FormError("驗證碼不正確"));
            } catch (MobileVerifyCodeTryTooManyTimesException $e) {
                return $this->render("app/website/auth/mobile-verify/try-too-many-times.html.twig");
            }
        }

        return $this->render("app/website/auth/mobile-verify/index.html.twig", array(
            "mobileVerifyForm" => $mobileVerifyForm->createView()
        ));
    }
}
