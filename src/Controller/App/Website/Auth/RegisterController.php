<?php

namespace App\Controller\App\Website\Auth;

use App\Service\Auth\RegisterByEmailService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RegisterController extends AbstractController
{
    /**
     * @param Request $request
     * @return Response
     * @Route("/auth/register", name="app.auth.register")
     */
    public function __invoke(Request $request): Response
    {
        return $this->render('app/website/auth/register/index.html.twig', [
            'controller_name' => 'RegisterController',
        ]);
    }
}
