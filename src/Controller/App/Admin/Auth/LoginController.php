<?php

namespace App\Controller\App\Admin\Auth;

use App\Form\App\Admin\Auth\LoginType;
use App\Service\Auth\Exception\UserExpiredException;
use App\Service\Auth\Exception\UsernameOrPasswordInvalidException;
use App\Service\Auth\LoginService;
use App\Service\Auth\Result\LoginResult;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LoginController extends AbstractController
{
    /**
     * @param Request $request
     * @param LoginService $loginService
     * @return Response
     * @Route("/admin/auth/login", name="app.admin.auth.login")
     */
    public function __invoke(Request $request, LoginService $loginService): Response
    {
        $data = array();

        $loginForm = $this->createForm(LoginType::class, $data);
        $loginForm->handleRequest($request);

        if($loginForm->isSubmitted() && $loginForm->isValid()) {

            $data = $loginForm->getData();

            $username = $data["username"] ?? null;
            $password = $data["password"] ?? null;

            if(is_null($username) || strlen($username) === 0)
                $loginForm->addError(new FormError("用戶名不得為空"));
            else if(is_null($password) || strlen($password) === 0)
                $loginForm->addError(new FormError("密碼不得為空"));
            else {
                try {
                    $loginResult = $loginService->loginWithUsername($username, $password);

                    $jwt = $loginResult->getJwt();
                    $response = new RedirectResponse($this->generateUrl("app.admin.index"));
                    $response->headers->setCookie(new Cookie("auth_token", $jwt));

                    return $response;

                } catch (UsernameOrPasswordInvalidException $usernameOrPasswordInvalidException) {
                    $loginForm->addError(new FormError("用戶名或密碼不正確"));
                } catch (UserExpiredException $userExpiredException) {
                    $loginForm->addError(new FormError("該帳號已經過期，無法再登入"));
                }
            }

        }

        return $this->render('app/admin/auth/login/index.html.twig', [
            "loginForm" => $loginForm->createView()
        ]);
    }
}
