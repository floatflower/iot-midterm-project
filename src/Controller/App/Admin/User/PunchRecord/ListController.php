<?php

namespace App\Controller\App\Admin\User\PunchRecord;

use App\Entity\AppUser;
use App\Entity\AppUserPunchRecord;
use App\Repository\AppUserPunchRecordRepository;
use App\Repository\AppUserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{
    /**
     * @Route("/admin/users/{uuid}/punch-records", name="app.admin.user.punch_records.list")
     */
    public function index(Request $request, string $uuid): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var AppUserRepository $appUserRepository
         */
        $appUserRepository = $entityManager->getRepository(AppUser::class);

        $appUser = $appUserRepository->findOneBy(array("uuid" => $uuid));
        if(!$appUser){
            return $this->redirectToRoute("app.admin.users.list");
        }

        /**
         * @var AppUserPunchRecordRepository $appUserPunchRecordRepository
         */
        $appUserPunchRecordRepository = $entityManager->getRepository(AppUserPunchRecord::class);

        return $this->render('app/admin/user/punch_record/list/index.html.twig', [
            "appUserPunchRecords" => $appUserPunchRecordRepository->findBy(array("appUser" => $appUser))
        ]);
    }
}
