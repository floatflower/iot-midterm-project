<?php

namespace App\Controller\App\Admin\User\LoggedInDevice;

use App\Entity\AppUserAuthToken;
use App\Repository\AppUserAuthTokenRepository;
use App\Service\Auth\LoginService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ForceLogoutController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $session
     * @param LoginService $loginService
     * @return Response
     * @Route("/admin/users/logged-in-devices/{session}/force-logout", name="app.admin.users.logged_in_devices.force_logout")
     */
    public function index(Request $request, string $session, LoginService $loginService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var AppUserAuthTokenRepository $appUserAuthTokenRepository
         */
        $appUserAuthTokenRepository = $entityManager->getRepository(AppUserAuthToken::class);

        $appUserAuthToken = $appUserAuthTokenRepository->findOneBy(array("session" => $session));

        $loggedOutUser = $appUserAuthToken->getAppUser();

        $loginService->logoutWithSession($appUserAuthToken->getSession());

        return $this->redirectToRoute("app.admin.user.logged_in_devices.list", array(
            "uuid" => $loggedOutUser->getUuid()
        ));
    }
}
