<?php

namespace App\Controller\App\Admin\User\LoggedInDevice;

use App\Entity\AppUser;
use App\Entity\AppUserAuthToken;
use App\Repository\AppUserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    private int $defaultLimit = 50;
    private int $defaultMaxLimit = 100;

    public function __construct()
    {

    }

    /**
     * @param Request $request
     * @param string $uuid
     * @return Response
     * @Route("/admin/users/{uuid}/logged-in-devices", name="app.admin.user.logged_in_devices.list")
     */
    public function index(Request $request, string $uuid): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        $page = $request->query->get("page", 1);
        $limit = $request->query->get("limit", $this->defaultLimit);

        $page = max(1, $page);
        $limit = min($this->defaultMaxLimit, $limit);
        $limit = max(1, $limit);

        /**
         * @var AppUserRepository $appUserRepository
         */
        $appUserRepository = $entityManager->getRepository(AppUser::class);
        $appUser = $appUserRepository->findOneByUuid($uuid);

        if(!$appUser) {
            return $this->redirectToRoute("app.admin.users.list");
        }

        $appUserAuthTokenRepository = $entityManager->getRepository(AppUserAuthToken::class);

        $criteria = array("appUser" => $appUser);
        $appUserAuthTokens = $appUserAuthTokenRepository->findBy($criteria, array("createAt" => "DESC"), $limit, ($page - 1) * $limit);
        $count = $appUserAuthTokenRepository->count($criteria);

        return $this->render('app/admin/user/logged-in-device/index.html.twig', array(
            "loggedInDevices" => $appUserAuthTokens,
            "appUser" => $appUser,
            "page" => $page,
            "limit" => $limit,
            "count" => $count,
            "maxPage" => ceil($count / $limit),
            "searchMode" => false
        ));
    }
}
