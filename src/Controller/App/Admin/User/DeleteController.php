<?php

namespace App\Controller\App\Admin\User;

use App\Entity\AppUser;
use App\Repository\AppUserRepository;
use App\Security\Voter\AppUserVoter;
use App\Service\Entity\AppUser\DeleteService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DeleteController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $uuid
     * @param DeleteService $deleteService
     * @return Response
     * @Route("/admin/users/{uuid}/delete", name="app.admin.users.delete")
     */
    public function __invoke(Request $request, string $uuid, DeleteService $deleteService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var AppUserRepository $appUserRepository
         */
        $appUserRepository = $entityManager->getRepository(AppUser::class);
        $appUser = $appUserRepository->findOneByUuid($uuid);

        $this->denyAccessUnlessGranted(AppUserVoter::APP_USER_DELETE, $appUser);

        if($appUser) {
            $deleteService->delete($appUser);
        }

        return $this->redirectToRoute("app.admin.users.list");
    }
}
