<?php

namespace App\Controller\App\Admin\User;

use App\Entity\AppUser;
use App\Entity\AppUserAuthToken;
use App\Entity\AppUserPunchRecord;
use App\Repository\AppUserAuthTokenRepository;
use App\Repository\AppUserPunchRecordRepository;
use App\Repository\AppUserRepository;
use App\Security\Voter\AppUserVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FetchController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $uuid
     * @return Response
     * @Route("/admin/users/{uuid}", name="app.admin.users.fetch")
     */
    public function __invoke(Request $request, string $uuid): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var AppUserRepository $appUserRepository
         */
        $appUserRepository = $entityManager->getRepository(AppUser::class);
        $appUser = $appUserRepository->findOneByUuid($uuid);

        if(!$appUser) {
            return $this->redirectToRoute("app.admin.users.list");
        }

        /**
         * @var AppUserAuthTokenRepository $appUserAuthTokenRepository
         */
        $appUserAuthTokenRepository = $entityManager->getRepository(AppUserAuthToken::class);

        $this->denyAccessUnlessGranted(AppUserVoter::APP_USER_FETCH, $appUser);

        /**
         * @var AppUserPunchRecordRepository $appUserPunchRecordRepository
         */
        $appUserPunchRecordRepository = $entityManager->getRepository(AppUserPunchRecord::class);

        return $this->render('app/admin/user/fetch/index.html.twig', array(
            "appUser" => $appUser,
            "loggedDevicesCount" => $appUserAuthTokenRepository->count(array("appUser" => $appUser)),
            "referralsCount" => $appUserRepository->count(array("referrer" => $appUser)),
            "punchRecordCount" => $appUserPunchRecordRepository->count(array("appUser" => $appUser))
        ));
    }
}
