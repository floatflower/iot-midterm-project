<?php

namespace App\Controller\App\Admin\User;

use App\Entity\AppUser;
use App\Repository\AppUserRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{
    private int $defaultLimit = 50;
    private int $defaultMaxLimit = 100;

    public function __construct()
    {

    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/admin/users", name="app.admin.users.list")
     */
    public function __invoke(Request $request): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $search = $request->query->get("search");

        // uncomment if search is enabled
        if($search) return $this->handleSearch($request);

        $page = $request->query->get("page", 1);
        $limit = $request->query->get("limit", $this->defaultLimit);

        $page = max(1, $page);
        $limit = min($this->defaultMaxLimit, $limit);
        $limit = max(1, $limit);

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var AppUserRepository $appUserRepository
         */
        $appUserRepository = $entityManager->getRepository(AppUser::class);

        $criteria = array();
        $users = $appUserRepository->findBy($criteria, array("createAt" => "DESC"), $limit, ($page - 1) * $limit);
        $count = $appUserRepository->count($criteria);

        return $this->render('app/admin/user/list/index.html.twig', [
            "users" => $users,
            "page" => $page,
            "limit" => $limit,
            "count" => $count,
            "maxPage" => ceil($count / $limit),
            "searchMode" => false
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    protected function handleSearch(Request $request) : Response
    {
        $search = $request->query->get("search");
        $page = $request->query->get("page", 1);
        $limit = $request->query->get("limit", $this->defaultLimit);

        $page = max(1, $page);
        $limit = min($this->defaultMaxLimit, $limit);
        $limit = max(1, $limit);


        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var AppUserRepository $appUserRepository
         */
        $appUserRepository = $entityManager->getRepository(AppUser::class);

        $appUsers = $appUserRepository->createQueryBuilder("appUser")
            ->where("appUser.email LIKE :search")
            ->orWhere("appUser.username LIKE :search")
            // add more
            ->setParameter("search", "%".$search."%")
            ->setFirstResult(($page - 1) * $limit)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        $count = 0;
        try {
            $count = $appUserRepository->createQueryBuilder("appUser")
                ->select("COUNT(appUser.uuid)")
                ->where("appUser.email LIKE :search")
                ->orWhere("appUser.username LIKE :search")
                // add more
                ->setParameter("search", "%".$search."%")
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
        }

        return $this->render('app/admin/user/list/index.html.twig', [
            "users" => $appUsers,
            "page" => $page,
            "limit" => $limit,
            "count" => $count,
            "maxPage" => ceil($count / $limit),
            "searchMode" => true
        ]);
    }
}
