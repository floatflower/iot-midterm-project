<?php

namespace App\Controller\App\Admin\User;

use App\Form\App\Admin\AppUser\CreateType;
use App\Service\Auth\EmailVerifyService;
use App\Service\Entity\AppUser\CreateService;
use App\Service\Entity\AppUser\Exception\EmailInUsedException;
use App\Service\Entity\AppUser\Exception\EmailRequiredException;
use App\Service\Entity\AppUser\Exception\PasswordRequiredException;
use App\Service\Entity\AppUser\Exception\UsernameInUsedException;
use App\Service\Entity\AppUser\Result\CreateResult;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateController extends AbstractController
{
    /**
     * @param Request $request
     * @param CreateService $createService
     * @param EmailVerifyService $emailVerifyService
     * @return Response
     * @Route("/admin/users/create", name="app.admin.users.create")
     */
    public function __invoke(Request $request,
                             CreateService $createService,
                             EmailVerifyService $emailVerifyService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $data = array();
        $appUserCreateForm = $this->createForm(CreateType::class, $data);
        $appUserCreateForm->handleRequest($request);

        if($appUserCreateForm->isSubmitted() && $appUserCreateForm->isValid()) {

            $data = $appUserCreateForm->getData();

            try {
                $createResult = $createService->create($data);
                $appUser = $createResult->getAppUser();

                $emailVerifyService->verify($appUser);

                return $this->redirectToRoute("app.admin.users.fetch", array(
                    "uuid" => $appUser->getUuid()
                ));
            } catch (EmailRequiredException $emailRequiredException) {
                $appUserCreateForm->addError(new FormError("電子郵件是必要欄位"));
            } catch (PasswordRequiredException $passwordRequiredException) {
                $appUserCreateForm->addError(new FormError("密碼是必要欄位"));
            } catch (EmailInUsedException $emailInUsedException) {
                $appUserCreateForm->addError(new FormError("電子郵件已經被使用"));
            } catch (UsernameInUsedException $usernameInUsedException) {
                $appUserCreateForm->addError(new FormError("用戶名稱已經被使用"));
            }
        }

        return $this->render("app/admin/user/create/index.html.twig", array(
            "appUserCreateForm" => $appUserCreateForm->createView()
        ));
    }
}
