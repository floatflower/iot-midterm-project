<?php

namespace App\Controller\App\Admin\User;

use App\Entity\AppUser;
use App\Repository\AppUserRepository;
use App\Security\Voter\AppUserVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListReferralController extends AbstractController
{
    private int $defaultLimit = 50;
    private int $defaultMaxLimit = 100;

    /**
     * @param Request $request
     * @param string $uuid
     * @return Response
     * @Route("/admin/users/{uuid}/referrals", name="app.admin.users.list_referrals")
     */
    public function index(Request $request, string $uuid): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $page = $request->query->get("page", 1);
        $limit = $request->query->get("limit", $this->defaultLimit);

        $page = max(1, $page);
        $limit = min($this->defaultMaxLimit, $limit);
        $limit = max(1, $limit);

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var AppUserRepository $appUserRepository
         */
        $appUserRepository = $entityManager->getRepository(AppUser::class);
        $appUser = $appUserRepository->findOneByUuid($uuid);

        if(!$appUser) {
            return $this->redirectToRoute("app.admin.users.list");
        }

        $this->denyAccessUnlessGranted(AppUserVoter::APP_USER_FETCH, $appUser);

        $criteria = array("referrer" => $appUser);
        $referrals = $appUserRepository->findBy($criteria, array("createAt" => "DESC"), $limit, ($page - 1) * $limit);
        $count = $appUserRepository->count($criteria);

        return $this->render('app/admin/user/list-referral/index.html.twig', array(
            "referrals" => $referrals,
            "appUser" => $appUser,
            "page" => $page,
            "limit" => $limit,
            "count" => $count,
            "maxPage" => ceil($count / $limit),
            "searchMode" => false
        ));
    }
}
