<?php

namespace App\Controller\App\Admin\User;

use App\Entity\AppUser;
use App\Form\App\Admin\AppUser\EditType;
use App\Repository\AppUserRepository;
use App\Security\Voter\AppUserVoter;
use App\Service\Entity\AppUser\Exception\EmailInUsedException;
use App\Service\Entity\AppUser\UpdateService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $uuid
     * @param UpdateService $updateService
     * @return Response
     * @Route("/admin/users/{uuid}/edit", name="app.admin.users.edit")
     */
    public function __invoke(Request $request, string $uuid, UpdateService $updateService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var AppUserRepository $appUserRepository
         */
        $appUserRepository = $entityManager->getRepository(AppUser::class);
        $appUser = $appUserRepository->findOneByUuid($uuid);

        if(!$appUser)
            $this->redirectToRoute("app.admin.users.list");

        $this->denyAccessUnlessGranted(AppUserVoter::APP_USER_UPDATE, $appUser);

        $data = array(
            "email" => $appUser->getEmail(),
            "mobile" => $appUser->getMobile(),
            "is_admin" => $appUser->isAdmin(),
            "forever" => $appUser->isNeverExpireUser(),
            // 為了體驗著想，如果發現是永久用戶，就將預設的過期時間切到今天
            "expire_time" => $appUser->isNeverExpireUser() ? (new \DateTime("now"))->add(new \DateInterval("P7D")) : $appUser->getExpireTime(),
            "avatar" => $appUser->getAvatar()
        );

        $appUserEditForm = $this->createForm(EditType::class, $data);
        $appUserEditForm->handleRequest($request);

        if($appUserEditForm->isSubmitted() && $appUserEditForm->isValid()) {

            $data = $appUserEditForm->getData();

            try {
                $updateResult = $updateService->update($appUser, $data);

                return $this->redirectToRoute("app.admin.users.fetch", array(
                    "uuid" => $appUser->getUuid()
                ));
            } catch (EmailInUsedException $emailInUsedException) {
                $appUserEditForm->addError(new FormError("電子郵件已經被使用"));
            }
        }

        return $this->render("app/admin/user/edit/index.html.twig", array(
            "appUser" => $appUser,
            "appUserEditForm" => $appUserEditForm->createView()
        ));
    }
}
