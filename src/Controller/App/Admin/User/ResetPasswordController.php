<?php

namespace App\Controller\App\Admin\User;

use App\Entity\AppUser;
use App\Form\App\Admin\AppUser\ResetPasswordType;
use App\Service\Entity\AppUser\ResetPasswordService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ResetPasswordController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $uuid
     * @param ResetPasswordService $resetPasswordService
     * @return Response
     * @Route("/admin/users/{uuid}/reset-password", name="app.admin.users.reset_password")
     */
    public function __invoke(Request $request,
                             string $uuid,
                             ResetPasswordService $resetPasswordService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        $appUserRepository = $entityManager->getRepository(AppUser::class);
        $appUser = $appUserRepository->findOneByUuid($uuid);

        if(!$appUser) {
            return $this->redirectToRoute("app.admin.users.list");
        }

        $data = array();
        $resetPasswordForm = $this->createForm(ResetPasswordType::class, $data);
        $resetPasswordForm->handleRequest($request);

        if($resetPasswordForm->isSubmitted() && $resetPasswordForm->isValid()) {

            $data = $resetPasswordForm->getData();

            $newPassword = $data["new_password"];
            $newPasswordConfirm = $data["new_password_confirm"];

            if($newPassword !== $newPasswordConfirm) {
                $resetPasswordForm->addError(new FormError("兩次輸入的密碼不相同"));
            } else {
                $passwordResetResult = $resetPasswordService->resetPassword($appUser, $newPassword);
                return $this->render("app/admin/user/reset-password/finished.html.twig", array(
                    "appUser" => $appUser
                ));
            }
        }

        return $this->render("app/admin/user/reset-password/index.html.twig", array(
            "resetPasswordForm" => $resetPasswordForm->createView(),
            "appUser" => $appUser
        ));
    }
}
