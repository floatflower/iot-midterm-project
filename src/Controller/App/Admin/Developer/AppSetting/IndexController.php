<?php

namespace App\Controller\App\Admin\Developer\AppSetting;

use App\Service\Entity\AppSetting\UpdateService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @param Request $request
     * @param UpdateService $updateService
     * @return Response
     * @Route("/admin/developer/app-settings", name="app.admin.developer.app_settings.enable")
     */
    public function index(Request $request, UpdateService $updateService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_DEVELOPER");

        return $this->render('app/admin/developer/app-setting/index.html.twig');
    }
}
