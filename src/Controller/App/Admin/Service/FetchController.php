<?php

namespace App\Controller\App\Admin\Service;

use App\Entity\Service;
use App\Repository\ServiceRepository;
use App\Security\Voter\ServiceVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FetchController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $uuid
     * @return Response
     * @Route("/admin/services/{uuid}", name="app.admin.services.fetch", methods={"GET"})
     */
    public function __invoke(Request $request,
                             string $uuid): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var ServiceRepository $serviceRepository
         */
        $serviceRepository = $entityManager->getRepository(Service::class);
        $service = $serviceRepository->findOneByUuid($uuid);

        if(!$service)
            return $this->redirectToRoute("app.admin.services.list");

        $this->denyAccessUnlessGranted(ServiceVoter::FETCH, $service);

        return $this->render('app/admin/service/fetch/index.html.twig', [
            "service" => $service
        ]);
    }
}
