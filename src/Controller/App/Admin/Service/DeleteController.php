<?php

namespace App\Controller\App\Admin\Service;

use App\Entity\Service;
use App\Repository\ServiceRepository;
use App\Security\Voter\ServiceVoter;
use App\Service\Entity\Service\DeleteService;
use App\Service\Entity\Service\Exception\ServiceNotFoundException;
use App\Service\Entity\Service\Result\DeleteResult;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DeleteController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $uuid
     * @param DeleteService $deleteService
     * @return Response
     * @Route("/admin/services/{uuid}/delete", name="app.admin.services.delete")
     */
    public function __invoke(Request $request,
                             string $uuid,
                             DeleteService $deleteService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var ServiceRepository $serviceRepository
         */
        $serviceRepository = $entityManager->getRepository(Service::class);
        $service = $serviceRepository->findOneByUuid($uuid);

        if(!$service)
            return $this->redirectToRoute("app.admin.services.list");

        $this->denyAccessUnlessGranted(ServiceVoter::DELETE, $service);

        $deleteResult = $deleteService->delete($service);

        return $this->redirectToRoute("app.admin.services.list");
    }
}
