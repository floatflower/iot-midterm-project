<?php

namespace App\Controller\App\Admin\Service;

use App\Entity\Service;
use App\Form\App\Admin\Service\EditType;
use App\Repository\ServiceRepository;
use App\Security\Voter\ServiceVoter;
use App\Service\Entity\Service\Result\UpdateResult;
use App\Service\Entity\Service\UpdateService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $uuid
     * @param UpdateService $updateService
     * @return Response
     * @Route("/app/admin/services/{uuid}/edit", name="app.admin.services.edit")
     */
    public function __invoke(Request $request,
                             string $uuid,
                             UpdateService $updateService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var ServiceRepository $serviceRepository
         */
        $serviceRepository = $entityManager->getRepository(Service::class);
        $service = $serviceRepository->findOneByUuid($uuid);

        if(!$service)
            return $this->redirectToRoute("app.admin.services.list");

        $this->denyAccessUnlessGranted(ServiceVoter::UPDATE, $service);

        $data = array(
            "name" => $service->getName(),
            "description" => $service->getDescription()
        );

        $serviceEditForm = $this->createForm(EditType::class, $data);
        $serviceEditForm->handleRequest($request);

        if($serviceEditForm->isSubmitted() && $serviceEditForm->isValid()) {

            $data = $serviceEditForm->getData();

            /**
             * @var UpdateResult $updateResult
             */
            $updateResult = $updateService->update($service, $data);

            return $this->redirectToRoute("app.admin.services.fetch", array("uuid" => $service->getUuid()));
        }

        return $this->render('app/admin/service/edit/index.html.twig', [
            "service" => $service,
            "serviceEditForm" => $serviceEditForm->createView()
        ]);
    }
}
