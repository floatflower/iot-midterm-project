<?php

namespace App\Controller\App\Admin\Service;

use App\Entity\Service;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{
    /**
     * @param Request $request
     * @return Response
     * @Route("/admin/services", name="app.admin.services.list", methods={"GET"})
     */
    public function __invoke(Request $request): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();
        $serviceRepository = $entityManager->getRepository(Service::class);

        $services = $serviceRepository->findBy(array(), array("createAt" => "DESC"));

        return $this->render("app/admin/service/list/index.html.twig", array(
            "services" => $services
        ));
    }
}
