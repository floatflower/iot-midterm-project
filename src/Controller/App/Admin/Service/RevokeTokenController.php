<?php

namespace App\Controller\App\Admin\Service;

use App\Entity\Service;
use App\Repository\ServiceRepository;
use App\Security\Voter\ServiceVoter;
use App\Service\Entity\Service\Result\TokenRevokeResult;
use App\Service\Entity\Service\TokenRevokeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RevokeTokenController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $uuid
     * @param TokenRevokeService $tokenRevokeService
     * @return Response
     * @Route("/admin/services/{uuid}/revoke-token", name="app.admin.services.revoke_token")
     */
    public function __invoke(Request $request,
                          string $uuid,
                          TokenRevokeService $tokenRevokeService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var ServiceRepository $serviceRepository
         */
        $serviceRepository = $entityManager->getRepository(Service::class);
        $service = $serviceRepository->findOneByUuid($uuid);

        if(!$service)
            return $this->redirectToRoute("app.admin.services.list");

        $this->denyAccessUnlessGranted(ServiceVoter::UPDATE, $service);

        /**
         * @var TokenRevokeResult $revokeResult
         */
        $tokenRevokeResult = $tokenRevokeService->revoke($service);

        return $this->render("app/admin/service/revoke-token/finished.html.twig", array(
            "service" => $service,
            "plainToken" => $tokenRevokeResult->getPlainToken()
        ));
    }
}
