<?php

namespace App\Controller\App\Admin\Service;

use App\Entity\Service;
use App\Form\App\Admin\Service\CreateType;
use App\Service\Entity\Service\CreateService;
use App\Service\Entity\Service\Exception\NameRequiredException;
use App\Service\Entity\Service\Result\CreateResult;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateController extends AbstractController
{
    /**
     * @param Request $request
     * @param CreateService $createService
     * @return Response
     * @Route("/admin/services/create", name="app.admin.services.create")
     */
    public function __invoke(Request $request,
                             CreateService $createService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $data = array();
        $serviceCreateForm = $this->createForm(CreateType::class, $data);
        $serviceCreateForm->handleRequest($request);

        if($serviceCreateForm->isSubmitted() && $serviceCreateForm->isValid()) {

            $data = $serviceCreateForm->getData();

            try {
                $createResult = $createService->create($data);
                $service = $createResult->getService();

                return $this->render("app/admin/service/create/finished.html.twig", array(
                    "service" => $service,
                    "plainToken" => $createResult->getPlainToken()
                ));
            } catch (NameRequiredException $nameRequiredException) {
                $serviceCreateForm->addError(new FormError("服務名稱不得為空"));
            }

        }

        return $this->render('app/admin/service/create/index.html.twig', array(
            "serviceCreateForm" => $serviceCreateForm->createView()
        ));
    }
}
