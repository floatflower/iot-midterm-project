<?php

namespace App\Controller\App\Admin\Banner;

use App\Entity\Banner;
use App\Entity\BannerItem;
use App\Repository\BannerItemRepository;
use App\Repository\BannerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FetchController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $uuid
     * @return Response
     * @Route("/admin/banners/{uuid}", name="app.admin.banners.fetch", methods={"GET"})
     */
    public function __invoke(Request $request, string $uuid): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var BannerRepository $bannerRepository
         */
        $bannerRepository = $entityManager->getRepository(Banner::class);
        $banner = $bannerRepository->findOneByUuid($uuid);

        if(!$banner)
            return $this->redirectToRoute("app.admin.banners.list");

        /**
         * @var BannerItemRepository $bannerItemRepository
         */
        $bannerItemRepository = $entityManager->getRepository(BannerItem::class);
        $bannerItems = $bannerItemRepository->findBy(array("banner" => $banner), array("sort" => "ASC"));

        return $this->render('app/admin/banner/fetch/index.html.twig', array(
            "banner" => $banner,
            "bannerItems" => $bannerItems
        ));
    }
}
