<?php

namespace App\Controller\App\Admin\Banner;

use App\Form\App\Admin\Banner\CreateType;
use App\Service\Entity\Banner\CreateService;
use App\Service\Entity\Banner\Exception\NameRequiredException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateController extends AbstractController
{
    /**
     * @param Request $request
     * @param CreateService $createService
     * @return Response
     * @Route("/admin/banners/create", name="app.admin.banners.create")
     */
    public function __invoke(Request $request, CreateService $createService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $data = array();
        $bannerCreateForm = $this->createForm(CreateType::class, $data);
        $bannerCreateForm->handleRequest($request);

        if($bannerCreateForm->isSubmitted() && $bannerCreateForm->isValid())
        {
            $data = $bannerCreateForm->getData();

            try {
                $createResult = $createService->create($data);

                return $this->redirectToRoute("app.admin.banners.fetch", array(
                    "uuid" => $createResult->getBanner()->getUuid()
                ));
            } catch (NameRequiredException $nameRequiredException) {
                $bannerCreateForm->addError(new FormError("Banner 名稱不得為空。"));
            }
        }

        return $this->render('app/admin/banner/create/index.html.twig', array(
            "bannerCreateForm" => $bannerCreateForm->createView()
        ));
    }
}
