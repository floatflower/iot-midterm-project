<?php

namespace App\Controller\App\Admin\Banner;

use App\Entity\Banner;
use App\Repository\BannerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{
    private int $defaultLimit = 50;
    private int $defaultMaxLimit = 100;

    public function __construct()
    {

    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/admin/banners", name="app.admin.banners.list", methods={"GET"})
     */
    public function __invoke(Request $request): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $page = $request->query->get("page", 1);
        $limit = $request->query->get("limit", $this->defaultLimit);

        $page = max(1, $page);
        $limit = min($this->defaultMaxLimit, $limit);
        $limit = max(1, $limit);

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var BannerRepository $bannerRepository
         */
        $bannerRepository = $entityManager->getRepository(Banner::class);

        $criteria = array();
        $banners = $bannerRepository->findBy($criteria, array("createAt" => "DESC"), $limit, ($page - 1) * $limit);
        $count = $bannerRepository->count($criteria);

        return $this->render('app/admin/banner/list/index.html.twig', array(
            "banners" => $banners,
            "page" => $page,
            "limit" => $limit,
            "count" => $count,
            "maxPage" => ceil($count / $limit),
            "searchMode" => false
        ));
    }
}
