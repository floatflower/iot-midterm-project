<?php

namespace App\Controller\App\Admin\Banner;

use App\Entity\Banner;
use App\Form\App\Admin\Banner\EditType;
use App\Repository\BannerRepository;
use App\Service\Entity\Banner\UpdateService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $uuid
     * @param UpdateService $updateService
     * @return Response
     * @Route("/admin/banners/{uuid}/edit", name="app.admin.banners.edit")
     */
    public function __invoke(Request $request,
                          string $uuid,
                          UpdateService $updateService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var BannerRepository $bannerRepository
         */
        $bannerRepository = $entityManager->getRepository(Banner::class);
        $banner = $bannerRepository->findOneByUuid($uuid);

        if(!$banner)
            return $this->redirectToRoute("app.admin.banners.list");

        $data = array(
            "name" => $banner->getName(),
            "alias" => $banner->getAlias()
        );

        $bannerEditForm = $this->createForm(EditType::class, $data);
        $bannerEditForm->handleRequest($request);

        if($bannerEditForm->isSubmitted() && $bannerEditForm->isValid())
        {
            $data = $bannerEditForm->getData();

            $updateResult = $updateService->update($banner, $data);

            return $this->redirectToRoute("app.admin.banners.fetch", array(
                "uuid" => $banner->getUuid()
            ));
        }

        return $this->render('app/admin/banner/edit/index.html.twig', array(
            "bannerEditForm" => $bannerEditForm->createView(),
            "banner" => $banner
        ));
    }
}
