<?php

namespace App\Controller\App\Admin\Banner;

use App\Entity\Banner;
use App\Repository\BannerRepository;
use App\Service\Entity\Banner\DeleteService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DeleteController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $uuid
     * @param DeleteService $deleteService
     * @return Response
     * @Route("/admin/banners/{uuid}/delete", name="app.admin.banners.delete")
     */
    public function __invoke(Request $request,
                          string $uuid,
                          DeleteService $deleteService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var BannerRepository $bannerRepository
         */
        $bannerRepository = $entityManager->getRepository(Banner::class);
        $banner = $bannerRepository->findOneByUuid($uuid);

        if($banner)
            $deleteService->delete($banner);

        return $this->redirectToRoute("app.admin.banners.list");
    }
}
