<?php

namespace App\Controller\App\Admin\AppSetting;

use App\Entity\AppSetting;
use App\Form\App\Admin\AppSetting\CodeInjectionType;
use App\Repository\AppSettingRepository;
use App\Service\Entity\AppSetting\AppSettingKey;
use App\Service\Entity\AppSetting\UpdateService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CodeInjectionController extends AbstractController
{
    /**
     * @param Request $request
     * @param UpdateService $updateService
     * @return Response
     * @Route("/admin/app-settings/code-injection", name="app.admin.app_settings.code_injection")
     */
    public function __invoke(Request $request, UpdateService $updateService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var AppSettingRepository $appSettingRepository
         */
        $appSettingRepository = $entityManager->getRepository(AppSetting::class);

        $codeInjectionStart = $appSettingRepository->findOneByKey(AppSettingKey::CODE_INJECTION_START);
        $codeInjectionEnd = $appSettingRepository->findOneByKey(AppSettingKey::CODE_INJECTION_END);

        $data = array();

        if($codeInjectionStart) $data["start"] = $codeInjectionStart->getValue();
        if($codeInjectionEnd) $data["end"] = $codeInjectionEnd->getValue();

        $settingForm = $this->createForm(CodeInjectionType::class, $data);
        $settingForm->handleRequest($request);

        if($settingForm->isSubmitted() && $settingForm->isValid()) {

            $data = $settingForm->getData();

            $updateService->set(AppSettingKey::CODE_INJECTION_START, $data["start"]);
            $updateService->set(AppSettingKey::CODE_INJECTION_END, $data["end"]);

            $codeInjectionStart = $appSettingRepository->findOneByKey(AppSettingKey::CODE_INJECTION_START);
            $codeInjectionEnd = $appSettingRepository->findOneByKey(AppSettingKey::CODE_INJECTION_END);
        }

        return $this->render('app/admin/app-setting/code-injection/index.html.twig', array(
            "settingForm" => $settingForm->createView(),
            "codeInjectionStart" => $codeInjectionStart,
            "codeInjectionEnd" => $codeInjectionEnd
        ));
    }
}
