<?php

namespace App\Controller\App\Admin\AppSetting;

use App\Entity\AppSetting;
use App\Form\App\Admin\AppSetting\GATrackIdType;
use App\Repository\AppSettingRepository;
use App\Service\Entity\AppSetting\AppSettingKey;
use App\Service\Entity\AppSetting\UpdateService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GATrackIdController extends AbstractController
{
    /**
     * @param Request $request
     * @param UpdateService $updateService
     * @return Response
     * @Route("/admin/app-settings/ga-track-id", name="app.admin.app_settings.ga_track_id")
     */
    public function __invoke(Request $request, UpdateService $updateService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var AppSettingRepository $appSettingRepository
         */
        $appSettingRepository = $entityManager->getRepository(AppSetting::class);

        $gaTrackId = $appSettingRepository->findOneByKey(AppSettingKey::GA_TRACK_ID);

        $data = array();

        if($gaTrackId) $data["ga_track_id"] = $gaTrackId->getValue();

        $settingForm = $this->createForm(GATrackIdType::class, $data);
        $settingForm->handleRequest($request);

        if($settingForm->isSubmitted() && $settingForm->isValid())
        {
            $data = $settingForm->getData();
            $gaTrackId = $updateService->set(AppSettingKey::GA_TRACK_ID, $data["ga_track_id"]);

            $gaTrackId = $appSettingRepository->findOneByKey(AppSettingKey::GA_TRACK_ID);
        }

        return $this->render('app/admin/app-setting/ga-track-id/index.html.twig', array(
            "settingForm" => $settingForm->createView(),
            "gaTrackId" => $gaTrackId
        ));
    }
}
