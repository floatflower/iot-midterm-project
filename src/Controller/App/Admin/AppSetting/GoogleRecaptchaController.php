<?php

namespace App\Controller\App\Admin\AppSetting;

use App\Entity\AppSetting;
use App\Form\App\Admin\AppSetting\GoogleRecaptchaType;
use App\Repository\AppSettingRepository;
use App\Service\Entity\AppSetting\AppSettingKey;
use App\Service\Entity\AppSetting\UpdateService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GoogleRecaptchaController extends AbstractController
{
    /**
     * @param Request $request
     * @param UpdateService $updateService
     * @return Response
     * @Route("/admin/app-settings/google-recaptcha", name="app.admin.app_settings.google_recaptcha")
     */
    public function __invoke(Request $request, UpdateService $updateService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var AppSettingRepository $appSettingRepository
         */
        $appSettingRepository = $entityManager->getRepository(AppSetting::class);

        $googleRecaptchaSiteKey = $appSettingRepository->findOneByKey(AppSettingKey::GOOGLE_RECAPTCHA_SITE_KEY);
        $googleRecaptchaSiteSecret = $appSettingRepository->findOneByKey(AppSettingKey::GOOGLE_RECAPTCHA_SITE_SECRET);

        $data = array();

        if($googleRecaptchaSiteKey) $data["site_key"] = $googleRecaptchaSiteKey->getValue();
        if($googleRecaptchaSiteSecret) $data["site_secret"] = $googleRecaptchaSiteSecret->getValue();

        $settingForm = $this->createForm(GoogleRecaptchaType::class, $data);
        $settingForm->handleRequest($request);

        if($settingForm->isSubmitted() && $settingForm->isValid())
        {
            $data = $settingForm->getData();

            $updateService->set(AppSettingKey::GOOGLE_RECAPTCHA_SITE_KEY, $data["site_key"]);
            $updateService->set(AppSettingKey::GOOGLE_RECAPTCHA_SITE_SECRET, $data["site_secret"]);

            $googleRecaptchaSiteKey = $appSettingRepository->findOneByKey(AppSettingKey::GOOGLE_RECAPTCHA_SITE_KEY);
            $googleRecaptchaSiteSecret = $appSettingRepository->findOneByKey(AppSettingKey::GOOGLE_RECAPTCHA_SITE_SECRET);
        }

        return $this->render('app/admin/app-setting/google-recaptcha/index.html.twig', array(
            "settingForm" => $settingForm->createView(),
            "googleRecaptchaSiteKey" => $googleRecaptchaSiteKey,
            "googleRecaptchaSiteSecret" => $googleRecaptchaSiteSecret
        ));
    }
}
