<?php

namespace App\Controller\App\Admin\AppSetting;

use App\Entity\AppSetting;
use App\Form\App\Admin\AppSetting\NewebPayType;
use App\Repository\AppSettingRepository;
use App\Service\Entity\AppSetting\AppSettingKey;
use App\Service\Entity\AppSetting\UpdateService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NewebPayController extends AbstractController
{
    /**
     * @param Request $request
     * @param UpdateService $updateService
     * @return Response
     * @Route("/admin/app-settings/newebpay", name="app.admin.app_settings.newebpay")
     */
    public function __invoke(Request $request, UpdateService $updateService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var AppSettingRepository $appSettingRepository
         */
        $appSettingRepository = $entityManager->getRepository(AppSetting::class);

        $apiUrl = $appSettingRepository->findOneByKey(AppSettingKey::NEWEBPAY_API_URL);
        $merchantId = $appSettingRepository->findOneByKey(AppSettingKey::NEWEBPAY_MERCHANT_ID);
        $hashKey = $appSettingRepository->findOneByKey(AppSettingKey::NEWEBPAY_HASH_KEY);
        $hashIV = $appSettingRepository->findOneByKey(AppSettingKey::NEWEBPAY_HASH_IV);

        $data = array();

        if($merchantId) $data["merchant_id"] = $merchantId->getValue();
        if($hashKey) $data["hash_key"] = $hashKey->getValue();
        if($hashIV) $data["hash_iv"] = $hashIV->getValue();
        if($apiUrl) $data["api_url"] = $apiUrl->getValue();

        $settingForm = $this->createForm(NewebPayType::class, $data);
        $settingForm->handleRequest($request);

        if($settingForm->isSubmitted() && $settingForm->isValid())
        {

            $data = $settingForm->getData();

            $updateService->set(AppSettingKey::NEWEBPAY_MERCHANT_ID, $data["merchant_id"]);
            $updateService->set(AppSettingKey::NEWEBPAY_HASH_IV, $data["hash_iv"]);
            $updateService->set(AppSettingKey::NEWEBPAY_HASH_KEY, $data["hash_key"]);
            $updateService->set(AppSettingKey::NEWEBPAY_API_URL, $data["api_url"]);

            $merchantId = $appSettingRepository->findOneByKey(AppSettingKey::NEWEBPAY_MERCHANT_ID);
            $hashKey = $appSettingRepository->findOneByKey(AppSettingKey::NEWEBPAY_HASH_KEY);
            $hashIV = $appSettingRepository->findOneByKey(AppSettingKey::NEWEBPAY_HASH_IV);
            $apiUrl = $appSettingRepository->findOneByKey(AppSettingKey::NEWEBPAY_API_URL);

        }

        return $this->render('app/admin/app-setting/newebpay/index.html.twig', array(
            "settingForm" => $settingForm->createView(),
            "merchantId" => $merchantId,
            "hashKey" => $hashKey,
            "hashIV" => $hashIV,
            "apiUrl" => $apiUrl
        ));
    }
}
