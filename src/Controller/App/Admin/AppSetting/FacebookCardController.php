<?php

namespace App\Controller\App\Admin\AppSetting;

use App\Entity\AppSetting;
use App\Form\App\Admin\AppSetting\FacebookCardType;
use App\Repository\AppSettingRepository;
use App\Service\Entity\AppSetting\AppSettingKey;
use App\Service\Entity\AppSetting\UpdateService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FacebookCardController extends AbstractController
{
    /**
     * @param Request $request
     * @param UpdateService $updateService
     * @return Response
     * @Route("/admin/app-settings/facebook-card", name="app.admin.app_settings.facebook_card")
     */
    public function __invoke(Request $request, UpdateService $updateService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var AppSettingRepository $appSettingRepository
         */
        $appSettingRepository = $entityManager->getRepository(AppSetting::class);

        $facebookImage = $appSettingRepository->findOneByKey(AppSettingKey::FACEBOOK_IMAGE);
        $facebookTitle = $appSettingRepository->findOneByKey(AppSettingKey::FACEBOOK_TITLE);
        $facebookDescription = $appSettingRepository->findOneByKey(AppSettingKey::FACEBOOK_DESCRIPTION);

        $data = array();

        if($facebookImage) $data["image"] = $facebookImage->getValue();
        if($facebookDescription) $data["description"] = $facebookDescription->getValue();
        if($facebookTitle) $data["title"] = $facebookTitle->getValue();

        $settingForm = $this->createForm(FacebookCardType::class, $data);
        $settingForm->handleRequest($request);

        if($settingForm->isSubmitted() && $settingForm->isValid())
        {
            $data = $settingForm->getData();

            $updateService->set(AppSettingKey::FACEBOOK_IMAGE, $data["image"]);
            $updateService->set(AppSettingKey::FACEBOOK_DESCRIPTION, $data["description"]);
            $updateService->set(AppSettingKey::FACEBOOK_TITLE, $data["title"]);

            $facebookImage = $appSettingRepository->findOneByKey(AppSettingKey::FACEBOOK_IMAGE);
            $facebookTitle = $appSettingRepository->findOneByKey(AppSettingKey::FACEBOOK_TITLE);
            $facebookDescription = $appSettingRepository->findOneByKey(AppSettingKey::FACEBOOK_DESCRIPTION);
        }

        return $this->render('app/admin/app-setting/facebook-card/index.html.twig', array(
            "settingForm" => $settingForm->createView(),
            "facebookImage" => $facebookImage,
            "facebookDescription" => $facebookDescription,
            "facebookTitle" => $facebookTitle
        ));
    }
}
