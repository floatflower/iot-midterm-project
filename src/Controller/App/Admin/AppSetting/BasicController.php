<?php

namespace App\Controller\App\Admin\AppSetting;

use App\Entity\AppSetting;
use App\Form\App\Admin\AppSetting\BasicType;
use App\Repository\AppSettingRepository;
use App\Service\Entity\AppSetting\AppSettingKey;
use App\Service\Entity\AppSetting\UpdateService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BasicController extends AbstractController
{
    /**
     * @param Request $request
     * @param UpdateService $updateService
     * @return Response
     * @Route("/admin/app-settings/basic", name="app.admin.app_settings.basic")
     */
    public function __invoke(Request $request, UpdateService $updateService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var AppSettingRepository $appSettingRepository
         */
        $appSettingRepository = $entityManager->getRepository(AppSetting::class);

        $siteTitle = $appSettingRepository->findOneByKey(AppSettingKey::SITE_TITLE);
        $siteDescription = $appSettingRepository->findOneByKey(AppSettingKey::SITE_DESCRIPTION);
        $siteUrl = $appSettingRepository->findOneByKey(AppSettingKey::SITE_URL);

        $data = array();

        if($siteTitle) $data["title"] = $siteTitle->getValue();
        if($siteDescription) $data["description"] = $siteDescription->getValue();
        if($siteUrl) $data["url"] = $siteUrl->getValue();

        $settingForm = $this->createForm(BasicType::class, $data);
        $settingForm->handleRequest($request);

        if($settingForm->isSubmitted() && $settingForm->isValid()) {

            $data = $settingForm->getData();

            $updateService->set(AppSettingKey::SITE_TITLE, $data["title"]);
            $updateService->set(AppSettingKey::SITE_DESCRIPTION, $data["description"]);
            $updateService->set(AppSettingKey::SITE_URL, $data["url"]);

            $siteTitle = $appSettingRepository->findOneByKey(AppSettingKey::SITE_TITLE);
            $siteDescription = $appSettingRepository->findOneByKey(AppSettingKey::SITE_DESCRIPTION);
            $siteUrl = $appSettingRepository->findOneByKey(AppSettingKey::SITE_URL);
        }

        return $this->render("app/admin/app-setting/basic/index.html.twig", array(
            "settingForm" => $settingForm->createView(),
            "siteTitle" => $siteTitle,
            "siteDescription" => $siteDescription,
            "siteUrl" => $siteUrl
        ));
    }
}
