<?php

namespace App\Controller\App\Admin\AppSetting;

use App\Entity\AppSetting;
use App\Form\App\Admin\AppSetting\TwitterCardType;
use App\Repository\AppSettingRepository;
use App\Service\Entity\AppSetting\AppSettingKey;
use App\Service\Entity\AppSetting\UpdateService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TwitterCardController extends AbstractController
{
    /**
     * @param Request $request
     * @param UpdateService $updateService
     * @return Response
     * @Route("/admin/app-settings/twitter-card", name="app.admin.app_settings.twitter_card")
     */
    public function __invoke(Request $request, UpdateService $updateService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var AppSettingRepository $appSettingRepository
         */
        $appSettingRepository = $entityManager->getRepository(AppSetting::class);

        $twitterImage = $appSettingRepository->findOneByKey(AppSettingKey::TWITTER_IMAGE);
        $twitterTitle = $appSettingRepository->findOneByKey(AppSettingKey::TWITTER_TITLE);
        $twitterDescription = $appSettingRepository->findOneByKey(AppSettingKey::TWITTER_DESCRIPTION);

        $data = array();

        if($twitterImage) $data["image"] = $twitterImage->getValue();
        if($twitterDescription) $data["description"] = $twitterDescription->getValue();
        if($twitterTitle) $data["title"] = $twitterTitle->getValue();

        $settingForm = $this->createForm(TwitterCardType::class, $data);
        $settingForm->handleRequest($request);

        if($settingForm->isSubmitted() && $settingForm->isValid())
        {
            $data = $settingForm->getData();

            $updateService->set(AppSettingKey::TWITTER_IMAGE, $data["image"]);
            $updateService->set(AppSettingKey::TWITTER_DESCRIPTION, $data["description"]);
            $updateService->set(AppSettingKey::TWITTER_TITLE, $data["title"]);

            $twitterImage = $appSettingRepository->findOneByKey(AppSettingKey::TWITTER_IMAGE);
            $twitterTitle = $appSettingRepository->findOneByKey(AppSettingKey::TWITTER_TITLE);
            $twitterDescription = $appSettingRepository->findOneByKey(AppSettingKey::TWITTER_DESCRIPTION);
        }

        return $this->render('app/admin/app-setting/twitter-card/index.html.twig', array(
            "settingForm" => $settingForm->createView(),
            "twitterImage" => $twitterImage,
            "twitterDescription" => $twitterDescription,
            "twitterTitle" => $twitterTitle
        ));
    }
}
