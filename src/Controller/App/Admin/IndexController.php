<?php

namespace App\Controller\App\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/admin", name="app.admin.index")
     */
    public function __invoke(): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");
        return $this->render('app/admin/index.html.twig');
    }
}
