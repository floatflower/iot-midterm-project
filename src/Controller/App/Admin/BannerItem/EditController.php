<?php

namespace App\Controller\App\Admin\BannerItem;

use App\Entity\BannerItem;
use App\Form\App\Admin\BannerItem\EditType;
use App\Repository\BannerItemRepository;
use App\Service\Entity\BannerItem\UpdateService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $uuid
     * @param UpdateService $updateService
     * @return Response
     * @Route("/admin/banner-items/{uuid}/edit", name="app.admin.banner_items.edit")
     */
    public function __invoke(Request $request, string $uuid, UpdateService $updateService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var BannerItemRepository $bannerItemRepository
         */
        $bannerItemRepository = $entityManager->getRepository(BannerItem::class);

        $bannerItem = $bannerItemRepository->findOneByUuid($uuid);

        if(!$bannerItem)
            return $this->redirectToRoute("app.admin.banners.list");

        $data = array(
            "title" => $bannerItem->getTitle() ?? "",
            "subtitle" => $bannerItem->getSubtitle() ?? "",
            "link" => $bannerItem->getLink() ?? "",
            "image" => $bannerItem->getImage() ?? ""
        );

        $bannerItemEditForm = $this->createForm(EditType::class, $data);
        $bannerItemEditForm->handleRequest($request);

        if($bannerItemEditForm->isSubmitted() && $bannerItemEditForm->isValid()) {
            $data = $bannerItemEditForm->getData();
            $updateResult = $updateService->update($bannerItem, $data);

            return $this->redirectToRoute("app.admin.banners.fetch", array(
                "uuid" => $bannerItem->getBanner()->getUuid()
            ));
        }

        return $this->render('app/admin/banner-item/edit/index.html.twig', array(
            "bannerItemEditForm" => $bannerItemEditForm->createView(),
            "banner" => $bannerItem->getBanner(),
            "bannerItem" => $bannerItem
        ));
    }
}
