<?php

namespace App\Controller\App\Admin\BannerItem;

use App\Entity\Banner;
use App\Form\App\Admin\BannerItem\CreateType;
use App\Repository\BannerRepository;
use App\Service\Entity\BannerItem\CreateService;
use App\Service\Entity\BannerItem\Exception\ImageRequiredException;
use App\Service\Entity\BannerItem\Result\CreateResult;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $uuid
     * @param CreateService $createService
     * @return Response
     * @Route("/admin/banners/{uuid}/items/create", name="app.admin.banner_items.create")
     */
    public function __invoke(Request $request, string $uuid, CreateService $createService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var BannerRepository $bannerRepository
         */
        $bannerRepository = $entityManager->getRepository(Banner::class);
        $banner = $bannerRepository->findOneByUuid($uuid);

        if(!$banner)
            return $this->redirectToRoute("app.admin.banners.list");

        $data = array();
        $bannerItemCreateForm = $this->createForm(CreateType::class, $data);
        $bannerItemCreateForm->handleRequest($request);

        if($bannerItemCreateForm->isSubmitted() && $bannerItemCreateForm->isValid()) {

            $data = $bannerItemCreateForm->getData();

            try {
                $createResult = $createService->create($banner, $data);
                return $this->redirectToRoute("app.admin.banners.fetch", array(
                    "uuid" => $banner->getUuid()
                ));
            } catch (ImageRequiredException $imageRequiredException) {
                $bannerItemCreateForm->addError(new FormError("圖片是必要欄位"));
            }

        }

        return $this->render('app/admin/banner-item/create/index.html.twig', array(
            "bannerItemCreateForm" => $bannerItemCreateForm->createView(),
            "banner" => $banner
        ));
    }
}
