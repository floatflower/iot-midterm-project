<?php

namespace App\Controller\App\Admin\BannerItem;

use App\Entity\BannerItem;
use App\Repository\BannerItemRepository;
use App\Service\Entity\BannerItem\DeleteService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DeleteController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $uuid
     * @param DeleteService $deleteService
     * @return Response
     * @Route("/admin/banner-items/{uuid}/delete", name="app.admin.banner_items.delete")
     */
    public function __invoke(Request $request, string $uuid, DeleteService $deleteService): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var BannerItemRepository $bannerItemRepository
         */
        $bannerItemRepository = $entityManager->getRepository(BannerItem::class);

        $bannerItem = $bannerItemRepository->findOneByUuid($uuid);

        if(!$bannerItem)
            return $this->redirectToRoute("app.admin.banners.list");

        $deleteResult = $deleteService->delete($bannerItem);

        $banner = $bannerItem->getBanner();
        return $this->redirectToRoute("app.admin.banners.fetch", array(
            "uuid" => $banner->getUuid()
        ));
    }
}
