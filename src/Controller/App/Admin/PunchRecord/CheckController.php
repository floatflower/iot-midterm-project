<?php

namespace App\Controller\App\Admin\PunchRecord;

use App\Entity\AppUser;
use App\Entity\AppUserPunchRecord;
use App\Repository\AppUserRepository;
use Firebase\JWT\JWT;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class CheckController extends AbstractController
{
    /**
     * @Route("/admin/punch-records/check", name="app.admin.punch_record.check")
     */
    public function index(Request $request, Security $security): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $code = $request->query->get("code", null);

        if(null !== $code) {

            $key = $this->getParameter("JWT_KEY");
            $decoded = (array)(JWT::decode($code, $key, array('HS256')));

            $userUuid = $decoded["user_uuid"];
            $type = $decoded["type"];

            $entityManager = $this->getDoctrine()->getManager();

            /**
             * @var AppUserRepository $appUserRepository
             */
            $appUserRepository = $entityManager->getRepository(AppUser::class);

            $appUser = $appUserRepository->findOneBy(array("uuid" => $userUuid));

            if($appUser) {

                $appUserPunchRecord = new AppUserPunchRecord();
                $appUserPunchRecord->setAppUser($appUser);
                $appUserPunchRecord->setType($type);

                $entityManager->persist($appUserPunchRecord);
                $entityManager->flush();

                return $this->render("app/admin/punch_record/check/index.html.twig", array(
                    "appUser" => $appUser,
                    "appUserPunchRecord" => $appUserPunchRecord
                ));

            }

        }

        return $this->redirectToRoute("app.admin.index");
    }
}
