<?php

namespace App\Command;

use App\Entity\AppUser;
use App\Entity\Developer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SetupDevCommand extends Command
{
    protected static $defaultName = 'app:setup:dev';

    /**
     * @var EntityManagerInterface $entityManager
     */
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager, string $name = null)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Setting up the data in development environment.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->generateData();


        $io->success('Setup Finished!');

        return Command::SUCCESS;
    }

    private function generateData()
    {
        $adminUser = new AppUser();
        $adminUser->setUsername("floatflower");
        $adminUser->setEmail("floatflower1029@gmail.com");
        $adminUser->setPasswordHash(password_hash("p@ssw0rD", PASSWORD_BCRYPT));
        $adminUser->setEmailVerified(true);
        $adminUser->setAsNeverExpireUser();
        $adminUser->setMobile("0952693232");
        $adminUser->setMobileVerified(true);
        $adminUser->setAdmin(true);
        $adminUser->setDeveloper(true);
        $this->entityManager->persist($adminUser);
        $this->entityManager->flush();
    }
}
