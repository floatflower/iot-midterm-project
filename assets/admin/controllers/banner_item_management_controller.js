import { Controller } from 'stimulus';

/*
 * This is an example Stimulus controller!
 *
 * Any element with a data-controller="hello" attribute will cause
 * this controller to be executed. The name "hello" comes from the filename:
 * hello_controller.js -> "hello"
 *
 * Delete this file or adapt it for your use!
 */
export default class extends Controller {
    connect() {
        let sortable = Sortable.create(this.element, {
            group: 'draggableMultiple',
            animation: 360,
            onUpdate: (event) => {
                sortable.option("disabled", true);
                let uuid = event.item.getAttribute("data-banner-item-uuid");
                fetch(`/api/admin/v1/banner-items/${uuid}/reorder`, {
                    method: "POST",
                    body: JSON.stringify({target: event.newIndex})
                }).then(() => {
                    sortable.option("disabled", false);
                }, () => {
                    sortable.option("disabled", false);
                })
            }
        });
    }
}
