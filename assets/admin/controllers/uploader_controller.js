import { Controller } from 'stimulus';

/*
 * This is an example Stimulus controller!
 *
 * Any element with a data-controller="hello" attribute will cause
 * this controller to be executed. The name "hello" comes from the filename:
 * hello_controller.js -> "hello"
 *
 * Delete this file or adapt it for your use!
 */
export default class extends Controller {
    static values = { image: String, output: String, title: String }

    connect() {
        this.element.classList.add("single-image-uploader");
        this.element.style.backgroundImage = `url('${this.imageValue}')`;

        let uploaderTitle = document.createElement("div");
        uploaderTitle.classList.add("uploader-title");
        uploaderTitle.id = "uploader-hint";
        uploaderTitle.innerText = this.titleValue ?? "";
        uploaderTitle.onclick = () => {
            this.element.click();
        }

        if(this.imageValue.length > 0) uploaderTitle.classList.add('d-none');
        this.element.append(uploaderTitle);

        let dropzone = new Dropzone(this.element, {
            url: "/api/admin/v1/upload/images",
            paramName: 'image[file]',
            maxFile: 1,
            maxFileSize: 10,
            acceptedFiles: "image/*",
            dictRemoveFile: true,
            success: (file, response) => {
                this.element.setAttribute("style", `background-image: url('${response.uri}');`)
                uploaderTitle.classList.add('d-none');
                let outputTarget = document.getElementById(this.outputValue);
                outputTarget.value = response.uri;
            },
            uploadprogress: (event) => {
                if(event.upload.progress === 100)
                    uploaderTitle.innerText = "上傳完畢，處理中...";
                else
                    uploaderTitle.innerText = `上傳中...${event.upload.progress}%`;
            }
        })

        let uploaderRemoveButton = document.createElement("button");
        uploaderRemoveButton.classList.add("uploader-remove");
        uploaderRemoveButton.innerHTML = "<i class=\"ion-trash-a\"></i>"
        uploaderRemoveButton.type = "button";
        uploaderRemoveButton.onclick = () => {
            dropzone.removeAllFiles();
            this.element.setAttribute("style", `background-image: url('');`)
            let outputTarget = document.getElementById(this.outputValue);
            outputTarget.value = "";
            uploaderTitle.classList.remove('d-none');
        }

        this.element.append(uploaderRemoveButton);
    }
}
