<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210225081135 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_user CHANGE expire_time expire_time DATETIME DEFAULT \'9999-12-31\' NOT NULL');
        $this->addSql('ALTER TABLE app_user_mobile_verify_code ADD mobile VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_user CHANGE expire_time expire_time DATETIME DEFAULT \'9999-12-31 00:00:00\' NOT NULL');
        $this->addSql('ALTER TABLE app_user_mobile_verify_code DROP mobile');
    }
}
