<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210501140601 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE app_user_punch_record (id BIGINT AUTO_INCREMENT NOT NULL, app_user_id BIGINT DEFAULT NULL, uuid VARCHAR(255) NOT NULL, type INT NOT NULL, create_at DATETIME NOT NULL, update_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_A7C6FD77D17F50A6 (uuid), INDEX IDX_A7C6FD774A3353D8 (app_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE app_user_punch_record ADD CONSTRAINT FK_A7C6FD774A3353D8 FOREIGN KEY (app_user_id) REFERENCES app_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_user CHANGE expire_time expire_time DATETIME DEFAULT \'9999-12-31\' NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE app_user_punch_record');
        $this->addSql('ALTER TABLE app_user CHANGE expire_time expire_time DATETIME DEFAULT \'9999-12-31 00:00:00\' NOT NULL');
    }
}
