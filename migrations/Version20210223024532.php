<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210223024532 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE app_setting (id BIGINT AUTO_INCREMENT NOT NULL, setting_key VARCHAR(255) NOT NULL, setting_value VARCHAR(255) DEFAULT NULL, create_at DATETIME NOT NULL, update_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_722938D55FA1E697 (setting_key), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_user (id BIGINT AUTO_INCREMENT NOT NULL, referrer_id BIGINT DEFAULT NULL, uuid VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, email VARCHAR(255) DEFAULT NULL, is_email_verified TINYINT(1) DEFAULT \'0\' NOT NULL, password_hash VARCHAR(255) DEFAULT NULL, mobile VARCHAR(255) DEFAULT NULL, is_mobile_verified TINYINT(1) DEFAULT \'0\' NOT NULL, facebook_id VARCHAR(255) DEFAULT NULL, google_id VARCHAR(255) DEFAULT NULL, referral_code VARCHAR(255) NOT NULL, is_admin TINYINT(1) DEFAULT \'0\' NOT NULL, is_developer TINYINT(1) DEFAULT \'0\' NOT NULL, expire_time DATETIME DEFAULT \'9999-12-31\' NOT NULL, avatar VARCHAR(255) DEFAULT NULL, create_at DATETIME NOT NULL, update_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_88BDF3E9D17F50A6 (uuid), UNIQUE INDEX UNIQ_88BDF3E9F85E0677 (username), UNIQUE INDEX UNIQ_88BDF3E9E7927C74 (email), UNIQUE INDEX UNIQ_88BDF3E93C7323E0 (mobile), UNIQUE INDEX UNIQ_88BDF3E99BE8FD98 (facebook_id), UNIQUE INDEX UNIQ_88BDF3E976F5C865 (google_id), UNIQUE INDEX UNIQ_88BDF3E96447454A (referral_code), INDEX IDX_88BDF3E9798C22DB (referrer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_user_auth_token (token_hash VARCHAR(255) NOT NULL, app_user_id BIGINT DEFAULT NULL, session VARCHAR(255) DEFAULT NULL, create_at DATETIME NOT NULL, expire_at DATETIME NOT NULL, update_at DATETIME NOT NULL, user_agent LONGTEXT DEFAULT NULL, client_ip VARCHAR(255) DEFAULT NULL, INDEX IDX_6EA8132B4A3353D8 (app_user_id), PRIMARY KEY(token_hash)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_user_email_verify_token (token_hash VARCHAR(255) NOT NULL, app_user_id BIGINT DEFAULT NULL, email VARCHAR(255) NOT NULL, create_at DATETIME NOT NULL, update_at DATETIME NOT NULL, expire_at DATETIME NOT NULL, INDEX IDX_71440ADA4A3353D8 (app_user_id), PRIMARY KEY(token_hash)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_user_mobile_verify_code (session VARCHAR(255) NOT NULL, app_user_id BIGINT DEFAULT NULL, code_hash VARCHAR(255) NOT NULL, error_count INT NOT NULL, create_at DATETIME NOT NULL, expire_at DATETIME NOT NULL, INDEX IDX_13E8F76C4A3353D8 (app_user_id), PRIMARY KEY(session)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_user_one_time_pin (session VARCHAR(255) NOT NULL, app_user_id BIGINT DEFAULT NULL, code_hash VARCHAR(255) NOT NULL, error_count INT NOT NULL, create_at DATETIME NOT NULL, expire_at DATETIME NOT NULL, INDEX IDX_923B81D64A3353D8 (app_user_id), PRIMARY KEY(session)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_user_reset_password_token (token_hash VARCHAR(255) NOT NULL, app_user_id BIGINT DEFAULT NULL, create_at DATETIME NOT NULL, update_at DATETIME NOT NULL, expire_at DATETIME NOT NULL, INDEX IDX_390374B04A3353D8 (app_user_id), PRIMARY KEY(token_hash)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE banner (id BIGINT AUTO_INCREMENT NOT NULL, uuid VARCHAR(255) NOT NULL, alias VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, create_at DATETIME NOT NULL, update_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_6F9DB8E7D17F50A6 (uuid), UNIQUE INDEX UNIQ_6F9DB8E7E16C6B94 (alias), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE banner_item (id BIGINT AUTO_INCREMENT NOT NULL, banner_id BIGINT DEFAULT NULL, uuid VARCHAR(255) NOT NULL, image LONGTEXT NOT NULL, title VARCHAR(255) NOT NULL, subtitle LONGTEXT DEFAULT NULL, link LONGTEXT DEFAULT NULL, sort INT NOT NULL, create_at DATETIME NOT NULL, update_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_17A27474D17F50A6 (uuid), INDEX IDX_17A27474684EC833 (banner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE service (id BIGINT AUTO_INCREMENT NOT NULL, uuid VARCHAR(255) NOT NULL, token_hash VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, create_at DATETIME NOT NULL, update_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_E19D9AD2D17F50A6 (uuid), UNIQUE INDEX UNIQ_E19D9AD2B3BC57DA (token_hash), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE app_user ADD CONSTRAINT FK_88BDF3E9798C22DB FOREIGN KEY (referrer_id) REFERENCES app_user (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE app_user_auth_token ADD CONSTRAINT FK_6EA8132B4A3353D8 FOREIGN KEY (app_user_id) REFERENCES app_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_user_email_verify_token ADD CONSTRAINT FK_71440ADA4A3353D8 FOREIGN KEY (app_user_id) REFERENCES app_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_user_mobile_verify_code ADD CONSTRAINT FK_13E8F76C4A3353D8 FOREIGN KEY (app_user_id) REFERENCES app_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_user_one_time_pin ADD CONSTRAINT FK_923B81D64A3353D8 FOREIGN KEY (app_user_id) REFERENCES app_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_user_reset_password_token ADD CONSTRAINT FK_390374B04A3353D8 FOREIGN KEY (app_user_id) REFERENCES app_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE banner_item ADD CONSTRAINT FK_17A27474684EC833 FOREIGN KEY (banner_id) REFERENCES banner (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_user DROP FOREIGN KEY FK_88BDF3E9798C22DB');
        $this->addSql('ALTER TABLE app_user_auth_token DROP FOREIGN KEY FK_6EA8132B4A3353D8');
        $this->addSql('ALTER TABLE app_user_email_verify_token DROP FOREIGN KEY FK_71440ADA4A3353D8');
        $this->addSql('ALTER TABLE app_user_mobile_verify_code DROP FOREIGN KEY FK_13E8F76C4A3353D8');
        $this->addSql('ALTER TABLE app_user_one_time_pin DROP FOREIGN KEY FK_923B81D64A3353D8');
        $this->addSql('ALTER TABLE app_user_reset_password_token DROP FOREIGN KEY FK_390374B04A3353D8');
        $this->addSql('ALTER TABLE banner_item DROP FOREIGN KEY FK_17A27474684EC833');
        $this->addSql('DROP TABLE app_setting');
        $this->addSql('DROP TABLE app_user');
        $this->addSql('DROP TABLE app_user_auth_token');
        $this->addSql('DROP TABLE app_user_email_verify_token');
        $this->addSql('DROP TABLE app_user_mobile_verify_code');
        $this->addSql('DROP TABLE app_user_one_time_pin');
        $this->addSql('DROP TABLE app_user_reset_password_token');
        $this->addSql('DROP TABLE banner');
        $this->addSql('DROP TABLE banner_item');
        $this->addSql('DROP TABLE service');
    }
}
